#use Gnome::Gtk3::MessageDialog;

#| a static class to keep global fields and methods
unit class Utils;


#| Signal that updates coinage lists / lexicons when one is updated
our $.coinages-changes = Supplier.new;
our $.glossary = Supplier.new;

#|[ list of all words that need to get updated this
should really be called @.unprocessed-coinages ]
our @.unprocessed;

#| keep track of all lexicons in the project
#our %.lexicons;


#|[ remove an $item form an @array ]
method cut(@array, $item) {
	if @array.first($item) {
		@array.splice(@array.first($item,:k), 1);
	}
	return @array;
}

#|[ force a $value between $min and $max ]
method clamp($value, $min, $max) is export {
	my $new-value = $value;
	$new-value = $min if $value < $min;
	$new-value = $max if $value > $max;
	return $new-value;
}

#| choose the highest value
method max(*@numbers) {
	my Numeric $largest = -Inf;
	for @numbers -> $num {
		next if $num !~~ Numeric;
		$largest = $num if $num > $largest;
	}
	return $largest;
}

#|[ make a dialog box pop up with a given $message ]
method show-dialog(Str $message) {
#	my Gnome::Gtk3::MessageDialog $dialog .= new(:markup-message($message));
#	$dialog.show-all;
#	$dialog.dialog-run;
#	$dialog.widget-destroy;
}


method parse-tsv(Str $tsv is copy) returns Array[List] {
	# remove empty lines and white space at end of lines and comments
	$tsv ~~ s:g/ ^^\s*'--'\V*\v+ || \s*'--'\N* || ^^\s* || \h*$$ //;
	#say $tsv;
	my Array[List] $result;

	# split tsv as a tsv
	my $split = $tsv.lines».split("\t", :skip-empty)».trim;
	#dd $split;
	for @$split {
		$result.push: [$_[0], $_[1], $_[2]];
	}
	return $result;
}

#|[ Normalize a $value that's between $min and $max, to be between 0 and 1 ]
method normalize(Numeric:D $value, Numeric:D $min, Numeric:D $max) returns Numeric {
	return ($value - $min) / ($max - $min);
}

our %.wrap-cache;
#|[ wrap text onto new lines for a given $width, but only wrap on whitespace ]
method wrap(Str $text, Int $width) {
	return Utils.wrap-cache{$text~$width} if Utils.wrap-cache{$text~$width}:exists;

	my Str $wrapped = Utils.wrap-text($text, $width);
	Utils.wrap-cache{$text~$width} = $wrapped;
	return $wrapped;
}

#|[ use the `wrap` method, not this ]
method wrap-text(Str $text is copy, Int $width) returns Str {
	# Trim the input string of leading and trailing spaces.
	$text .= trim;
	#    If the trimmed string's length is <= the width,
	#        Return the trimmed string.
	return $text if $text.chars ≤ $width;

	# Find the index of the last space in the trimmed string, starting at width
	# If there are no spaces, use the width as the index.
	my Int $index = $text.substr(0..$width).rindex(' ') // $width;
	# Split the trimmed string into two pieces at the index.
	my Str $left = $text.substr(0..$index).trim;
	my Str $right = $text.substr($index+1..*).trim;

	# Concatenate and return:
	# 	the trimmed portion before the index,
	#	a line break,
	#   and the result of calling WordWrap on the trimmed portion after
	#  	  the index (with the same width as the original call).
	return $left ~ "\n" ~ self.wrap($right, $width);
}
