use Utils;

class Word {
	has Str $.word is rw;
	has Str $.root-word is rw;
	has Str $.definition is rw;
	has Str $.root-definition is rw;
	has @.path;

	has Bool $.will-root-word-be-changed is rw = False;
	has Bool $.will-root-definition-be-changed is rw = False;
	has Supplier $.will-i-be-changed .= new;

	has Int $.starting-node is rw = 0;

	method processed() is rw {
		my $self = self;
		my Bool $storage = True;
		Proxy.new(
				FETCH => method () { $storage },
				STORE => method (Bool:D $new) {
						say 'updating ', $self.word, ' to ', $new;
						$storage = $new;
#						if $storage {
#							Utils.unprocessed = Utils.unprocessed.map({$_ if .root-word ne $self.root-word and .definition ne $self.definition and .path».get-name.join ne $self.path».get-name.join});
#						} else {
						Utils.unprocessed.append: $self if not $storage;
#						}
				    }
				);
	}
	has Bool $.processed;

	submethod BUILD(:$word, :$root-word, :$definition, :$root-definition, :@path, :$processed, :$will-root-word-be-changed=False, :$will-root-definition-be-changed=False) {
		$!word = $word; $!root-word = $root-word; $!definition = $definition; $!root-definition = $root-definition; @!path = @path; $!will-root-word-be-changed=$will-root-word-be-changed; $!will-root-definition-be-changed=$will-root-definition-be-changed;
		$!processed := self.processed();
		$!processed = $processed;
#		$!starting-node = @path[0];
	}

	#has $.lexicon is rw;
	#|[ returns a string that is a unique value associated with each word ]
	method id returns Str {
		return $!root-word ~ $!root-definition ~ @!path».get-name.join;
	}
}

#| Each node in the tree will be represented as a Node object
class Node {
	has Str $.title is required;
	has Int $.year is required;
	has Str $.sound-changes is rw = '';
	has Str $.description is rw = '';
	has Word @.coinages;
	has Word @.words is rw;

	has @.parents;
	has @.children;

	#| [ adds the specified child if it doesn't exist yet ]
	method add-child(Node $child) {
		if @!children.elems == 0 {
			@!children.append: $child;
		} else {
			my Bool $can-add = True;
			$can-add = False if .title eq $child.title and .year == $child.year for @!children;
			@!children.append: $child if $can-add;
		}
	}

	#| [ adds the specified parent if it doesn't exist yet ]
	method add-parents(@parents) {
		for @parents -> $parent {
			if @!parents.elems == 0 {
				@!parents.append: $parent;
			} else {
				my Bool $can-add = True;
				for @!parents {
					$can-add = False if .title eq $parent.title and .year == $parent.year;
				}
				@!parents.append: $parent if $can-add;
			}
		}
	}

	method add-or-update-word(Str:D $root-word, Str:D $root-definition, Node:D @path, Str:D $new-word='', Str:D $new-definition='', Bool $processed = False) {
		say ($root-word, $root-definition, @path».get-name.join("»")).join(" ");
		if (my $existing-word = self.get-word-by-id($root-word, $root-definition, @path)) {
			if $new-word {
				say 'updating word ↑ ';
				$existing-word.word = $new-word;
				$existing-word.definition = $new-definition;
				$existing-word.processed = $processed;
			}
			return $existing-word;
		} else {
			return self.add-word($new-word, $new-definition, $root-word, $root-definition, @path, $processed);
		}
	}

	method add-word(Str:D $word, Str:D $definition, Str:D $root-word, Str:D $root-definition, @path, Bool $processed, Bool $will-root-word-be-changed, Bool $will-root-definition-be-changed) {
		my Word $new-word .= new(:$word, :$definition, :$root-word, :$root-definition, :@path, :$processed, :$will-root-word-be-changed, :$will-root-definition-be-changed);
		@!words.append: $new-word;

		return $new-word;
	}

	#|[ update all the words that come from a specific $old-coinage to
		the $new-coinage  ]
	method update-words-root(Word:D $old-coinage, Word:D $new-coinage) {
		for @!words -> $word {
			if $word.root-word eq $old-coinage.root-word and
			  $word.root-definition eq $old-coinage.root-definition and
			  $word.path[0].get-name eq $old-coinage.path[0].get-name {
				say "updating {$word.root-word} to {$new-coinage.root-word}";
				$word.will-root-word-be-changed = $word.root-word ne $new-coinage.root-word;
				$word.will-root-definition-be-changed = $word.root-definition ne $new-coinage.root-definition;
				$word.root-word = $new-coinage.root-word;
				$word.root-definition = $new-coinage.root-definition;
				$word.will-i-be-changed.emit($word);
			}
		}
	}

	#| add word to coinages array
	method coin-word(Str $word, Str $definition, Bool $processed=False) {
		my Word $new-word .= new(:$word, :$definition, :root-word($word), :root-definition($definition), :$processed);
		$new-word.path.append: self;
		@!coinages.append: $new-word;
#		Utils.coinages.append: $new-word if not $processed;
		return $new-word;
	}

	multi method coin-or-update-word(Word:D $old-word, Str:D $word, Str:D $definition, Bool $processed = False) {
		for @!coinages.map({$_ if $old-word.id eq $_.id}) -> $_ {
			.word = $word;
			.root-word = $word;
			.definition = $definition;
			.root-definition = $definition;
			.processed = $processed;
			#.lexicon = $lexicon;
			return $_;
		}
		return self.coin-word($word, $definition, $processed);
	}
	multi method coin-or-update-word(Word:D $old-word is rw, Word:D $new-word, Bool $processed = False) {
		if @!coinages.grep({ $old-word.id eq $_.id }).any {
			$old-word = $new-word;
			return $old-word;
		} else {
			@!coinages.append: $new-word;
			return $new-word;
		}
	}

	#|[ remove a word from the coinages list ]
	multi method remove-coined-word(Word $word) {
		Utils.cut(@!coinages, $word);

		my Node @path;
		sub recursively-remove-word($node) {
			my Word $word-by-id = $node.get-word-by-id($word.root-word, $word.root-definition, @path);
			say 'before: ', $node.words».root-word;
			Utils.cut($node.words, $word-by-id);
			say 'after:  ', $node.words».root-word;

			for $node.children -> $child {
				@path.push: $child;
				recursively-remove-word($child);
			}
			@path.pop;
		}
		@path.push: self;
		say "starting to remove words";
		recursively-remove-word(self);
	}

	method remove-parent(Node $parent) {
		Utils.cut(@!parents, $parent);
	}

	#|[ delete this Node and all references to it ]
	method delete {
		for @!parents -> $parent {
			Utils.cut($parent.children, self);
		}
		for @!children -> $child {
			Utils.cut($child.parents, self);
		}
	}

	#|[ the id is the combination of the root-word, root definition, and path ]
	method get-word-by-id(Str:D $root-word, Str:D $root-definition, Node:D @path) returns Word {
		for @!words {
			if .id eq ($root-word ~ $root-definition ~ @path».get-name.join) {
				say 'found word: ', .word;
				return $_;
			}
		}
	}

	#|[ the id is the combination of the root-word, root definition, and path ]
	method get-coined-word-by-id(Str:D $root-word, Str:D $root-definition) returns Word {
		for @!coinages {
			return $_ if $root-word eq .root-word and $root-definition eq .root-definition;
		}
	}

	method gist() {
		return "{$!title}\@{$!year}-{@!words».word}(P: {@!parents».title.join(',')}, C: {@!children».title.join(',')})";
	}

	#| get the id of this node
	method id returns Str { $!title ~ $!year }

	#| like the id, but human readable
	method get-name() returns Str {
		return "{$!title} \@{$!year}"
	}
}

#| represents a tree data structure
class Graph {
	has Node $.root;
	has Node $.cur-gen is rw;
	has Array @.children;
	has Int $.max-year = -999_999_999;
	has Int $.min-year = 999_999_999;

	submethod TWEAK() {
		$!root = Node.new(title=>'root', year=>0);
		@!children.push: [$!root];
		$!cur-gen = $!root;
	}

	#|[ Insert a $node for a given $parent
	#|  returns the added node
	#|  if $node already exists, return the existing node ]
	method add-node(Node $node, @parents?) returns Node {
		#dd $node, @parents;
		for @!children.kv -> $i, $child {
			if $child[0].title eq $node.title and $child[0].year == $node.year {
				#say "Node '{$child[0].title}' already exists";
				$!cur-gen = $child[0];
				return $child[0];
			} else {
				# dont add node if theres already one
				PARENTS: for @parents -> $parent {
					if ($child[0].title eq $parent.title and $child[0].year == $parent.year) {
						for $child[1..*-1] -> $c {
							if ($c.title eq $node.title) and ($c.year == $node.year) {
								next PARENTS;
							}
						}
						@!children[$i].push: $node;
					}
				}
			}
		}
		@!children.push: [$node];
		$!cur-gen = $node;
		$!max-year = $node.year if $node.year > $!max-year;
		$!min-year = $node.year if $node.year < $!min-year;
		return $node;
	}

	#|[ returns the node with the given $name and $year,
	#|  or create that node if it doesnt exist when :create is set ]
	method get-node(Str $name, Numeric $year, :$create) returns Node {
		return $!root if $name eq 'root'|'';
		for @!children».[0] -> $child {
			return $child if $child.title eq $name and $child.year == $year;
		}
		return self.add-node(Node.new(title=>$name, :$year), [$!root]) if $create;
		# if all else fails
		return Nil;
	}

	method has-node(Str $name, Numeric $year) returns Bool {
		return $!root if $name eq 'root'|'';
		for @!children».[0] -> $child {
			return True if $child.title eq $name and $child.year == $year;
		}
		return False;
	}

	#| delete a node and all references to it
	method delete-node(Node $node) {
		$node.delete;
		Utils.cut($!root.children, $node);

		# give the node's children the parent of root if it doesnt have any parents
		for $node.children -> $child {
			if $child.parents.elems < 1 {
				$child.add-parents([$!root]);
			}
		}
		#.add-parents([$!root]) for $node.children;
		#for @!children.kv -> $i, $child {
			## if the node to delete is this node
			#if $child[0].title~$child[0].year eq $node.title~$node.year {
				## then move all the children to the root node if it doesnt create a duplicate
				#for $child[1..*-1] -> $element {
					#if $element ∉ @!children[0][1..*-1] {
						#@!children[0].append: $element;
					#}
				#}
				#@!children.splice($i, 1);
			## if its not, check all the children for the node to delete
			## (and remove it so that we dont have any references to non-existent nodes)
			#} else {
				#for $child[1..*-1].kv -> $j, $element {
					#if $element.title~$element.year eq $node.title~$node.year {
						#@!children[$i].splice($j, 1);
						#last;
					#}
				#}
			#}
		#}
	}

	#|[ detect whether the Graph is cyclic or not.
	#|  if it is, return the branch that causes the cycle ]
	method is-cyclic {
		my %visited;
		(%visited{$_} = False) for @!children».[0];
		for @!children -> $children {
			if not %visited{$children[0]} {
				my $ans = DFS($children[0], %visited, $children[1..*-1]);
				return $ans if $ans;
			}
		}
		return False;
	}

	#|[ depth first search all nodes ]
	sub DFS($current, %visited, $children) {
		%visited{$current} = True;
		for |$children -> $child {
			#say $child;
			if %visited{$child} {
				# find out which child node causes the loop
				return [$_, $child] if %visited{$_} for $child.children;
				return $child;
			} else {
				my $ans = DFS($child, %visited, $child.children);
				return $ans if $ans;
			}
		}
		%visited{$current} = False;
		return False;
	}

	#|[ go through each node and return the current node and its parent
	#| it gets returned as [parent, child] ]
	method nodes-parent(:$breadth-first) {
		my @queue = $[$!root, $!root];
		return sub {
			while @queue {
				#say @queue;
				my ($parent, $node) = pop @queue;
				if $node.children {
					$breadth-first
						?? @queue.unshift: |$node.children.map({$node, $_})
						!! @queue.push: |$node.children.map({$node, $_});
				}
				return $parent, $node;
			}
		};
	}


	#|[ returns an iterator of all nodes in this graph ]
	method nodes(:$breadth-first) {
		my @queue = $!root;
		return sub {
			while @queue {
				my $node = pop @queue;
				if $node.children {
					$breadth-first
					?? @queue.unshift: |$node.children
					!! @queue.push: |$node.children;
				}
				return $node;
			}
		};
	}

	#|[ returns an iterator of all nodes in this graph, but it never returns the same node more than once ]
	method nodes-unique(:$breadth-first) {
		my @queue = $!root;
		my @seen;
		return sub {
			while @queue {
				my $node = pop @queue;
				if $node.children {
					$breadth-first
					?? @queue.unshift: |$node.children
					!! @queue.push: |$node.children;
				}
				next if @seen ∋ $node;
				@seen.append: $node;
				return $node;
			}
		};
	}

	method gist() {
		return @!children;
	}
}

#my $tree = Tree.new(root=>Node.new(title=>'root', year=>0));
#$tree.add-node(Node.new(title=>'G1', year=>0), $tree.get-node('root'));
#$tree.add-node(Node.new(title=>'G1.5', year=>500), $tree.get-node('G1'));
#$tree.add-node(Node.new(title=>'G 2', year=>1000), $tree.get-node('G1'));
#$tree.add-node(Node.new(title=>'G3', year=>2000), $tree.get-node('G 2'));
#say $tree.gist;
