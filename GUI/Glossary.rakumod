use Utils;
use Graph;
use GUI::GlossaryRow;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Label;
use GUI::Gnome::Gtk3::Expander;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::ScrolledWindow;
use Gnome::Gtk3::Frame;

unit class Glossary is Gnome::Gtk3::Grid;

has Graph $.graph is required;
has Hash %!sections{Node};


submethod new ( |c ) {
  self.bless( :GtkGrid, |c);
}

submethod TWEAK ( ) {
	self.setup;
}

method setup() {
	my Supply $glossary-supply = Utils.glossary.Supply;
	$glossary-supply.tap(-> $/ {
		self.update-glossary($<node>, $<old-word>//Word, $<new-word>//Word);
	});
	self.create-glossary-groupings;

	self.show-all;
}

method create-glossary-groupings {
	# first remove all the rows
	my Gnome::Glib::List $list .= new( :native-object(self.get-children) );
	say $list.length;
	for 0..^ $list.length {
		self.remove-row($_);
	}

	# then add in the updated rows
	my $i = -1;
	my $nodes = $!graph.nodes-unique(:breadth-first);
	while my $node = $nodes() {
		my Gnome::Gtk3::Expander $expander .= new(:mnemonic('<big>'~$node.get-name~'</big>'~(' - '~$node.description if $node.description)));
		$expander.set-use-markup(True);
		my Gnome::Gtk3::Frame $frame .= new;
		my Gnome::Gtk3::Grid $container .= new;
		#my Gnome::Gtk3::Button $add-word .= new(:label('Add Word'));
		#$add-word.set-hexpand(True);
		#$add-word.register-signal(self, 'on-add-word-clicked', 'clicked', :$node);
		%!sections{$node}<grid> = Gnome::Gtk3::Grid.new;
		$container.attach(%!sections{$node}<grid>, 0, 0, 1, 1);
		#$container.attach($add-word, 0, 1, 1, 1);
		$frame.add($container);
		$expander.add($frame);
		self.attach($expander, 0, ++$i, 1, 1);
		self.update-glossary($node);
	}
}

method add-glossary-row(Node $node, Word $word, :$refresh) {
	my GlossaryRow $row .= new(:$node, :$word);

	# add the row to the main list
	%!sections{$node}<grid>.attach($row, 0, ++%!sections{$node}<row-num>, 1, 1);

	if $refresh {
		self.renumber-glossary-rows($node);
		%!sections{$node}<grid>.show-all;
	}
}

method renumber-glossary-rows(Node $node) {
#	say 'inside renumbering';
	my Gnome::Glib::List $list .= new( :native-object(%!sections{$node}<grid>.get-children) );
	my $i = 0;
	for ^$list.length {
#		say $_;
		if (my Gnome::Gtk3::Grid $row .= new(:native-object(%!sections{$node}<grid>.get-child-at(0,$_)))).is-valid {
#			say 'updating label';
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			#say $list.length;
			say 'removing row ', $_;
			#return if $_ == $list.length-1;
			%!sections{$node}<grid>.remove-row($_);
			self.renumber-glossary-rows($node);
			return;
		}
	}
}

method update-glossary(Node $node, Word $old-word=Word, Word $new-word=Word) {
#	say "updating { $old-word.word } to { $new-word.word } in { $node.get-name }";
	# remove old row contents
	my Gnome::Glib::List $list .= new(:native-object(%!sections{$node}<grid>.get-children));
	if not $old-word and not $new-word {
		for ^$list.length {
			# continuously remove the top row till all rows are gone
			%!sections{$node}<grid>.remove-row(0);
			--%!sections{$node}<row-num>;
		}

		# now add back all row contents - including newly added or changed rows
		for $node.words -> $word {
			say $word.word;
			self.add-glossary-row($node, $word);
		}
	}

	# update the glossary while Run! in going
	if $old-word and $new-word {
		my Gnome::Glib::List $list .= new(:native-object(%!sections{$node}<grid>.get-children));
		for ^$list.length {
			my Gnome::Gtk3::Grid $row .= new(:native-object(%!sections{$node}<grid>.get-child-at(0, $_)));
			my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
			my Gnome::Gtk3::Entry $root .= new(:native-object($box.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(2,0)));
			my Gnome::Gtk3::Entry $path .= new(:native-object($box.get-child-at(4,0)));
			say ('old word:', $new-word.word, $new-word.root-word, $new-word.definition, $new-word.path».get-name.join(' » ')).join(' ');
			say ('entries:', $word.get-text, $root.get-text, $definition.get-text, $path.get-text).join(' ');
			if $root.get-text eq $new-word.root-word and
			  $definition.get-text eq $new-word.definition and
			  $path.get-text eq $new-word.path».get-name.join(' » ') {
				say 'updating ', $old-word.word, ' to ', $new-word.word;
				$word.set-text($new-word.word);
				#	$definition.set-text($new-word.definition);
				self.renumber-glossary-rows($node);
				return;
			}
		}
		# if we got to here then the word doesn't exist yet, and we need to add it
		self.add-glossary-row($node, $new-word);
#	} elsif $old-word and not $new-word {
#		%!sections{$node}<grid>.remove-row($_) for ^$list.length
	} elsif not $old-word and $new-word {
		self.add-glossary-row($node, $new-word);
	}

	# add column numbers
	say 'renumbering';
	self.renumber-glossary-rows($node);

	%!sections{$node}<grid>.show-all;
}
