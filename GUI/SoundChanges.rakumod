use Utils;
use PhoneticEvolver;
use Graph;
use Parser;
use ActionsSplit;
use GUI::CoinageRow;
use GUI::GlossaryRow;
use Gnome::Gtk3::Paned;
use Gnome::Gtk3::Box;
use Gnome::Gtk3::ScrolledWindow;
use Gnome::Gtk3::Adjustment;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::ToolButton;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::Image;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Dialog;
use Gnome::Gtk3::FileChooser;
use Gnome::Gtk3::FileChooserDialog;
use Gnome::Gtk3::FileFilter;
use Gnome::Gtk3::MessageDialog;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::Notebook;
use Gnome::Gtk3::Builder;
use Gnome::Gtk3::Enums;
use Gnome::Gtk3::CssProvider;
use Gnome::Gtk3::StyleProvider;
use Gnome::Gtk3::StyleContext;
use Gnome::Gdk3::Events;
use Gnome::GObject::Value;
use Gnome::GObject::Type;
use Gnome::Glib::List;
use UUID;


unit class SoundChanges is Gnome::Gtk3::Grid;

has Gnome::Gtk3::Notebook $!notebook;
has Gnome::Gtk3::Grid $!grid;
has Graph $.graph is required;
has Node $.node is required;
has $.main is required;
my $sc-rows = -1;
my Array $grid-items;
# the file to save the sc to
has Str $!file;
my Str $id;

my Gnome::Gtk3::Grid $glossary-grid;
my Gnome::Gtk3::Grid $coinages-grid;

has Supplier $.supplier .= new;
has Supply $.supply = $!supplier.Supply;
has Bool %!slash{Gnome::Gtk3::Button};
has Gnome::Gtk3::Entry $!current-entry;


submethod new ( |c ) {
  self.bless( :GtkGrid, |c);
}

submethod TWEAK ( ) {
	self.setup if $!graph;
}

method setup() {
	$id = Str(UUID.new);
	my Str $template = 'SoundChanges.glade'.IO.slurp;
	$template ~~ s:g/\$id/$id/;
	my Gnome::Gtk3::Builder $builder .= new;
	my Gnome::Glib::Error $e = $builder.add-from-string($template);
	die $e.message if $e.is-valid;
	#self.attach(Gnome::Gtk3::Paned.new(:build-id("sc-tab-template-$id")), 0, 0, 1, 1);
	self.attach(Gnome::Gtk3::Notebook.new(:build-id("node-top-$id")), 0, 0, 1, 1);
	$!grid = Gnome::Gtk3::Grid.new(:build-id("sc-sound-changes-$id"));
	$!notebook = Gnome::Gtk3::Notebook.new(:build-id("tree"));
	$glossary-grid .= new(:build-id("glossary-grid-$id"));
	$coinages-grid .= new(:build-id("coinages-grid-$id"));
	say "sc: ", $id;

	# reduce padding of buttons so that the sound changes shows up properly
	my Gnome::Gtk3::CssProvider $style .= new;
	my $context = Gnome::Gtk3::StyleContext.new(:native-object(self.get-style-context()));
	$style.load-from-data('
	paned notebook grid button {
		padding: 0px;
	}
	paned notebook fixed button {
		padding: 0px;
	}
	paned notebook grid label {
		border-left: 1px solid lightgray;
		border-top: 1px solid lightgray;
	}
	paned notebook grid grid *:nth-child(2n) {
		border-right: 1px solid gray;
	}
	');
	my Gnome::Gdk3::Screen $screen .= new;
	$context.add-provider-for-screen($screen, $style, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);


	# show all the sound changes that this node has
	self.add-sound-changes($!node.sound-changes);
	# show one row of words
	#self.on-sc-new-clicked;

	my Hash $handlers = %(
		#:on-sc-export-clicked(self),
		#:on-sc-import-clicked(self),
		#:on-sc-new-clicked(self),
		#:on-close-tab(self),
		:on-sc-clicked(self),
	);
	$builder.connect-signals-full($handlers);
	my Gnome::Gtk3::Button $close-tab .= new(:build-id("sc-close-tab-$id"));
	$close-tab.register-signal(self, 'on-close-tab', 'clicked');
	my Gnome::Gtk3::ToolButton $export .= new(:build-id("sc-export-$id"));
	$export.register-signal(self, 'on-sc-export-clicked', 'clicked');
	my Gnome::Gtk3::ToolButton $import .= new(:build-id("sc-import-$id"));
	$import.register-signal(self, 'on-sc-import-clicked', 'clicked');
	my Gnome::Gtk3::ToolButton $new-row .= new(:build-id("sc-new-$id"));
	$new-row.register-signal(self, 'on-sc-new-clicked', 'clicked');

	# map happens when the widget gets shown
	#self.register-signal(self, 'update', 'map');
	
	# set up a signal to update the entry
	$!supply.tap({
		my Str $text = $!current-entry.get-text;
		my $comb = $text.NFD».chr;
		my Int $cursor-pos = $!current-entry.g-object-get-property('cursor-position', Gnome::GObject::Value.new(:init(G_TYPE_INT))).value-get-int;
		my Str $new-text;
		# if there isn't any text yet in the entry
		if not $text {
			$new-text = $_;
		# if cursor is at beginning of entry
		} elsif $cursor-pos == 0 {
			$new-text = $_ ~ $text;
		# if the cursor is at the end of the entry
		} elsif $cursor-pos == $comb.elems {
			$new-text = $text ~ $_;
		} else {
			$new-text = $comb[^$cursor-pos].join ~ $_ ~ $comb[$cursor-pos..*-1].join;
		}
		$!current-entry.set-text($new-text);
	});


	my Gnome::Gtk3::ToolButton $add-row .= new(:build-id("coinages-add-$id"));
	$add-row.register-signal(self, 'add-coinages-row', 'clicked', :refresh);

	self.update;
}

method update {
	self.update-glossary;
	self.update-coinage;
}

method on-sc-new-clicked(:$lhs-text, :$rhs-text, :$slash-toggled, :$whs-text, :$dont-refresh) {
	my Gnome::Gtk3::Grid $row .= new;
	# so that it doesn't cover the scroll bar…
	$row.set-margin-end(15);

	# create all widgets
	my Gnome::Gtk3::Label $row-num-label .= new(:text(''));
	my Gnome::Gtk3::Entry $lhs .= new;
	my Gnome::Gtk3::Label $arrow .= new(:text('➜'));
	my Gnome::Gtk3::Entry $rhs .= new;
	my Gnome::Gtk3::Button $slash .= new(:label('/'));
	my Gnome::Gtk3::Entry $whs .= new;
	my Gnome::Gtk3::Button $delete-row-button .= new;

	# set the widget values
	$lhs.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$rhs.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$lhs.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	$rhs.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	#$word-entry.set-icon-tooltip-markup;
	#$definition-entry.set-icon-tooltip-markup;
	$lhs.set-text($lhs-text//'');
	$rhs.set-text($rhs-text//'');
	$whs.set-text($whs-text//'');
	$lhs.set-placeholder-text('LHS');
	$rhs.set-placeholder-text('RHS');
	$whs.set-placeholder-text('WHS');
	self.on-sc-entry-type(:_widget($lhs));
	self.on-sc-entry-type(:_widget($rhs));
	$slash.set-relief(GTK_RELIEF_NONE);
	$row-num-label.set-width-chars(3);
	$delete-row-button.set-image(Gnome::Gtk3::Image.new(:icon-name<user-trash>));
	$lhs.set-hexpand(True);
	$rhs.set-hexpand(True);
	$whs.set-hexpand(True);

	# add all items to the main grid
	$row.attach($row-num-label, 		0,0, 1, 1);
	$row.attach($lhs, 					1,0, 1, 1);
	$row.attach($arrow, 				2,0, 1, 1);
	$row.attach($rhs, 					3,0, 1, 1);
	$row.attach($slash, 				4,0, 1, 1);
	$row.attach($whs, 					5,0, 1, 1);
	$row.attach($delete-row-button, 	6,0, 1, 1);

	++$sc-rows;
	$!grid.attach($row, 0, $sc-rows, 1, 1);
	#$grid-items.append: $row;

	#$!grid.container-foreach(self, 'renumber-columns');

	$lhs.register-signal(self, 'on-sc-entry-type', 'changed');
	$rhs.register-signal(self, 'on-sc-entry-type', 'changed');
	$lhs.register-signal(self, 'on-focus-in-event', 'focus-in-event');
	$rhs.register-signal(self, 'on-focus-in-event', 'focus-in-event');
	$whs.register-signal(self, 'on-focus-in-event', 'focus-in-event');
	$slash.register-signal(self, 'on-slash-clicked', 'clicked');
	$delete-row-button.register-signal(self, 'on-delete-row', 'clicked', :$row);

	self.on-slash-clicked(:_widget($slash)) if $slash-toggled;
	%!slash{$slash} = ($slash-toggled ?? True !! False);

	# make sure to show the update on the screen
	if not $dont-refresh {
		self.renumber-columns;
		$!grid.show-all;
	}

	# scroll the window to the bottom
	my Gnome::Gtk3::ScrolledWindow $scroll .= new(:build-id("sc-scroll-$id"));
	my Gnome::Gtk3::Adjustment $adj .= new(:native-object($scroll.get-vadjustment));
	$adj.set-value($adj.get-upper+$adj.get-page-size);
}

method on-delete-row(:_widget($button), :$row) {
	$row.destroy;
	self.renumber-columns;
	$!grid.show-all;
}
method on-focus-in-event(N-GdkEventFocus $event, :_widget($w)) {
	$!current-entry = $w;
}
method on-slash-clicked(:_widget($w)) {
	if %!slash{$w} {
		$w.set-label('//');
	} else {
		$w.set-label('/');
	}
}

method on-sc-clicked(:_widget($w)) {
	my Gnome::Gtk3::Button $b .= new(:native-object($w));
	if $b.get-label {
		try $!supplier.emit: $b.get-label.lc;
	} else {
		try $!supplier.emit: Gnome::Gtk3::Label.new(:native-object($b.get-child)).get-label.lc~' ';
		
	}
}

method renumber-columns {
	my Gnome::Glib::List $list .= new( :native-object($!grid.get-children) );
	my $i = 0;
	say $list.length;
	for 0..^ $list.length {
	#while $list.is-valid {
		#say $_, ' ', $i;
		if (my Gnome::Gtk3::Grid $row .= new(:native-object($!grid.get-child-at(0,$_)))).is-valid {
		#if (my Gnome::Gtk3::Grid $row .= new(:native-object($list.data))).defined {
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			#say $list.length;
			say 'removing row ', $_;
			#return if $_ == $list.length-1;
			$!grid.remove-row($_);
			self.renumber-columns;
			return;
		}
		#$list .= next;
	}
}

method get-sc-rows() {
	my Str $file-contents;
	my Int $i = 1;
	my Gnome::Glib::List $list .= new( :native-object($!grid.get-children) );
	say $list.length;

	for ^$list.length -> $i {
		next unless (my Gnome::Gtk3::Grid $row .= new(:native-object($!grid.get-child-at(0,$i)))).is-valid;
		#say $i;
		#my Gnome::Gtk3::Grid $row .= new(:native-object($!grid.get-child-at(0, $i)));
		my Gnome::Gtk3::Entry $lhs 	.= new(:native-object($row.get-child-at(1, 0)));
		my Gnome::Gtk3::Entry $rhs 		.= new(:native-object($row.get-child-at(3, 0)));
		my Gnome::Gtk3::Entry $whs 			.= new(:native-object($row.get-child-at(5, 0)));

		# make sure none of the entries are empty
		if $lhs.get-text.trim.chars < 1 {
			Utils.show-dialog("The entry at row {$i+1} at the column <i>LHS</i> is empty,\nand it's not allowed to be empty.");
			return False;
		} elsif $rhs.get-text.trim.chars < 1 {
			Utils.show-dialog("The entry at row {$i+1} at the column <i>RHS</i> is empty,\nand it's not allowed to be empty.");
			return False;
		}

		$file-contents ~= $lhs.get-text~' → '~$rhs.get-text~(' / '~$whs.get-text if $whs.get-text.trim)~"\n";
	}
	return $file-contents;
}

method on-sc-export-clicked() {
	my $file-contents = self.get-sc-rows;
	return if $file-contents eq '';

	#save sc to a file
	self.pick-file-and-save($file-contents);
}

#| open a file and load it into the current tab
method on-sc-import-clicked() {
	my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  :title("Open File"), :parent(self),
	  :action(GTK_FILE_CHOOSER_ACTION_OPEN),
	  :button-spec( [
		  "_Cancel", GTK_RESPONSE_CANCEL,
		  "_Open", GTK_RESPONSE_ACCEPT,
		]
	  )
	);
	my Gnome::Gtk3::FileFilter $file-filter .= new;
	$file-filter.add-pattern('*.scf');
	$dialog.set-filter($file-filter);

	my $response = $dialog.gtk-dialog-run;
	$dialog.gtk-widget-hide;
	if ($response ~~ GTK_RESPONSE_ACCEPT) {
		$!file = $dialog.get-filename;
		note "Opening file $!file";
		self.add-sound-changes($!file.IO.slurp);
	}
}

method add-sound-changes($sound-changes) {
	my Array[List] $sc;
	try $sc = Parser.parse($sound-changes, actions=>Split.new).made;
	for @$sc {
		say $_;
		self.on-sc-new-clicked(:lhs-text(.[0]), :rhs-text(.[1]), :slash-toggled(.[2]), :whs-text(.[3]), :dont-refresh);
	}
	self.renumber-columns;
	$!grid.show-all;
}

method pick-file-and-save($file-contents) {
	my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  :title("Open File"), :parent(self),
	  :action(GTK_FILE_CHOOSER_ACTION_SAVE),
	  :button-spec( [
		  "_Cancel", GTK_RESPONSE_CANCEL,
		  "_Open", GTK_RESPONSE_ACCEPT
		]
	  )
	);
	my Gnome::Gtk3::FileFilter $file-filter .= new;
	$file-filter.add-pattern('*.scf');
	$dialog.set-filter($file-filter);
	$dialog.set-do-overwrite-confirmation(1);
	$dialog.set_current_name("sound-changes.scf");
	my $response = $dialog.gtk-dialog-run;
	$dialog.gtk-widget-hide;
	if ($response ~~ GTK_RESPONSE_ACCEPT) {
	  $!file = $dialog.get-filename;
	  note "Saving to file $!file";
	  $!file.IO.spurt: $file-contents;
	}
	$dialog.gtk-widget-destroy;
}

method on-sc-entry-type(:_widget($entry)) {
	if $entry.get-text.trim.chars < 1 {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	} else {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, Nil);
	}
}

method save-sound-changes {
	my $sc-rows = self.get-sc-rows;
	return False if $sc-rows eqv False;
	my Str $file-contents = $sc-rows // '';
	# don't close tab if there are empty entries
	my Gnome::Glib::List $list .= new(:native-object($!grid.get-children));
	if $list.length > 0 {
		return if not $file-contents;
	}
	#| were the sound changes edited?
	my Bool $changed = $!node.sound-changes ne $file-contents;
	$!node.sound-changes = $file-contents;
	# change all words in all parents to unprocessed so that the new sound changes get applied
	if $changed {
		for $!node.parents -> $parent {
			for $parent.words -> $word {
				$word.processed = False;
				my Int $index = 0;
				for $word.path.kv -> $i, $path { $index = $i if $path.id eq $parent.id };
				$word.starting-node = $index;
			}
		}
	}
	return True;
}

method on-close-tab() {
	# save the sound changes first
	return if not self.save-sound-changes;
	#my Gnome::Gtk3::Notebook $main .= new(:build-id("node-top-$id"));
	#$main.destroy;
	$!notebook.remove-page($!notebook.get-current-page);
	$!main.remove-tab($!node.get-name);
}

method file() {
	return $!file.IO.basename;
}


#-----------#
# word bank #
#-----------#

######################
#### GLOSSARY TAB ####
######################

my Int $glossary-rows = -1;
method add-glossary-row($word, :$refresh) {
	my GlossaryRow $row .= new(:$!node, :$word);

	# add the row to the main list
	$glossary-grid.attach($row, 0, ++$glossary-rows, 1, 1);

	if $refresh {
		self.renumber-glossary-columns();
		$coinages-grid.show-all;
	}
}

method renumber-glossary-columns() {
	my Gnome::Glib::List $list .= new( :native-object($glossary-grid.get-children) );
	my $i = 0;
	#say $list.length;
	for ^$list.length {
		with (my $grid = $glossary-grid.get-child-at(0,$_)) {
			#say $i;
			my Gnome::Gtk3::Grid $row .= new(:native-object($grid));
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			$glossary-grid.remove-row($_);
			self.renumber-glossary-columns;
			return;
		}
	}
}

method update-glossary(Word $old-word=Word, Word $new-word=Word) {
	say 'in here';
	if $old-word and $new-word {
		my Gnome::Glib::List $list .= new(:native-object($glossary-grid.get-children));
		for ^$list.length {
			my Gnome::Gtk3::Grid $row .= new(:native-object($glossary-grid.get-child-at(0, $_)));
			my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
			my Gnome::Gtk3::Entry $root .= new(:native-object($box.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(2,0)));
			my Gnome::Gtk3::Entry $path .= new(:native-object($box.get-child-at(4,0)));
			say ('old word:', $new-word.word, $new-word.root-word, $new-word.definition, $new-word.path».get-name.join(' » ')).join(' ');
			say ('entries: ', $word.get-text, $root.get-text, $definition.get-text, $path.get-text).join(' ');
			if $root.get-text eq $new-word.root-word and
			  $definition.get-text eq $new-word.definition and
			  $path.get-text eq $new-word.path».get-name.join(' » ') {
				say 'updating ', $word.get-text, ' to ', $new-word.word, '. they are ', ($word.get-text eq $new-word.word ?? 'the same' !! 'different');
				$word.set-text($new-word.word);
#				$definition.set-text($new-word.definition);
				self.renumber-glossary-columns;
				return;
			}
		}
		# if we got to here then the word doesnt exist yet, and we need to add it
		self.add-glossary-row($new-word);
	} else {
		say 'updading glossary';
		# remove old row contents
		my Gnome::Glib::List $list .= new(:native-object($glossary-grid.get-children));
		for ^$list.length {
			# continuously remove the top row till all rows are gone
			$glossary-grid.remove-row(0);
			say 'removed row: ', $_;
		}

		# now add back all row contents - including newly added or changed rows
		$glossary-rows = -1;
		for $!node.words -> $word {
			say "Word {$word.root-word} is set to {$word.processed}";
			self.add-glossary-row($word);
		}
	}

	# add column numbers
	self.renumber-glossary-columns;

	$glossary-grid.show-all;
}


######################
#### COINAGES TAB ####
######################

my Int $coinages-rows = -1;
method add-coinages-row(:$word, :$definition, :$refresh) {
	my CoinageRow $row .= new(:$!node, :$word, :$definition);

	# add the row to the main list
	$coinages-grid.attach($row, 0, ++$coinages-rows, 1, 1);

	if $refresh {
		self.renumber-coinages-columns();
		$coinages-grid.show-all;
	}
}

method renumber-coinages-columns() {
	my Gnome::Glib::List $list .= new( :native-object($coinages-grid.get-children) );
	my $i = 0;
	for ^$list.length {
		#say $_;
		if (my Gnome::Gtk3::Grid $row .= new(:native-object($coinages-grid.get-child-at(0,$_)))).is-valid {
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			#say $list.length;
			say 'removing row ', $_;
			#return if $_ == $list.length-1;
			$coinages-grid.remove-row($_);
			self.renumber-coinages-columns();
			return;
		}
	}
}

method update-coinage(Word $old-word=Word, Word $new-word=Word, Int $row-num=0) {
	# remove old row contents
	my Gnome::Glib::List $list .= new(:native-object($coinages-grid.get-children));
	if not $old-word and not $new-word {
		for ^$list.length {
			# continuously remove the top row till all rows are gone
			$coinages-grid.remove-row(0);
			--$coinages-rows;
		}

		# now add back all row contents - including newly added or changed rows
		for $!node.coinages -> $word {
			self.add-coinages-row(:word($word.root-word), :definition($word.root-definition));
		}
	}

	if $old-word and $new-word and $row-num {
		say "list = {$list.length}, {$row-num}";
		my Gnome::Gtk3::Grid $row .= new(:native-object($coinages-grid.get-child-at(0, $row-num))); # <- doesnt get an existing row
		my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0))); # <- so this doesnt get an existing child
		my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
		my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(1,0)));
		#my Gnome::Gtk3::Entry $lexicon .= new(:native-object($box.get-child-at(2,0)));
		if $word.get-text eq $old-word.root-word and
		  $definition.get-text eq $old-word.definition {
			say 'updating ', $old-word.word, ' to ', $new-word.word;
			$word.set-text($new-word.root-word);
			$definition.set-text($new-word.root-definition);
			if (my Gnome::Gtk3::Button $check .= new(:native-object($row.get-child-at(3,0)))).is-valid {
				$check.destroy;
			}
		}
	} elsif $old-word and not $new-word {
		# its 1-indexed
		$coinages-grid.remove-row($row-num-1);
	} elsif not $old-word and $new-word and $row-num {
		# when i need to add a new word, don't add it twice where i manually added the word from
		my Gnome::Gtk3::Grid $row .= new(:native-object($coinages-grid.get-child-at(0, $row-num)));
		if not $row.is-valid {
			self.add-coinages-row(:word($new-word.word), :definition($new-word.definition));
		} else {
			my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
			my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(1,0)));
			#my Gnome::Gtk3::Entry $lexicon .= new(:native-object($box.get-child-at(2,0)));
			if $word.get-text eq $new-word.word and
			  $definition.get-text eq $new-word.definition {
				say 'adding ', $new-word.word, ' at ', $row-num;
				if (my Gnome::Gtk3::Button $check .= new(:native-object($row.get-child-at(3,0)))).is-valid {
					$check.destroy;
				}
			}
		}
	}


	# add column numbers
	self.renumber-coinages-columns;

	$coinages-grid.show-all;
}
