#TL:1:Gnome::Gtk3::Expander:

use v6;
use NativeCall;

use Gnome::N::NativeLib;
use Gnome::N::N-GObject;

use Gnome::Gtk3::Bin;
use Gnome::Gtk3::Enums;
use Gnome::Gtk3::Label;

#-------------------------------------------------------------------------------
# See /usr/include/gtk-3.0/gtk/gtkexpander.h
# https://developer.gnome.org/gtk3/stable/GtkExpander.html
unit class Gnome::Gtk3::Expander:auth<github:MARTIMM>;
also is Gnome::Gtk3::Bin;

#-------------------------------------------------------------------------------
my Bool $signals-added = False;
#-------------------------------------------------------------------------------
submethod BUILD ( *%options ) {

  # add signal info in the form of group<signal-name>.
  # groups are e.g. signal, event, nativeobject etc
  $signals-added = self.add-signal-types( $?CLASS.^name,
    :w0<activate>,
  ) unless $signals-added;

  # prevent creating wrong widgets
  if self.^name eq 'Gnome::Gtk3::Expander' or %options<GtkExpander> {

    if self.is-valid { }

    # process all named arguments
    elsif %options<native-object>:exists or %options<widget>:exists or
      %options<build-id>:exists { }

    else {
      my $no;

      if %options<mnemonic>.defined {
        $no = _gtk_expander_new_with_mnemonic(%options<mnemonic>);
      }

      else {
        $no = _gtk_expander_new();
      }

      self.set-native-object($no);
    }

    # only after creating the native-object, the gtype is known
    self.set-class-info('GtkExpander');
  }
}

#-------------------------------------------------------------------------------
# no pod. user does not have to know about it.
method _fallback ( $native-sub is copy --> Callable ) {

  my Callable $s;
  try { $s = &::("gtk_expander_$native-sub"); };
  try { $s = &::("gtk_$native-sub"); } unless ?$s;
  try { $s = &::($native-sub); } if !$s and $native-sub ~~ m/^ 'gtk_' /;

  self.set-class-name-of-sub('GtkExpander');
  $s = callsame unless ?$s;

  $s
}

sub _gtk_expander_new ( --> N-GObject )
  is native(&gtk-lib)
  is symbol('gtk_expander_new')
  { * }

#-------------------------------------------------------------------------------
#TM:0:_gtk_expander_new_with_mnemonic:
#`{{
=begin pod
=head2 [[gtk_] expander_] new_with_mnemonic

Creates a new B<Gnome::Gtk3::Expander> containing a label.
If characters in I<label> are preceded by an underscore, they are underlined.
If you need a literal underscore character in a label, use “__” (two
underscores). The first underlined character represents a keyboard
accelerator called a mnemonic.
Pressing Alt and that key activates the expander.

Returns: a new B<Gnome::Gtk3::Expander>

  method gtk_expander_new_with_mnemonic ( Str $label --> N-GObject  )

=item Str $label; The text of the expander, with an underscore in front of the mnemonic character

=end pod
}}

sub _gtk_expander_new_with_mnemonic ( Str $label --> N-GObject )
  is native(&gtk-lib)
  is symbol('gtk_expander_new_with_mnemonic')
  { * }


sub gtk_expander_set_expanded ( N-GObject $expander, Bool $expanded )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_expanded ( N-GObject $expander )
  returns Bool
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_label ( N-GObject $expander, Str $label )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_label ( N-GObject $expander )
  returns Str
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_use_underline ( N-GObject $expander, Bool $use_underline )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_use_underline ( N-GObject $expander )
  returns Bool
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_use_markup ( N-GObject $expander, Bool $use_markup )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_use_markup ( N-GObject $expander )
  returns Bool
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_label_widget ( N-GObject $expander, N-GObject $label_widget )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_label_widget ( N-GObject $expander )
  returns N-GObject
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_label_fill ( N-GObject $expander, Bool $label_fill )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_label_fill ( N-GObject $expander )
  returns Bool
  is native(&gtk-lib)
  { * }

sub gtk_expander_set_resize_toplevel ( N-GObject $expander, Bool $resize_toplevel )
  is native(&gtk-lib)
  { * }

sub gtk_expander_get_resize_toplevel ( N-GObject $expander )
  returns Bool
  is native(&gtk-lib)
  { * }



