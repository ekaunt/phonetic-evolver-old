use Utils;
use Graph;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::Image;

unit class GlossaryRow is Gnome::Gtk3::Grid;

has Word $.Word;
our %.sections;

has Gnome::Gtk3::Label $.lbl-row-num;
has Gnome::Gtk3::Entry $.word;
has Gnome::Gtk3::Entry $.root;
has Gnome::Gtk3::Entry $.definition;
has Gnome::Gtk3::Entry $.root-definition;
has Gnome::Gtk3::Entry $.coined;
has Gnome::Gtk3::Entry $.path;
has Gnome::Gtk3::Grid  $.box;

submethod new(|c) {
	self.bless(:GtkGrid, |c);
}
submethod BUILD(Node :$node, Word :$word) {
	$!Word = $word;

	# create all widgets
	$!lbl-row-num .= new(:text((++self.sections{$node}).Str));
	$!word .= new;
	$!root .= new;
	$!definition .= new;
	$!root-definition .= new;
	$!coined .= new;
	$!path .= new;
	$!box .= new;

	# set the widget values
	$!word.set-text($word.word//'');
	$!root.set-text($word.root-word//'');
	$!definition.set-text($word.definition//'');
	$!root-definition.set-text($word.root-definition//'');
	$!coined.set-text($word.path[0].get-name//'');
	$!path.set-text($word.path».get-name.join(' » ')//'');
	$!lbl-row-num.set-width-chars(5);
	$!box.set-hexpand(True);
	$!box.set-column-homogeneous(True);
	$!box.set-column-spacing(10);


	# add entries to box
	$!box.attach($!word,       				0,0,1,1);
	$!box.attach($!root,	 				1,0,1,1);
	$!box.attach($!definition,				2,0,1,1);
	$!box.attach($!coined,					3,0,1,1);
	$!box.attach($!path,					4,0,1,1);
	$!box.attach($!root-definition,			5,0,1,1);

	# add everything to the row
	self.attach($!lbl-row-num, 	0,0,1,1);
	self.attach($!box, 			1,0,1,1);

	$!Word.will-i-be-changed.Supply.tap(-> $new-word {
		say "checking if {$!Word.id} eq {$new-word.id}";
		say "they are equal";
		self.mark-unprocessed-word($new-word);
	});
	self.mark-unprocessed-word($!Word);
}

method mark-unprocessed-word(Word:D $new-word) {
#	if $new-word.processed eqv False {
#		self.set-name("changed");
#	} else {
#		self.set-name("");
#	}
	if $new-word.will-root-word-be-changed {
		$!word.set-name("changed");
	} else {
		$!word.set-name("");
	}
	if $new-word.will-root-definition-be-changed {
		$!definition.set-name("changed");
	} else {
		$!definition.set-name("");
	}
}
