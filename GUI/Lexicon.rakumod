use Utils;
use PhoneticEvolver;
use Graph;
use Gnome::Gtk3::Box;
use Gnome::Gtk3::ScrolledWindow;
use Gnome::Gtk3::Adjustment;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::Image;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Dialog;
use Gnome::Gtk3::FileChooser;
use Gnome::Gtk3::FileChooserDialog;
use Gnome::Gtk3::FileFilter;
use Gnome::Gtk3::MessageDialog;
use Gnome::Gtk3::Notebook;
use Gnome::Gtk3::Builder;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::ToolButton;
use UUID;


unit class Lexicon is Gnome::Gtk3::Grid;

has $.main is required;
has Gnome::Gtk3::Notebook $!notebook;
#has Gnome::Gtk3::Grid $tab-header .= new(:text(''));
has Gnome::Gtk3::Label $.tab-name is rw;
has Gnome::Gtk3::Grid $!grid;
has PhoneticEvolver $.pe is required;
my $lexicon-rows = -1;
my Array $grid-items;
# the file to save the lexicon to
has Str $!file;
my Str $id;

submethod new ( |c ) {
  self.bless( :GtkGrid, |c);
}

submethod TWEAK ( ) {
	self.setup;
}

method setup() {
	$id = Str(UUID.new);
	## create the menu bar
	#my Gnome::Gtk3::Grid $menu .= new;
	#$menu.set-column-spacing(20);
	#my Gnome::Gtk3::Button $open .= new;
	#$open.set-image(Gnome::Gtk3::Image.new(:icon-name<document-new>));
	#$open.register-signal(self, 'on-open-clicked', 'clicked');
	#my Gnome::Gtk3::Button $load .= new;
	#$load.set-image(Gnome::Gtk3::Image.new(:icon-name<document-open>));
	#$load.register-signal(self, 'on-load-clicked', 'clicked');
	#my Gnome::Gtk3::Button $save .= new;
	#$save.set-image(Gnome::Gtk3::Image.new(:icon-name<document-save>));
	#$save.register-signal(self, 'on-save-clicked', 'clicked');
	#my Gnome::Gtk3::Button $save-as .= new;
	#$save-as.set-image(Gnome::Gtk3::Image.new(:icon-name<document-save-as>));
	#$save-as.register-signal(self, 'on-save-as-clicked', 'clicked');

	#$menu.attach($open, 	0, 0, 1, 1);
	#$menu.attach($load, 	1, 0, 1, 1);
	#$menu.attach($save, 	2, 0, 1, 1);
	#$menu.attach($save-as, 	3, 0, 1, 1);

	## create the header for the grid
	#my Gnome::Gtk3::Grid $box .= new;
	#my Gnome::Gtk3::Label $box-left .= new(:text(''));
	#$box-left.set-width-chars(5);
	#my Gnome::Gtk3::Label $box-word .= new(:text('Word'));
	#$box-word.set-hexpand(True);
	#my Gnome::Gtk3::Label $box-definition .= new(:text('Definition'));
	#$box-definition.set-hexpand(True);
	#my Gnome::Gtk3::Label $box-generation-coined .= new(:text('Generation coined'));
	#$box-generation-coined.set-hexpand(True);
	#my Gnome::Gtk3::Label $box-right .= new(:text(''));
	#$box-left.set-width-chars(5);

	#$box.attach($box-left, 0, 0, 1, 1);
	#$box.attach($box-word, 1, 0, 1, 1);
	#$box.attach($box-definition, 2, 0, 1, 1);
	#$box.attach($box-generation-coined, 3, 0, 1, 1);
	#$box.attach($box-right, 4, 0, 1, 1);

	##create the 'add word' button
	#my Gnome::Gtk3::Button $add-word-button .= new(:label('Add Word'));
	#$add-word-button.register-signal(self, 'add-lexicon-row', 'clicked');
	#$add-word-button.set-hexpand(True);

	##create the container for the main grid
	#my Gnome::Gtk3::ScrolledWindow $container .= new;
	#$container.set-propagate-natural-height(True);

	#$container.gtk-container-add($!grid);

	#self.set-hexpand(True);
	#self.set-vexpand(True);
	#self.attach($menu, 				0, 0, 1, 1);
	#self.attach($box, 				0, 1, 1, 1);
	#self.attach($container, 		0, 2, 1, 1);
	#self.attach($add-word-button, 	0, 3, 1, 1);

	##self.on-load-clicked();

	# add the new tab
	#my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  #:title("Open File"), :parent(self),
	  #:action(GTK_FILE_CHOOSER_ACTION_OPEN),
	  #:button-spec( [
		  #"_Cancel", GTK_RESPONSE_CANCEL,
		  #"_Open", GTK_RESPONSE_ACCEPT,
		#]
	  #)
	#);

	#my $response = $dialog.gtk-dialog-run;
	#$dialog.gtk-widget-hide;
	#if ($response ~~ GTK_RESPONSE_ACCEPT) {
		my Str $template = 'Lexicon.glade'.IO.slurp;
		$template ~~ s:g/\$id/$id/;
		my Gnome::Gtk3::Builder $builder .= new;
		my Gnome::Glib::Error $e = $builder.add-from-string($template);
		die $e.message if $e.is-valid;
		self.attach(Gnome::Gtk3::Box.new(:build-id("lexicon-tab-template-{$id}")), 0, 0, 1, 1);
		$!grid = Gnome::Gtk3::Grid.new(:build-id("grid-{$id}"));
		$!notebook = Gnome::Gtk3::Notebook.new(:build-id("lexicon"));
		#say $!grid;
		#say $!grid.is-valid;
		say 'lexicon: ', $id;

		#$!file = "new";
		#note "Opening file $!file";
		#$!tab-name.set-text('new');

		#my Array[Pair] $lexicon = $!pe.parse-root-word-list: $!file.IO.slurp;
		#for @$lexicon {
			#say $_;
			#self.add-lexicon-row(:word(.key), :definition(.value{'definition'}), :gen-coined(.value{'path'}), :dont-refresh);
		#}
		# show one row of words
		#self.add-lexicon-row;

		my Gnome::Gtk3::ToolButton $delete .= new(:build-id("lexicon-delete-$id"));
		$delete.register-signal(self, 'on-delete-clicked', 'clicked');
		my Gnome::Gtk3::ToolButton $import .= new(:build-id("lexicon-load-$id"));
		$import.register-signal(self, 'on-load-clicked', 'clicked');
		my Gnome::Gtk3::ToolButton $export .= new(:build-id("lexicon-save-as-$id"));
		$export.register-signal(self, 'on-save-as-clicked', 'clicked');
		my Gnome::Gtk3::ToolButton $rename .= new(:build-id("lexicon-rename-$id"));
		$rename.register-signal(self, 'on-rename-clicked', 'clicked');
		my Gnome::Gtk3::ToolButton $add-word .= new(:build-id("lexicon-add-word-$id"));
		$add-word.register-signal(self, 'add-lexicon-row', 'clicked');

		# map happens when the widget gets shown
		#self.register-signal(self, 'update-lexicon', 'map');
		#my Hash $handlers = %(
		#);
		#$builder.connect-signals-full($handlers);
	#}

	#self.update-lexicon;
}

method add-lexicon-row(:$word, :$definition, :$gen-coined, :$dont-refresh) {
	++$lexicon-rows;
	# create a GtkBox and put the entries within it
	# put into the $!grid [label, GtkBox, delete] in this order
	my Gnome::Gtk3::Grid $row .= new;
	my Gnome::Gtk3::Grid $box .= new;

	# create all widgets
	my Gnome::Gtk3::Label $row-num-label .= new(:text(''));
	my Gnome::Gtk3::Entry $word-entry .= new;
	my Gnome::Gtk3::Entry $definition-entry .= new;
	my Gnome::Gtk3::Entry $gen-appeared-search-entry .= new;
	my Gnome::Gtk3::Button $delete-row-button .= new;
	my Gnome::Gtk3::EntryCompletion $completion .= new;
	$completion.set-model(Gnome::Gtk3::ListStore.new(:build-id('gen-list')));
	$completion.set-text-column(0);
	$gen-appeared-search-entry.set-placeholder-text('Generation @Year');
	$gen-appeared-search-entry.set-completion($completion);

	# set the widget values
	$word-entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$definition-entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$gen-appeared-search-entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$word-entry.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	$definition-entry.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	$gen-appeared-search-entry.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	#$word-entry.set-icon-tooltip-markup;
	#$definition-entry.set-icon-tooltip-markup;
	#$gen-appeared-search-entry.set-icon-tooltip-markup;
	$word-entry.set-text($word//'');
	$definition-entry.set-text($definition//'');
	$gen-appeared-search-entry.set-text($gen-coined//'');
	self.on-lexicon-entry-type(:_widget($word-entry));
	self.on-lexicon-entry-type(:_widget($definition-entry));
	self.on-lexicon-entry-type(:_widget($gen-appeared-search-entry));
	$row-num-label.set-width-chars(5);
	$delete-row-button.set-image(Gnome::Gtk3::Image.new(:icon-name<user-trash>));
	$box.set-hexpand(True);
	$box.set-column-homogeneous(True);
	$box.set-column-spacing(10);


	# add entries to box
	$box.attach($word-entry,       				0,0,1,1);
	$box.attach($definition-entry, 				1,0,1,1);
	$box.attach($gen-appeared-search-entry, 	2,0,1,1);

	# add everything to the row
	$row.attach($row-num-label, 	0,0,1,1);
	$row.attach($box, 				1,0,1,1);
	$row.attach($delete-row-button, 2,0,1,1);

	#then add the row to the main list
	$!grid.attach($row, 0,$lexicon-rows, 1, 1);
	#$!grid-items.append: $row;

	#$!grid.container-foreach(self, 'renumber-columns');

	$word-entry.register-signal(self, 'on-lexicon-entry-type', 'changed');
	$definition-entry.register-signal(self, 'on-lexicon-entry-type', 'changed');
	$gen-appeared-search-entry.register-signal(self, 'on-lexicon-entry-type', 'changed');
	$delete-row-button.register-signal(self, 'on-delete-row', 'clicked', :$row);

	# make sure to show the update on the screen
	if not $dont-refresh {
		self.renumber-columns;
		$!grid.show-all;
	}

	# scroll the window to the bottom
	my Gnome::Gtk3::ScrolledWindow $scroll .= new(:build-id("lexicon-scroll-$id"));
	my Gnome::Gtk3::Adjustment $adj .= new(:native-object($scroll.get-vadjustment));
	$adj.set-value($adj.get-upper);
}

method on-delete-row(:_widget($button), :$row, :$row-num) {
	#$!grid-items[$row-num] = Nil;
	my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
	my Gnome::Gtk3::Entry $gen .= new(:native-object($box.get-child-at(2, 0)));
	my Node $node = $!pe.graph.get-node($gen.get-text.split('@')[0].trim, $gen.get-text.split('@')[1].trim.Int);
	$node.remove-coined-word(Gnome::Gtk3::Entry.new(:native-object($box.get-child-at(1,0))).get-text, Gnome::Gtk3::Entry.new(:native-object($box.get-child-at(0,0))).get-text);
	Utils.coinages-changes.emit($node);
	$row.destroy;
	#$!grid.remove-row($grid-items.index($row)); # ← check if works properly
	#$grid-items.remove
	#since a row was just deleted, we need to renumber the rows
	#$!grid.container-foreach(self, 'renumber-columns');
	self.renumber-columns;
	$!grid.show-all;
}

method renumber-columns() {
	#my Gnome::Gtk3::Grid $row .= new(:native-object($nw));
	#say $grid-items;
	my Gnome::Glib::List $list .= new( :native-object($!grid.get-children) );
	my $i = 0;
	#say $list.length;
	for ^$list.length {
		if (my $grid = $!grid.get-child-at(0,$_)).defined {
			#say $i;
			my Gnome::Gtk3::Grid $row .= new(:native-object($grid));
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			$!grid.remove-row($_);
			self.renumber-columns;
			return;
		}
	}
}

method get-words {
	my Word @words;
	my Int $i = 1;
	#say $i;
	my Gnome::Glib::List $list .= new( :native-object($!grid.get-children) );
	say $list.length;

	#while $list.is-valid {
	#while $!grid.get-child-at(0, $i++) -> $row is copy {
	for ^$list.length -> $i {
		#say $i;
		my Gnome::Gtk3::Grid $row .= new(:native-object($!grid.get-child-at(0, $i)));
		my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
		#say $box;
		my Gnome::Gtk3::Entry $word 		.= new(:native-object($box.get-child-at(0, 0)));
		my Gnome::Gtk3::Entry $definition 	.= new(:native-object($box.get-child-at(1, 0)));
		my Gnome::Gtk3::Entry $gen 			.= new(:native-object($box.get-child-at(2, 0)));

		# make sure none of the entries are empty
		if $word.get-text.trim.chars < 1 {
			Utils.show-dialog("The entry at row {$i+1} at the column <i>Word</i> is empty,
and it's not allowed to be empty.");
			return;
		} elsif $definition.get-text.trim.chars < 1 {
			Utils.show-dialog("The entry at row {$i+1} at the column <i>Definition</i> is empty,
and it's not allowed to be empty.");
			return;
		} elsif $gen.get-text.trim.chars < 1 {
			Utils.show-dialog("The entry at row {$i+1} at the column <i>Generation coined</i> is empty,
and it's not allowed to be empty.");
			return;
		}

		#check if the gen is valid
		#say $!pe.graph.children».[0]».get-name;
		#say $gen.get-text;
		#say not $gen.get-text.contains('@');
		#say $!pe.graph.get-node($gen.get-text.split('@')[0].trim, $gen.get-text.split('@')[1].trim.Int);
		if not $gen.get-text.contains('@') {
			Utils.show-dialog("The generation name at row {$i+1} must include a year after the name.
Notation: Generation @Year");
			return '';
		}
		#say Int($gen.get-text.split('@')[1].trim) ~~ Failure;
		if Int($gen.get-text.split('@')[1].trim) ~~ Failure {
			Utils.show-dialog("The generation name at row {$i+1} must include a numeric year after the name.
Notation: Generation @Year");
			return '';
		}
		#say $!pe.graph.get-node($gen.get-text.split('@')[0].trim, $gen.get-text.split('@')[1].trim.Int);
		my Node $node = $!pe.graph.get-node($gen.get-text.split('@')[0].trim, $gen.get-text.split('@')[1].trim.Int);
		if not $node {
			Utils.show-dialog("The generation name at row {$i+1} does not exist
Notation: Generation @Year");
			return '';
		}
		@words.append: Word.new(:word($word.get-text), :root-word($word.get-text), :definition($definition.get-text), :path([$node]));
	}
	return @words;
}

method get-lexicon-rows() {
	my Str $file-contents;
	my @words = self.get-words;
	for @words -> $word {
		$file-contents ~= $word.word~"\t"~$word.definition~"\t"~$word.path[0]~"\n";
	}
	return $file-contents;
}

method on-save-as-clicked() {
	my $file-contents = self.get-lexicon-rows;
	return if $file-contents eq '';

	#save lexicon to a file
	self.pick-file-and-save($file-contents);
}

method on-save-clicked() {
	my $file-contents = self.get-lexicon-rows;
	#say $file-contents;
	return if $file-contents eq '';

	#select file to save lexicon to if theres no file selected yet
	if not $!file {
		self.pick-file-and-save($file-contents);
	} else {
		$!file.IO.spurt: $file-contents;
	}
}

#| open a file and load it into the current tab
method on-load-clicked() {
	my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  :title("Open File"), :parent(self),
	  :action(GTK_FILE_CHOOSER_ACTION_OPEN),
	  :button-spec( [
		  "_Cancel", GTK_RESPONSE_CANCEL,
		  "_Open", GTK_RESPONSE_ACCEPT,
		]
	  )
	);
	my Gnome::Gtk3::FileFilter $file-filter .= new;
	$file-filter.add-pattern('*.lexicon');
	$dialog.set-filter($file-filter);


	my $response = $dialog.gtk-dialog-run;
	$dialog.gtk-widget-hide;
	if ($response ~~ GTK_RESPONSE_ACCEPT) {
		$!file = $dialog.get-filename;
		note "Opening file $!file";
		#note $!file.IO.slurp;
		$!tab-name.set-text($!file.IO.basename);
		self.load-file: $!file.IO;
	}
}

method load-file(IO::Path $file) {
	my Array[List] $lexicon = Utils.parse-tsv: $file.slurp;
	for @$lexicon {
		#say $_;
		self.add-lexicon-row(:word(.[0]), :definition(.[1]), :gen-coined(.[2]), :dont-refresh);
		my $node = $!pe.graph.get-node(.[2].split('@')[0].trim, .[2].split('@')[1].trim.Int);
		$node.coin-word(.[0], .[1], self) if $node;
	}
	self.renumber-columns;
	$!grid.show-all;
}

method on-open-clicked() {
	$!main.add-lexicon-tab;
}

method pick-file-and-save($file-contents) {
	my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  :title("Open File"), :parent(self),
	  :action(GTK_FILE_CHOOSER_ACTION_SAVE),
	  :button-spec( [
		  "_Cancel", GTK_RESPONSE_CANCEL,
		  "_Open", GTK_RESPONSE_ACCEPT
		]
	  )
	);
	$dialog.set-do-overwrite-confirmation(1);
	$dialog.set_current_name("rootwordlist.lexicon");
	my $response = $dialog.gtk-dialog-run;
	$dialog.gtk-widget-hide;
	if ($response ~~ GTK_RESPONSE_ACCEPT) {
	  $!file = $dialog.get-filename;
	  note "Saving to file $!file";
	  $!file.IO.spurt: $file-contents;
	}
	$dialog.gtk-widget-destroy;
}

method on-lexicon-entry-type(:_widget($entry)) {
	if $entry.get-text.trim.chars < 1 {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	} else {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, Nil);
	}

}

method on-rename-clicked() {
	my Gnome::Gtk3::Dialog $dialog .= new(
      :title('Message Dialog'), :parent($!notebook),
      :flags(GTK_DIALOG_DESTROY_WITH_PARENT),
      :button-spec( "Ok", GTK_RESPONSE_NONE)
    );

    my Gnome::Gtk3::Box $content-area .= new(
      :native-object($dialog.get-content-area)
    );

    my Gnome::Gtk3::Label $label .= new(:text('Rename tab'));
    my Gnome::Gtk3::Entry $entry .= new;
    $content-area.gtk_container_add($label);
    $content-area.gtk_container_add($entry);

    # Show the dialog. After return (Ok pressed) the dialog widget
    # is destroyed. show-all() must be called, otherwise the message
    # will not be seen.
    $dialog.show-all;
    $dialog.gtk-dialog-run;
	my Str $name = $entry.get-text.trim;
    $dialog.destroy;
	self.rename-tab($name);
}

method rename-tab(Str $new-name) {
	# prevent more than one lexicon from having the same name
	my Bool $return = $!main.rename-tab($!tab-name.get-text, $new-name, self);
	$!tab-name.set-text($new-name) if $return;
}

method on-delete-clicked() {
	$!main.remove-tab($!tab-name.get-text);
	$!notebook.remove-page($!notebook.get-current-page);
}

method file() {
	return $!file.IO.basename;
}


method update-lexicon(Word $old-word=Word, Word $new-word=Word) {
	my Gnome::Glib::List $list .= new(:native-object($!grid.get-children));

	if not $old-word and not $new-word {
		# remove old row contents
		if $list.length > 0 {
			for ^$list.length {
				# continuously remove the top row till all rows are gone
				$!grid.remove-row(0);
			}
		}

	# now add back all row contents - including newly added or changed rows
		$lexicon-rows = -1;
		my $nodes = $!pe.graph.nodes;
		while $nodes() -> $node {
			for $node.coinages -> $word {
				if $word.lexicon ~~ self {
					self.add-lexicon-row(:word($word.root-word), :definition($word.definition), :gen-coined($word.path[0].get-name), :dont-refresh);
				}
			}
		}
	} elsif $old-word and $new-word {
		for ^$list.length {
			my Gnome::Gtk3::Grid $row .= new(:native-object($!grid.get-child-at(0, $_)));
			my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
			my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $gen-coined .= new(:native-object($box.get-child-at(2,0)));
			if $word.get-text eq $old-word.word and
			  $definition.get-text eq $old-word.definition and
			  $gen-coined.get-text eq $old-word.path[0].get-name {
				say 'updating ', $old-word.word, ' to ', $new-word.word;
				$word.set-text($new-word.word);
				$definition.set-text($new-word.definition);
				$gen-coined.set-text($new-word.path[0].get-name);
				if (my Gnome::Gtk3::Button $check .= new(:native-object($row.get-child-at(3,0)))).is-valid {
					$check.destroy;
				}
				last;
			}
		}
	} elsif $old-word and not $new-word {
		for ^$list.length {
			self.remove-row($_);
		}
	} elsif not $old-word and $new-word {
		self.add-lexicon-row(:word($new-word.word), :definition($new-word.definition), :gen-coined($new-word.path[0].get-name), :dont-refresh);
	}

	# add column numbers
	self.renumber-columns;

	$!grid.show-all;
}

