use Utils;
use Graph;
use GUI::CoinageRow;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Label;
use GUI::Gnome::Gtk3::Expander;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::ScrolledWindow;
use Gnome::Gtk3::Frame;

unit class CoinageList is Gnome::Gtk3::Grid;

has Graph $.graph is required;
has Hash %!sections{Node};


submethod new ( |c ) {
  self.bless( :GtkGrid, |c);
}

submethod TWEAK ( ) {
	self.setup;
}

method setup() {
	my Supply $coinages-supply = Utils.coinages-changes.Supply;
	$coinages-supply.tap(-> $/ {
		self.update-coinage($<node>, $<old-word>//Word, $<new-word>//Word, $<row-num>);
	});
	self.create-coinage-groupings;
	self.show-all;
}

method create-coinage-groupings {
	# first remove all the rows
	my Gnome::Glib::List $list .= new( :native-object(self.get-children) );
	say $list.length;
	for 0..^ $list.length {
		self.remove-row($_);
	}

	# then add in the updated rows
	my $i = -1;
	my $nodes = $!graph.nodes-unique(:breadth-first);
	while my $node = $nodes() {
		my Gnome::Gtk3::Expander $expander .= new(:mnemonic('<big>'~$node.get-name~'</big>'~(' - '~$node.description if $node.description)));
		$expander.set-use-markup(True);
		my Gnome::Gtk3::Frame $frame .= new;
		my Gnome::Gtk3::Grid $container .= new;
		my Gnome::Gtk3::Button $add-word .= new(:label('Add Word'));
		$add-word.set-hexpand(True);
		$add-word.register-signal(self, 'on-add-word-clicked', 'clicked', :$node);
		%!sections{$node}<grid> = Gnome::Gtk3::Grid.new;
		$container.attach(%!sections{$node}<grid>, 0, 0, 1, 1);
		$container.attach($add-word, 0, 1, 1, 1);
		$frame.add($container);
		$expander.add($frame);
		self.attach($expander, 0, ++$i, 1, 1);
		self.update-coinage($node);
	}
}

method add-coinages-row(Node $node, :$word, :$definition, :$refresh) {
	my CoinageRow $row .= new(:$node, :$word, :$definition);

	# add the row to the main list
	%!sections{$node}<grid>.attach($row, 0, ++%!sections{$node}<row-num>, 1, 1);

	if $refresh {
		self.renumber-coinages-columns($node);
		%!sections{$node}<grid>.show-all;
	}
}

method renumber-coinages-columns(Node $node) {
	my Gnome::Glib::List $list .= new( :native-object(%!sections{$node}<grid>.get-children) );
	my $i = 0;
	for ^$list.length {
		#say $_;
		if (my Gnome::Gtk3::Grid $row .= new(:native-object(%!sections{$node}<grid>.get-child-at(0,$_)))).is-valid {
			my Gnome::Gtk3::Label $label .= new(:native-object($row.get-child-at(0,0)));
			$label.set-text(Str(++$i));
		} else {
			#say $list.length;
			say 'removing row ', $_;
			#return if $_ == $list.length-1;
			%!sections{$node}<grid>.remove-row($_);
			self.renumber-coinages-columns($node);
			return;
		}
	}
}

method update-coinage(Node $node, Word $old-word=Word, Word $new-word=Word, Int $row-num=0) {
	# remove old row contents
	my Gnome::Glib::List $list .= new(:native-object(%!sections{$node}<grid>.get-children));
	if not $old-word and not $new-word {
		for ^$list.length {
			# continuously remove the top row till all rows are gone
			%!sections{$node}<grid>.remove-row(0);
			--%!sections{$node}<row-num>;
		}

		# now add back all row contents - including newly added or changed rows
		for $node.coinages -> $word {
			say $word.word;
			self.add-coinages-row($node, :word($word.word), :definition($word.definition));
		}
	}

	if $old-word and $new-word and $row-num {
		my Gnome::Gtk3::Grid $row .= new(:native-object(%!sections{$node}<grid>.get-child-at(0, $row-num)));
		my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
		my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
		my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(1,0)));
		#my Gnome::Gtk3::Entry $lexicon .= new(:native-object($box.get-child-at(2,0)));
		if $word.get-text eq $old-word.word and
		  $definition.get-text eq $old-word.definition {
			say 'updating ', $old-word.word, ' to ', $new-word.word;
			$word.set-text($new-word.word);
			$definition.set-text($new-word.definition);
			#$lexicon.set-text($new-word.lexicon.tab-name.get-text);
			if (my Gnome::Gtk3::Button $check .= new(:native-object($row.get-child-at(3,0)))).is-valid {
				$check.destroy;
			}
		}
	} elsif $old-word and not $new-word {
		%!sections{$node}<grid>.remove-row($row-num-1);
	} elsif not $old-word and $new-word and $row-num {
		my Gnome::Gtk3::Grid $row .= new(:native-object(%!sections{$node}<grid>.get-child-at(0, $row-num)));
		if not $row.is-valid {
			self.add-coinages-row($node, :word($new-word.word), :definition($new-word.definition));
		} else {
			my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
			my Gnome::Gtk3::Entry $word .= new(:native-object($box.get-child-at(0,0)));
			my Gnome::Gtk3::Entry $definition .= new(:native-object($box.get-child-at(1,0)));
			if $word.get-text eq $new-word.word and
			  $definition.get-text eq $new-word.definition {
				say 'adding ', $new-word.word;
				if (my Gnome::Gtk3::Button $check .= new(:native-object($row.get-child-at(3,0)))).is-valid {
					$check.destroy;
				}
			}
		}
	}

	# add column numbers
	self.renumber-coinages-columns($node);

	%!sections{$node}<grid>.show-all;
}

method on-add-word-clicked(Node :$node) {
	self.add-coinages-row($node, :refresh);
}
