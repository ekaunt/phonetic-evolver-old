use Utils;
use Graph;
use Gnome::Gtk3::Window;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::DrawingArea;
use Gnome::Gtk3::MessageDialog;
use Gnome::Gtk3::Dialog;
use Gnome::Gtk3::Enums;
use GUI::Gnome::Gtk3::EventBox;
use Gnome::Gtk3::SpinButton;
use Gnome::Gtk3::ToolButton;
use Gnome::Gtk3::CssProvider;
use Gnome::Gtk3::StyleProvider;
use Gnome::Gtk3::StyleContext;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::TreePath;
use Gnome::Gdk3::Events;
use Gnome::Gdk3::Types;
use Gnome::Gdk3::Window;
use Gnome::Cairo;
use Gnome::Cairo::Types;
use Gnome::Cairo::Enums;
use Gnome::N::GlibToRakuTypes;
use Gnome::N::NativeLib;
#use Gnome::Gtk3::GLArea;
#use Gnome::Glib::Error;
#use OpenGL;
#use GdkPixmap;
use NativeCall;

unit class Tree;

class Point {
	has gdouble $.x is rw = 0e0;
	has gdouble $.y is rw = 0e0;
	multi method set(gdouble:D $x, gdouble:D $y) {
		$!x = $x;
		$!y = $y;
	}
	multi method set(Num:D $x, Num:D $y) {
		$!x = $x;
		$!y = $y;
	}
	multi method set(Point:D $new) {
		$!x = $new.x;
		$!y = $new.y;
	}
	#| move the position relative to its current position
	multi method move(gdouble:D $x, gdouble:D $y) {
		$!x += $x;
		$!y += $y;
	}
}
class Rect {
	has gdouble $.width = 1e0;
	has gdouble $.height = 1e0;
	method resize(gdouble $new-width, gdouble $new-height) {
		$!width = $new-width;
		$!height = $new-height;
	}
}

class Button {
	#| the top center of the button
	has Point $.position .= new;
	#| the size of the button
	has Rect $.size .= new;
	#| the node that this button represents
	has Node $.node is required;
	#| a signal that fires when the button gets pressed
	has Supplier $.signal .= new;
}

has Gnome::Gtk3::DrawingArea $.da;
#has cairo_surface_t $!surface;
# whether to lock the ability to draw to the screen
has Bool $!drawing = False;
has cairo_t $.buffer;
#has Gnome::Cairo::Surface $.surface;
has Bool $.initialized = False;

my Str $id;
has $.sc is required;
has Graph $.graph is required;
#has Gnome::Gtk3::Window $.w is required;
#has Num $.zoom = 1e0;

has Bool $.mouse-clicked = False;

has Point $.offset .= new;
has Point $.start-pan .= new;
has Point $.scale .= new(x => 1e0, y => 1e0);

has gdouble $!width;#  = Num($!w.get-allocated-width);
has gdouble $!height;# = Num($!w.get-allocated-height);

# a hash with all plus buttons with the name of the gen as the id
has %!plus;
has Point $!drag-start .= new;
has Point $!current-position .= new;
has %!selected;
my %buttons;

# the space to draw between each line in the description
my gdouble $line-spacing = 5e0;
# the empty space to draw between the title and description of each button
my gdouble $divider = 2e0;
# the space between the edge of a button and contents within the button
my gdouble $margin = 5e0;

my Gnome::Gtk3::ListStore $list-store;

submethod TWEAK() {
	# initialize screen settings
	#	$!offset.set(0, 0);
	$!da .= new(:build-id<tree-draw>);

	$!da.register-signal(self, 'on-draw', 'draw');
	$!da.add-events(GDK_SCROLL_MASK +| GDK_BUTTON_MOTION_MASK +| GDK_BUTTON_PRESS_MASK +| GDK_BUTTON_RELEASE_MASK);
	$!da.register-signal(self, 'on-any-button-clicked', 'button-press-event');
	$!da.register-signal(self, 'on-scroll-event', 'scroll-event');
	$!da.register-signal(self, 'on-plus-released', 'button-release-event');
	$!da.register-signal(self, 'on-motion-notify-event', 'motion-notify-event');
	$!da.register-signal(self, 'init', 'realize');
	$!da.register-signal(self, 'modify-drawing', 'configure-event');

	my Gnome::Gtk3::SpinButton $spinner .= new(:build-id<tree-zoom>);
	$spinner.register-signal(self, 'on-tree-zoomed', 'value-changed');

	my Gnome::Gtk3::ToolButton $delete .= new(:build-id<tree-delete>);
	$delete.register-signal(self, 'on-delete-clicked', 'clicked');
	my Gnome::Gtk3::ToolButton $add-child .= new(:build-id<tree-add-child>);
	$add-child.register-signal(self, 'on-add-child-clicked', 'clicked');
	my Gnome::Gtk3::ToolButton $add-parent .= new(:build-id<tree-add-parent>);
	$add-parent.register-signal(self, 'on-add-parent-clicked', 'clicked');
	my Gnome::Gtk3::ToolButton $open .= new(:build-id<tree-open>);
	$open.register-signal(self, 'on-open-generation', 'clicked');

	$list-store .= new(:build-id<gen-list>);

	self.reload-tree;
}

method init {
	$!width = Num($!da.get-allocated-width);
	$!height = Num($!da.get-allocated-height);

	$!initialized = True;
}

method modify-drawing(GdkEventConfigure $event) {
	self.init;
	# twice on purpose
	self.reload-tree;
#	self.reload-tree;
}

#| fires when the mouse uses the scroll wheel
method on-scroll-event(N-GdkEventScroll $event) {
	my Point $world-before-zoom = self.screen-to-world($event.x, $event.y);
	say $event.direction;
	if $event.direction == GDK_SCROLL_UP {
		$!scale.x *= Num(1.1);
		$!scale.y *= Num(1.1);
		self.reload-tree;
	} elsif $event.direction == GDK_SCROLL_DOWN {
		$!scale.x *= Num(0.9);
		$!scale.y *= Num(0.9);
		self.reload-tree;
	}
	my Point $world-after-zoom = self.screen-to-world($event.x, $event.y);
	$!offset.x += ($world-before-zoom.x - $world-after-zoom.x);
	$!offset.y += ($world-before-zoom.y - $world-after-zoom.y);
}

multi method world-to-screen(Point:D $world) returns Point {
	my gdouble $screen-x = ($world.x - $!offset.x) * $!scale.x;
	my gdouble $screen-y = ($world.y - $!offset.y) * $!scale.y;
#	$world.set($screen-x, $screen-y);
	return Point.new(x=>$screen-x, y=>$screen-y)
}
multi method screen-to-world(Point:D $screen) returns Point {
	my gdouble $world-x = ($screen.x / $!scale.x) + $!offset.x;
	my gdouble $world-y = ($screen.y / $!scale.y) + $!offset.y;
#	$screen.set($world-x, $world-y);
	return Point.new(x=>$world-x, y=>$world-y);
}
multi method world-to-screen(gdouble $world-x, gdouble $world-y) returns Point {
	my gdouble $screen-x = ($world-x - $!offset.x) * $!scale.x;
	my gdouble $screen-y = ($world-y - $!offset.y) * $!scale.y;
	return Point.new(x => $screen-x, y => $screen-y);
}
multi method screen-to-world(gdouble $screen-x, gdouble $screen-y) returns Point {
	my gdouble $world-x = ($screen-x / $!scale.x) + $!offset.x;
	my gdouble $world-y = ($screen-y / $!scale.y) + $!offset.y;
	return Point.new(x => $world-x, y => $world-y);
}
multi method world-to-screen(Num $world-x, Num $world-y) returns Point {
	my gdouble $screen-x = ($world-x - $!offset.x) * $!scale.x;
	my gdouble $screen-y = ($world-y - $!offset.y) * $!scale.y;
	return Point.new(x => $screen-x, y => $screen-y);
}
multi method screen-to-world(Num $screen-x, Num $screen-y) returns Point {
	my gdouble $world-x = ($screen-x / $!scale.x) + $!offset.x;
	my gdouble $world-y = ($screen-y / $!scale.y) + $!offset.y;
	return Point.new(x => $world-x, y => $world-y);
}

#| [ add the buttons with all generations to the tree ]
method !reload-generations {
	# 150 is the sidebar width
	self!show-nodes($!graph.root, 0e0, $!width);
}

#|[ go through each generation one by one and add buttons for each of them ]
method !show-nodes(Node $node, gdouble $x-left, gdouble $x-right) {
	if %buttons{$node}:!exists {
		self!create-button($node);
	}
	#say 'showing ', $node;
	my $children = $node.children;
	my gdouble $child-count = Num($children.elems);
	for $children.kv -> $i, $child {
		#recursively go to all nodes and put a button at each node
		my gdouble $section-width = $x-right - $x-left;
		my gdouble $subsection-width = ($section-width) / $child-count;
		my gdouble $x = Num(($subsection-width * ($i + 1) - ($subsection-width / 2e0)) + $x-left);
		#		say "$x: {$child.get-name}";
		self!process-node($node, $child, $x, %buttons{$child}<button>:exists);
		self!show-nodes($child, $x - ($subsection-width / 2), $x + ($subsection-width / 2));
	}
}

#| create a generations button on the drawingarea screen
method !create-button(Node $node) returns Button {
	my Button $button .= new(:$node);
	#$button.signal.Supply.tap(-> $button { self!on-button-clicked($button); });
	%buttons{$node}<button> = $button;
	return $button;
}

#|[
	set up a button for a generation.

	x: the x position to place a button
	visited: a flag whether this node has been visited.
			if it has, connect parent to this node
]
method !process-node(Node $parent, Node $node, gdouble $x-val, Bool $visited) {
	if not $visited {
		self!create-button($node);
	}

	my Button $parent-button = %buttons{$parent}<button>;
	my Button $child-button = %buttons{$node}<button>;
	%buttons{$node}<parent>.append: $parent-button if $parent.id ne $!graph.root.id;

#	my gdouble $y-val = Num(Utils.normalize($node.year, $!graph.min-year, $!graph.max-year) * $!height);
	my gdouble $y-val = Num($node.year);
	$child-button.position.set(self.world-to-screen($x-val, $y-val));
}


method on-draw(cairo_t $cr) {
	say "redraw";

	self!timeline($cr);


	# multi thread as much as possible
	race for %buttons.values -> $value {
		my $button = $value<button>;
		# do not render buttons that are not on screen
		next if 0e0 > $button.position.x or $button.position.x > $!width;
		next if 0e0 > $button.position.y or $button.position.y > $!height;

		if $button.node.id ne $!graph.root.id {
			#draw buttons

			# child button
			self.draw-plus-button($cr, $button, $button.position.x, $button.position.y, True);
			self.draw-plus-button($cr, $button, $button.position.x, $button.position.y+$button.size.height);

#			given $surface {
			cairo_set_source_rgb($cr, 0e0, 0e0, 0e0);
			cairo_set_line_width($cr, 3e0);
			# draw the buttons rectangle/extents
			my gdouble $top-left-x = ($button.position.x - $button.size.width / 2);
			my gdouble $top-left-y = ($button.position.y);
			cairo_select_font_face($cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
			cairo_set_font_size($cr, 18e0);
#			say "set font";
			my cairo_text_extents_t $title-extents .= new;
#			say "setting extents";
			cairo_text_extents($cr, $button.node.get-name, $title-extents);
#			say "set text extents";
			cairo_select_font_face($cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
			cairo_set_font_size($cr, 12e0);
			my Str $desc = Utils.wrap($button.node.description, 20);
#			say 'desc: ', $desc.lines, "\n" if $desc;
			my cairo_text_extents_t $desc-extents .= new;
#			say 'longest: ', $desc.lines.map(-> $line {cairo_text_extents($cr, $line, $desc-extents); return $desc-extents}).sort(*.width)[*-1] if $desc;
			my cairo_text_extents_t @extents;
			for $desc.lines -> $line {
				my cairo_text_extents_t $extents .= new;
				cairo_text_extents($cr, $line, $extents);
				@extents.append: $extents;
			}
			$desc-extents = @extents.sort(*.width)[*-1] if @extents;
			my gdouble $button-width = Utils.max($desc-extents.width, $title-extents.width) + $margin*2;
			my gdouble $button-height = $title-extents.height + $margin*3;
			$button-height = $title-extents.height + (($desc-extents.height + $line-spacing) * $desc.lines) + $divider + $margin*3 if $desc;
			cairo_rectangle($cr, $top-left-x, $top-left-y, $button-width, $button-height);
#			say "drew rectangle";
			cairo_set_source_rgb($cr, 0e0, 0e0, 0e0);
			cairo_stroke_preserve($cr);
			cairo_set_source_rgb($cr, 0.9e0, 0.9e0, 0.9e0);
			cairo_fill($cr);
			# draw the buttons text
			cairo_set_source_rgb($cr, 0e0, 0e0, 0e0);
			my gdouble $font-size = 18e0;
			cairo_move_to($cr, $top-left-x+$margin, $top-left-y+$margin + $font-size);
			cairo_select_font_face($cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
			cairo_set_font_size($cr, $font-size);
#			say "drew font";
			cairo_show_text($cr, $button.node.get-name);
			if $desc {
				my $pos = Point.new(x => $top-left-x+$margin, y => $top-left-y+$margin + $title-extents.height);
				self!draw-description($cr, $desc, $pos, $desc-extents.height) if $desc;
			}

			$button.size.resize($button-width, $button-height);

			# draw the bezier curve
			race for $value<parent>.kv -> $i, $parent {
				my gdouble $start-x = $parent.position.x;
				my gdouble $start-y = $parent.position.y + $parent.size.height;

				cairo_set_source_rgb($cr, 0.5e0, 0.5e0, 0.5e0);
				my gdouble $x = ($button.position.y - $start-y);
				my gdouble $pull = $x < 0 ?? -$x / 2 !! $x / 2;
				my gdouble $x1 = $start-x;
				my gdouble $y1 = $start-y + $pull;
				my gdouble $x2 = $button.position.x;
				my gdouble $y2 = $button.position.y - $pull;
				# add the actual bezier curve
				cairo_move_to($cr, $start-x, $start-y);
				cairo_curve_to($cr, $x1, $y1, $x2, $y2, $button.position.x, $button.position.y);

				cairo_stroke($cr);
			}
		}
	}


	# if button is pressed then make a line till the $!current-position
	if %!selected {
		for %!plus.values -> $v {
			for $v.values -> $plus {
				if $plus<clicked> {
					# draw the bezier curve
					cairo_set_source_rgb($cr, Num(0.7), Num(0.7), Num(0.7));
					cairo_set_line_width($cr, 3e0);
					my gdouble $start-x = $plus<location>.x;
					my gdouble $start-y = $plus<location>.y;
					my gdouble $end-x = $!current-position.x;
					my gdouble $end-y = $!current-position.y;
					my gdouble $x = ($end-y - $start-y);
					my gdouble $pull = $x < 0 ?? -$x / 2 !! $x / 2;
					my gdouble $y1;
					my gdouble $y2;
					if $plus<top> {
						$y1 = $start-y - $pull;
						$y2 = $end-y + $pull;
					} else {
						$y1 = $start-y + $pull;
						$y2 = $end-y - $pull;
					}
					# add the actual bezier curve
					cairo_move_to($cr, $start-x, $start-y);
					cairo_curve_to($cr, $start-x, $y1, $end-x, $y2, $end-x, $end-y);
					# add an arrow tip at end of bezier curve
					# flip the arrow if the curve is going upwards
					if $plus<top> {
						cairo_rel_line_to($cr, -5e0, +5e0);
						cairo_rel_move_to($cr, 5e0, -5e0);
						cairo_rel_line_to($cr, 5e0, +5e0);
					} else {
						cairo_rel_line_to($cr, -5e0, -5e0);
						cairo_rel_move_to($cr, 5e0, 5e0);
						cairo_rel_line_to($cr, 5e0, -5e0);
					}
					cairo_stroke($cr);
				}
			}
		}
	}
}

#|[ draw the description onto the screen, while taking into account word wrap ]
method !draw-description(cairo_t $cr, Str $desc, Point $pos, gdouble $line-height) {
	for $desc.lines.kv -> $i, $line {
		my gdouble $y = $pos.y + (($i + 1e0) * ($line-height + $line-spacing)) + $divider;
		cairo_move_to($cr, $pos.x, $y);
		cairo_select_font_face($cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
		cairo_set_font_size($cr, 12e0);
		cairo_show_text($cr, $line);
	}
}

#here be slow
#|[ draw plus buttons next to each generations button
#| (top is flag whether its the plus button above the generation or the one below) ]
method draw-plus-button(cairo_t $cr, Button $button, gdouble $x, gdouble $y, Bool $top= False) {
	my gdouble $radius = 5e0;
#	say "drawing plus button";
	cairo_set_source_rgb($cr, 0e0, 0e0, 0e0);
	cairo_set_line_width($cr, 2e0);
	cairo_move_to($cr, $x, $y + (($top ?? -$radius!!$radius) * 2e0));
	cairo_rel_line_to($cr, -5e0, $top ?? 5e0 !! -5e0);
#	say "drew line";
	cairo_rel_move_to($cr, 10e0, 0e0);
	cairo_rel_line_to($cr, -5e0, $top ?? -5e0 !! 5e0);
	cairo_stroke($cr);
	#save the button
	%!plus{$button.node}{$top}<location> = Point.new(:$x, y => $y + (($top ?? -$radius!!$radius) * 2));
	%!plus{$button.node}{$top}<radius> = $radius * 2;
	%!plus{$button.node}{$top}<button> = $button;
	%!plus{$button.node}{$top}<clicked> = False if not %!plus{$button.node}{$top}<clicked>;
	%!plus{$button.node}{$top}<top> = $top;
}

#| draw a notched adaptive zoom guide (timeline) at the side of the tree
method !timeline(cairo_t $cr) {
	my gdouble $offset = 20e0;

	#draw a straight line down the screen
	cairo_set_source_rgb($cr, 0e0, 0e0, 0e0);
	cairo_set_line_width($cr, 5e0);
	cairo_move_to($cr, $offset, $offset);
	cairo_rel_line_to($cr, 0e0, $!height-$offset*2);

	# add ticks
	# first get screen extents
	my gdouble $top         = self.screen-to-world(0e0, $offset).y;
	my gdouble $bottom      = self.screen-to-world(0e0, $!height-$offset).y;
	my gdouble $screen-size = $bottom - $top;
	my Numeric $magnitude   = round(log($screen-size, 5));
	my Numeric $big-ticks   = 5**($magnitude-1);
	my Numeric $small-ticks = 5**($magnitude-2);

	for $top … $bottom -> $y is copy {
		$y = Num(round($y));
		# big ticks
		if $y %% $big-ticks {
			cairo_move_to($cr, $offset, self.world-to-screen(0e0, $y).y);
			cairo_set_line_width($cr, 5e0);
			cairo_rel_line_to($cr, $offset, 0e0);
			# write the year next to the big tick
			cairo_rel_move_to($cr, 5e0, 0e0);
			cairo_select_font_face($cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
			cairo_set_font_size($cr, 12e0);
			cairo_show_text($cr, Str($y));
		# small ticks
		} elsif $y %% $small-ticks {
			cairo_move_to($cr, $offset, self.world-to-screen(0e0, $y).y);
			cairo_set_line_width($cr, 2e0);
			cairo_rel_line_to($cr, $offset/2e0, 0e0);
		}
	}
}


# c functions to directly call the cairo library for functions that need to be super fast
sub cairo_set_source_rgb(cairo_t, gdouble, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_set_line_width(cairo_t, gdouble) is native(&cairo-lib) { * }
sub cairo_move_to(cairo_t, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_rel_move_to(cairo_t, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_line_to(cairo_t, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_rel_line_to(cairo_t, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_curve_to(cairo_t, gdouble, gdouble, gdouble, gdouble, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_stroke(cairo_t) is native(&cairo-lib) { * }
sub cairo_rectangle(cairo_t, gdouble, gdouble, gdouble, gdouble) is native(&cairo-lib) { * }
sub cairo_stroke_preserve(cairo_t) is native(&cairo-lib) { * }
sub cairo_select_font_face(cairo_t, Str, gint, gint) is native(&cairo-lib) { * }
sub cairo_set_font_size(cairo_t, gdouble) is native(&cairo-lib) { * }
sub cairo_text_extents(cairo_t, Str, cairo_text_extents_t is rw) is native(&cairo-lib) { * }
sub cairo_show_text(cairo_t, Str) is native(&cairo-lib) { * }
sub cairo_fill(cairo_t) is native(&cairo-lib) { * }
sub cairo_paint(cairo_t) is native(&cairo-lib) { * }
sub cairo_destroy(cairo_t) is native(&cairo-lib) { * }
sub cairo_image_surface_create(gint, gint, gint) returns cairo_surface_t is native(&cairo-lib) { * }
sub cairo_create(cairo_surface_t) returns cairo_t is native(&cairo-lib) { * }
sub cairo_surface_destroy(cairo_surface_t) is native(&cairo-lib) { * }
sub cairo_set_source_surface(cairo_t, cairo_surface_t, gdouble, gdouble) is native(&cairo-lib) { * }


method on-motion-notify-event(N-GdkEventMotion $event, :_widget($w)) {
	if $!mouse-clicked {
		say "button moving";
		$!offset.x -= ($event.x - $!start-pan.x) / $!scale.x;
		$!offset.y -= ($event.y - $!start-pan.y) / $!scale.y;

		$!start-pan.set($event.x, $event.y);

		self.reload-tree;
	}

	if %!selected and %!selected !~~ Any {
		say "SELECTED BUTTON: ", %!selected<button>.node.get-name;
		$!current-position.set(self.screen-to-world($event.x, $event.y));
		$!da.queue-draw;
	}
}

#| activates when the little plus next to each generation is clicked
method on-any-button-clicked(N-GdkEventButton $event) {
	# on single click
	if $event.type == GDK_BUTTON_PRESS {
		say "button  clicked";
		# go through the node buttons
		for %buttons.values -> $value {
			my $button = $value<button>;
			if ($button.position.x - $button.size.width / 2) < $event.x < ($button.position.x + $button.size
					.width / 2) and
					($button.position.y - $button.size.height / 2) < $event.y < ($button.position.y + $button.size
							.height / 2) {
				self!select-button($button);
				return;
			}
		}

		# go through above and below plus buttons
		for %!plus.kv -> $node, $top {
			for $top.values -> $plus {
				if sqrt(abs($event.x − $plus<location>.x)² + abs($event.y - $plus<location>.y)²) ≤ $plus<radius> {
					$!drag-start.set($event.x, $event.y);
					$plus<clicked> = True;
					self!select-button($plus<button>);
					return;
				}
			}
		}

		# if we got here then we want to draw the screen, not select a button
		$!mouse-clicked = True;
		$!start-pan.set($event.x, $event.y);

	# on double click
	} elsif $event.type == GDK_2BUTTON_PRESS {
		say "button double clicked";
		# go through the node buttons
		for %buttons.values -> $value {
			my $button = $value<button>;
			if ($button.position.x - $button.size.width / 2) < $event.x < ($button.position.x + $button.size
					.width / 2) and
					($button.position.y - $button.size.height / 2) < $event.y < ($button.position.y + $button.size
							.height / 2) {
				self!select-button($button);
				self.on-open-generation;
				return;
			}
		}
	}
}

method on-plus-released(N-GdkEventButton $event) {
	say "button released";
	$!mouse-clicked = False;
	# if mouse is over a generation
	LOOP: for %!plus.values -> $top {
		# go through above and below plus buttons
		for $top.values -> $end-value {
			$end-value<clicked> = False;
			my $end = $end-value<button>;
			if $end.position.x < $event.x < $end.position.x + $end.size.width and
					$end.position.y < $event.y < $end.position.y + $end.size.height
			{
				# we need to differentiate if were clicking the button from the top or bottom of the button
				if %!selected<top> {
					# if the selected node has root as a parent then remove it before adding the new node to it
					if %!selected<node>.parents ∋ $!graph.root {
						%!selected<node>.remove-parent($!graph.root);
					}
					%!selected<node>.add-parents([$end-value<node>]);
					$end-value<node>.add-child(%!selected<node>);
				} else {
					# if the child node has root as a parent then remove it before adding the current node to it
					if $end-value<node>.parents ∋ $!graph.root {
						$end-value<node>.remove-parent($!graph.root);
					}
					%!selected<node>.add-child($end-value<node>);
					$end-value<node>.add-parents([%!selected<node>]);
				}
				my $cyclic = $!graph.is-cyclic;
				if $cyclic {
					if %!selected<top> {
						%!selected<node>.parents.pop;
						$end-value<node>.children.pop;
					} else {
						%!selected<node>.children.pop;
						$end-value<node>.parents.pop;
					}
					Utils
							.show-dialog("Generation \"{ $cyclic[0].title } \@{ $cyclic[0].year }\" creates an infinitely repeating loop with itself by having generation \"{ $cyclic[1].title } \@{ $cyclic[1].year }\" as a parent.");
				}
				self!deselect-button;
				self.reload-tree;
				return;
			}
		}
	}
	# if we got up to here then the user let go of the arrow not on a button,
	# so we should add another generation
	if %!selected<button> and $!drag-start.x != 0e0 and $!drag-start.y != 0e0 {
		my gdouble $y = Num(Utils.normalize($event.y, 0, $!height - 150) * $!graph.max-year);
		$y = self.world-to-screen(Point.new(x => 0e0, :$y)).y;
		if %!selected<top> {
			self.on-add-parent-clicked(:y(Int($y)));
		} else {
			self.on-add-child-clicked(:y(Int($y)));
		}
	}
	self!deselect-button;
	$!da.queue-draw;
}

#method on-tree-zoomed(Gnome::Gtk3::SpinButton :_widget($spinner)) {
#	$!zoom = $spinner.get-value;
#	say $!zoom;
#	self.reload-tree;
#}

method reload-tree {
	self.init unless $!initialized;

	say 'reloading tree';
	self!reload-generations;
	#	self.show-all
	$!da.queue-draw;
#	start self.redraw;
}

method !select-button(Button $button, Bool $top= False) {
	#%!selected<button>.set-name('outline') if %!selected<button>;
	%!selected<button> = $button;
	#%!selected<button>.set-name('selected');
	%!selected<top> = $top;
	#say %!selected;
}
method !deselect-button {
	#    %!selected<button>.set-name('outline') if %!selected<button>;
	%!selected<button node top> = Any;
	$!drag-start.set(Point.new);
}

#method !on-button-clicked(Button $button) {
#self!select-button($button, False);
#}

method on-open-generation {
	$!sc.add-tab(%!selected<button>.node);
}

method on-delete-clicked {
	if not %!selected<node> {
		Utils.show-dialog('To delete a node, you must select it first.');
		return;
	}

	%buttons{%!selected<button>.node}:delete;
	# delete references to the node that we made in %buttons
	for %buttons.values -> $button {
		for $button<parent>.values -> $parent {
			for $button<parent>».node -> $node {
				if $node.id eq %!selected<button>.node.id {
					$button<parent>:delete;
				}
			}
		}
	}
	#%!selected.destroy;
	$!graph.delete-node(%!selected<button>.node);

	# remove the new node to the drop down list from the root word list generation picker
	$list-store.list-store-clear;
	my $nodes = $!graph.nodes;
	# dont get the root node in the list
	$nodes();
	while $nodes() -> $node {
		my $iter = $list-store.gtk-list-store-append;
		$list-store.gtk-list-store-set($iter, 0, $node.get-name);
	}

	#%!plus{$%!selected<node>}:delete;
	%!selected<button node top> = Any;
	$!drag-start.set(0, 0);

	self.reload-tree;
}

method on-add-child-clicked(Str :$name, Int :$y) {
	if not %!selected<button> {
		Utils.show-dialog('Must select a node before you can add a child.');
		return;
	}
	my Gnome::Gtk3::Dialog $dialog .= new(:build-id<tree-new-generation>);
	my Gnome::Gtk3::Entry $title .= new(:build-id<tree-title>);
	my Gnome::Gtk3::Entry $year .= new(:build-id<tree-year>);
	my Gnome::Gtk3::Entry $description .= new(:build-id<tree-description>);
	$title.set-text: $name ?? $name!!'';
	$year.set-text: $y ?? Str($y)!!'';
	$description.set-text: '';
	$dialog.show;
	given GtkResponseType($dialog.run) {
		when GTK_RESPONSE_OK {
			say 'add-child';
			if $title.get-text.trim.chars < 1 {
				Utils.show-dialog('Name must be specified.');
				$dialog.hide;
				self.on-add-child-clicked(:y($year.get-text.Int));
				return;
			}
			if $year.get-text.trim.chars < 1 {
				Utils.show-dialog('Year must be specified.');
				$dialog.hide;
				self.on-add-child-clicked(:name($title.get-text));
				return;
			}
			try {
				my Int $yr = $year.get-text.Int;
				CATCH {
					default {
						Utils.show-dialog('Year must be a numeric value');
						$dialog.hide;
						return;
					}
				}
			}
			my @parents = [%!selected<button>.node];
			my Node $new-node = $!graph.add-node(Node.new(title => $title.get-text, year => $year.get-text.Int,
					description => $description.get-text), @parents);
			%!selected<button>.node.add-child($new-node);
			$new-node.add-parents: @parents;

			# add the new node to the drop down list from the root word list generation picker
			my $iter = $list-store.gtk-list-store-append;
			$list-store.gtk-list-store-set($iter, 0, $new-node.get-name);

			$dialog.hide;
			self.reload-tree;
		}
		default {
			say 'remove-child';
			$dialog.hide;
		}
	};
}

method on-add-parent-clicked(Str :$name, Int :$y) {
	if not %!selected<button> {
		Utils.show-dialog('Must select a node before you can add a parent.');
		return;
	}
	my Gnome::Gtk3::Dialog $dialog .= new(:build-id<tree-new-generation>);
	my Gnome::Gtk3::Entry $title .= new(:build-id<tree-title>);
	my Gnome::Gtk3::Entry $year .= new(:build-id<tree-year>);
	my Gnome::Gtk3::Entry $description .= new(:build-id<tree-description>);
	$title.set-text: $name ?? $name!!'';
	$year.set-text: $y ?? Int($y)!!'';
	$description.set-text: '';
	$dialog.show;
	given GtkResponseType($dialog.run) {
		when GTK_RESPONSE_OK {
			say 'add-parent';
			if $title.get-text.trim.chars < 1 {
				Utils.show-dialog('Name must be specified.');
				$dialog.hide;
				self.on-add-parent-clicked(:y($year.get-text.Int));
				return;
			}
			if $year.get-text.trim.chars < 1 {
				Utils.show-dialog('Year must be specified.');
				$dialog.hide;
				self.on-add-parent-clicked(:name($title.get-text));
				return;
			}
			try {
				my Int $yr = $year.get-text.Int;
				CATCH {
					default {
						Utils.show-dialog('Year must be an integer value.');
						$dialog.hide;
						self.on-add-parent-clicked(:name($title.get-text), :y($year.get-text.Int));
						return;
					}
				}
			}
			my @parents = [$!graph.root];
			my Node $new-node = $!graph.add-node(Node.new(title => $title.get-text.trim, year => $year.get-text.Int,
					description => $description.get-text), @parents);
			%!selected<button>.node.add-parents([$new-node]);
			$new-node.add-child: %!selected<button>.node;
			$new-node.add-parents: @parents;
			$!graph.root.add-child: $new-node;

			# add the new node to the drop down list from the root word list generation picker
			my $iter = $list-store.gtk-list-store-append;
			$list-store.gtk-list-store-set($iter, 0, $new-node.get-name);

			$dialog.hide;
			self.reload-tree;
		}
		default {
			say 'remove-parent';
			$dialog.hide;
		}
	};
}
