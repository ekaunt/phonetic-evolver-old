use Utils;
use Graph;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::EntryCompletion;
use Gnome::Gtk3::Image;

unit class CoinageRow is Gnome::Gtk3::Grid;

has Word $.Word;
our %.sections;

has Gnome::Gtk3::Label $.lbl-row-num;
has Gnome::Gtk3::Entry $.word;
has Gnome::Gtk3::Entry $.definition;
#has Gnome::Gtk3::Entry $.lexicon;
has Gnome::Gtk3::Grid  $.box;
has Gnome::Gtk3::Button $.btn-delete;

submethod new(|c) {
	self.bless(:GtkGrid, |c);
}
submethod BUILD(Node :$node, :$word, :$definition) {
	$!Word = $node.get-coined-word-by-id($word, $definition) if $word and $definition;

	$!word .= new;
	$!definition .= new;
	#$!lexicon .= new;
	$!box .= new;
	$!btn-delete .= new;

	# create all widgets
	$!lbl-row-num .= new(:text((++self.sections{$node}).Str));
	my Gnome::Gtk3::EntryCompletion $completion .= new;
	#$completion.set-model(Gnome::Gtk3::ListStore.new(:build-id('lexicon-list')));
	$completion.set-text-column(0);
	#$!lexicon.set-placeholder-text('Lexicon this word belong to');
	#$!lexicon.set-completion($completion);

	# set the widget values
	$!word.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$!definition.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	#$!lexicon.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	$!word.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	$!definition.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	#$!lexicon.set-icon-tooltip-text(GTK_ENTRY_ICON_SECONDARY, 'This entry <i>must</i> have content.');
	$!word.set-text($word//'');
	$!definition.set-text($definition//'');
	#$!lexicon.set-text($lexicon);
	self.on-entry-type(:_widget($!word));
	self.on-entry-type(:_widget($!definition));
	#self.on-entry-type(:_widget($!lexicon));
	$!lbl-row-num.set-width-chars(5);
	$!btn-delete.set-image(Gnome::Gtk3::Image.new(:icon-name<user-trash>));
	$!box.set-hexpand(True);
	$!box.set-column-homogeneous(True);
	$!box.set-column-spacing(10);


	# add entries to box
	$!box.attach($!word,       				0,0,1,1);
	$!box.attach($!definition, 				1,0,1,1);
	#$!box.attach($!lexicon, 	2,0,1,1);

	$!word.register-signal(self, 'on-entry-type', 'changed', :$node);
	$!definition.register-signal(self, 'on-entry-type', 'changed', :$node);
	#$!lexicon.register-signal(self, 'on-entry-type', 'changed', :$node);
	$!btn-delete.register-signal(self, 'on-coinage-delete-row', 'clicked', :$node);

	# add everything to the row
	self.attach($!lbl-row-num, 	0,0,1,1);
	self.attach($!box, 			1,0,1,1);
	self.attach($!btn-delete,	2,0,1,1);
}

method on-entry-type(:_widget($entry), Node :$node) {
	if $entry.get-text.trim.chars < 1 {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, 'gtk-dialog-warning');
	} else {
		$entry.set-icon-from-icon-name(GTK_ENTRY_ICON_SECONDARY, Nil);
	}

	# only execute this once it already finished loading
	if $node {
		if not Gnome::Gtk3::Button.new(:native-object(self.get-child-at(3, 0))).is-valid {
			say 'no button yet';
			# add the accept changes button
			my Gnome::Gtk3::Button $accept-changes .= new;
			$accept-changes.set-image(Gnome::Gtk3::Image.new(:icon-name<gtk-yes>));
			$accept-changes.register-signal(self, 'on-accept-changes-clicked', 'clicked', :$node);
			self.attach($accept-changes, 3, 0, 1, 1);
			self.show-all;
		}
	}
}

method on-coinage-delete-row(:_widget($button), :$node) {
	#my Gnome::Gtk3::Grid $box .= new(:native-object($row.get-child-at(1,0)));
	#$node.remove-coined-word(Gnome::Gtk3::Entry.new(:native-object($box.get-child-at(1,0))).get-text, Gnome::Gtk3::Entry.new(:native-object($box.get-child-at(0,0))).get-text);
	Utils.coinages-changes.emit(%(:$node, old-word=>$!Word, row-num=>$!lbl-row-num.get-text.Int));
	say "removing word {$!Word.word}";
	$node.remove-coined-word($!Word);
	--self.sections{$node};
	#$row.destroy;
	#self.renumber-coinages-columns($node);
}

method on-accept-changes-clicked(:_widget($button),  Node :$node) {
	say 'accept changes';
	# make sure none of the entries are empty
	if $!word.get-text.trim.chars < 1 {
		Utils.show-dialog("The entry at the column <i>Word</i> is empty, and it's not allowed to be empty.");
		return;
	} elsif $!definition.get-text.trim.chars < 1 {
		Utils.show-dialog("The column <i>Definition</i> is empty, and it's not allowed to be empty.");
		return;
	}

	# i duplicate $!Word here so that $!Word can be updated without updating $old-word
	say 'duplication word';
	my Word $old-word .= new(:word($!Word.word), :root-word($!Word.root-word), :definition($!Word.definition), :root-definition($!Word.root-definition), :path($!Word.path), :processed(True)) if $!Word;
	my Word $new-word;
	if Gnome::Gtk3::Button.new(:native-object(self.get-child-at(3, 0))).is-valid {
		if $!Word {
			say 'word exists';
			$new-word = $node.coin-or-update-word($!Word, $!word.get-text, $!definition.get-text);
			say "{$new-word.word}";
		} else {
			say 'word not exists';
			$new-word = $node.coin-word($!word.get-text, $!definition.get-text);
		}
	}
	say 'row-num: ',  $!lbl-row-num.get-text.Int-1;
	Utils.coinages-changes.emit(%(:$node, old-word=>$old-word//Word, new-word=>$new-word//Word, row-num=>($!lbl-row-num.get-text.Int-1)));

	sub recursively-update-roots($node) {
		say "updating {$old-word.root-word} to {$new-word.root-word} in {$node.get-name}";
		say "before: {$node.words».word}\n{$node.words».root-word}";
		$node.update-words-root($old-word, $new-word);
		say "after:  {$node.words».word}\n{$node.words».root-word}";

		for $node.children -> $child {
			recursively-update-roots($child);
		}
	}
	recursively-update-roots($node) with $old-word;

	# reload glossary after applying the outlines so that random cells dont get outlined (no idea why that happens, but it does, so wtvr)
	Utils.glossary.emit(%(:$node));

	$button.destroy;
}
