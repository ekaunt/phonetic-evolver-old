### DEPENDENCIES

```
choco install rakudostar;
choco install msys2;
# open msys
pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-toolchain base-devel

# ⚠ IMPORTANT! ⚠
#add C:\msys64\mingw64\bin to your PATH environment variable

zef install Grammar::ErrorReporting;
zef install Gnome::N;
zef install Gnome::Gtk3; #will take a while
zef install UUID;
```
