#use Grammar::Tracer;
use Terminal::ANSIColor;
grammar RootWordListParser {
	rule TOP {
		[
			| <comment>
			| <statements>
		]+
	}

	rule statements {
		<word-definition>+ %% \v
	}

	rule word-definition {
		<word=letters> '=' <meaning=letters> '=' <node> <comment>?
	}

	token letters {
		<[\wˌˈː.\h]>+
	}

	rule node {
		[<title>||<error("Missing generation's title/name.")>]
		['@'||<error("Year must be specified with an @ sign (before the year number)")>]
		[<year=number>||<error('The year MUST be specified')>]
	}
	token title {
		<-[^@\v]>+
	}
	token number { \d+ }

	rule comment {
		'--' <-[\v]>*
	}

	method error($msg) {
		my $parsed = self.target.substr(0, self.pos).trim-trailing;
		my $context = colored($parsed.substr($parsed.chars - 20 max 0), 'green') ~ colored('⏏', 'yellow') ~ colored(self.target.substr($parsed.chars, 20), 'red');
		my $line-no = $parsed.lines.elems;
		say "Cannot parse input: $msg\n"
			~ "at line $line-no, around \"" ~ $context ~ '"'
			~ "\n(error location indicated by ⏏)\n";
		#die if ;
		exit;
	}
}


#say RootWordListParser.parse('ˈnɐː.ˌtʰɑŋ = nathaniel = G1 @500

#b = a = G1 @10
#b = a = root @0');
