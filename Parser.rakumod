#use Grammar::Tracer;
use Grammar::ErrorReporting;
use Terminal::ANSIColor;

grammar Parser #`(does Grammar::ErrorReporting) {
	regex TOP {
		[ \h* <conversion>* <comment>? ]+ %% [ \v | ';' ]
	}
	rule conversion {
		<lhs=side> [<becomes>||<error("missing rhs")>] [<rhs=side>||<error("missing rhs")>] 
		[ <where> <word-beg=word-break>?
			#[<preceded=side>? <placeholder> <followed=side> || <preceded=side> <placeholder> || <placeholder>]
			[<when=side>||<error('when side is incomplete')>]
		<word-end=word-break>? 
		| <surround> <word-beg=word-break>? [<sides=side>||<error('surround side is incomplete')>] <word-end=word-break>? ]?
	}

	token side {
		[ \h+
			|| <placeholder>
			|| <features>
			|| <naked-class> # we cant have classes like nasal just sitting in the open
			|| <jump>
			|| <reference>
			|| <percentages>
			|| <empty>
			|| <no>
			|| <letter>
		]+
	}

	token features {
		| <brackets>
		| <braces>
		| <parenthesis>
		| <chevrons>
		| <slashes>
	}
	# the brackets represent all the features the changed sounds have in common
	rule brackets {
		'[' ~ ']' [
			# comma or whitespace delimited classes
			| [ <features> || <class> \s*?]+ %% ',' ||<error("incorrect class name")>
			| [ <features> || <class> \s*?]+ ||<error("incorrect class name")>
		]
	}
	# { } (Curly Braces): Indicate a logical-disjunction relationship of two expressions. For example, # The two expressions, ABD and AED and be written with curly braces as:
    # `A { B E } D`. A is followed by either B or E and then D.
	rule braces {
		| '{' ~ '}' ([ <word-break> || <side> \s*? ]+ %% ',')
		| '{' ~ '}' [ <word-break> || <side> \s*? ]+
	}
	# ( ) (Parenthesis): Indicate a logical-disjunction relationship of two expressions and an abbreviated version of the curly braces notation, while maintaining the same disjunctive relationship function. For example,
	# The two expressions, ABD and AD and be written with parentheses as:
    # `A ( B ) D`, B is optionally permitted to come between A and D.
	rule parenthesis {
		| '(' ~ ')' [ <word-break> || <side> \s*? ]+ %% ','
		| '(' ~ ')' [ <word-break> || <side> \s*? ]+
	}
	# ANGLED  BRACKET  NOTATION:    < >
	# Used  with  rules  that involve dependencies between  two  feature  specifications  by  way  of adding a condition to the rule of the form
	#“if a , then b ”
	rule chevrons {
		| '<' ~ '>' [ <features> || <class> || <letter> \s*? ]+
		| '<' ~ '>' [ <features> || <class> || <letter> \s*? ]+ %% ','
	}

	# everything in slashes is interpreted as a literal character
	rule slashes {
		'/' ~ '/' [<letter>+ \h* [<letter>+]?]+
	}

	token sign {
		<[-+]>
	}
	token surround { '//' }
	token where {
		'/' | 'when' | 'where'
	}
	token becomes {
		'->' | '=>' | '→' | '>' | 'becomes' | 'is' | '='
	}
	regex letter {
		<[ˌˈ\wː] - [\v_;]>+ [ <syllable> <[ˈ\wː] - [\n_;]>+ ]*
	}
	token word-break { '#' }
	token no { '!' | 'not' }
	token jump { '…' | '...' }
	token placeholder { '_'+ | '–'|'—' }
	token empty { '∅' | 'silent' }
	token number { \d+ }
	token syllable { '.' | '$' }

	rule percentages {
		#'%' ~ '%' [<path=node>+ ';' [<letter>||<error("Word must be specified.")>] <origin=node>]
		'%' ~ '%' [<letter>||<error("Word must be specified.")>]
	}
	rule node {
		'^'
		[<title>||<error("Missing generation's title/name.")>]
		['@'||<error("Year must be specified with an @ sign (before the year number)")>]
		[<year=number>||<error('The year MUST be specified')>]
	}
	token title {
		<-[^@\v]>+
	}

	
	token reference {
		| [<number> #`(| <[αβγδεζηθικλμνξοπρςτυφχψω]>) ] [ ':' [ <class> | <features> ] ]?
		| '@'
	}

	regex class {
		$<class>=(<sign>?<group><number>?<.ws>)+ [ <superscript>? <subscript> | <subscript> <superscript>? ]?
	}
	token subscript   { <[₀₁₂₃₄₅₆₇₈₉]>+ }
	token superscript { <[⁰¹²³⁴⁵⁶⁷⁸⁹]>+ }
	proto token group {*}
		token group:sym<X> 	{ 'X'|'*' } # stands for literally anything
		token group:sym<consonants> 	{ 'C' | 'consonant' }
		token group:sym<vowels> 		{ 'V' | 'vowel' }
		token group:sym<syllable> 		{ 'S' | 'syllable' }
		token group:sym<fricative> 		{ 'F' | 'fricative' | "fric" }
		token group:sym<affricate> 		{ "A" | "affricate" }
		token group:sym<laryngeal> 		{ "L" | "laryngeal" }
		token group:sym<semivowel> 		{ "W" | "semivowel" }
		token group:sym<sibilant> 		{ "sibilant" | "sib" }
		token group:sym<approximant> 	{ "approximant" | "approx" }
		token group:sym<labial> 		{ "P" | "labial" | "lab" }
		token group:sym<bilabial> 		{ <sym> }
		token group:sym<dental> 		{ "dental" | "dent" }
		token group:sym<palatal> 		{ "palatal" | "pal" }
		token group:sym<plosive> 		{ "stop" | "plosive" | "O" | "obstruent" | "obstr" }
		token group:sym<alveolar> 		{ <sym> }
		token group:sym<stressed> 		{ <sym> }
		token group:sym<syllabic> 		{ "syllabic" | "syll" }
		token group:sym<short> 			{ <sym> }
		token group:sym<spread-glottis> { "spread-glottis" | "spread glottis" }
		token group:sym<aspirated> 		{ "aspirated" | "asp" }
		token group:sym<tap> 			{ 'flap' | 'tap' }

		# major group features
		token group:sym<sonorant> 		{ 'R' | 'sonorant' | "son" | "resonant" }
		token group:sym<velar> 			{ 'velar' | "vel"}
		token group:sym<liquid> 		{ 'L' | 'liquid' | "liqu" }
		token group:sym<vocalic> 		{ "vocalic" | "voc" }
		token group:sym<consonantal> 	{ "consonantal" | "cons" }
		#cavity features
		token group:sym<coronal> 	{ <sym> }
		token group:sym<anterior> 	{ <sym> }
			# tongue body features
			token group:sym<close> 	{ "close" | "high" | "hi" }
			token group:sym<near-close> 	{ "near-close" | "near-high" }
			token group:sym<close-mid> 	{ "close-mid" | "high-mid" }
			token group:sym<mid> 	{ <sym> }
			token group:sym<open-mid> 	{ "open-mid" | "low-mid" }
			token group:sym<near-open> 	{ "near-open" | "near-low" }
			token group:sym<open> 	{ "open" | "low" | "lo" }
			token group:sym<back> 	{ 'B' | "back" }
			token group:sym<center> 	{ <sym> }
			token group:sym<front> 	{ "E" | "front" }
		token group:sym<rounded> 	{ <sym> }
		token group:sym<unrounded> 	{ <sym> }
		token group:sym<distributed> 	{ <sym> }
		token group:sym<covered> 	{ <sym> }
		token group:sym<glottal> 	{ "glottal" | "glott" }
			#secondary apertures
			token group:sym<nasal> 	{ 'N' | 'nasal' | 'nas' }
			token group:sym<nasalized> 	{ <sym> }
			token group:sym<centralized> 	{ <sym> }
			token group:sym<mid-centralized> 	{ <sym> }
			token group:sym<rhoticized> 	{ <sym> }
			token group:sym<labialized> 	{ <sym> }
			token group:sym<palatalized> 	{ <sym> }
			token group:sym<velarized> 	{ <sym> }
			token group:sym<pharyngealized> 	{ <sym> }
			token group:sym<lateral> 	{ <sym> }
		# manner of articulation features
		token group:sym<continuant> 	{ "Z" | "continuant" | "cont" }
			#release features
			token group:sym<primary release> 	{ <sym> }
			token group:sym<secondary release> 	{ <sym> }
		# supplementary movements
		token group:sym<click> 	{ <sym> }
		token group:sym<ejective> 	{ <sym> }
		token group:sym<tense> 	{ <sym> }
		#source features
		token group:sym<voiced> 	{ 'voiced' | 'voice' }
		token group:sym<voiceless> 	{ <sym> }
		token group:sym<strident> 	{ <sym> }
			#prosodic features
			token group:sym<stress> 	{ <sym> }
				# pitch
				token group:sym<high-pitch> 	{ <sym> }
				token group:sym<low-pitch> 	{ <sym> }
				token group:sym<elevated-pitch> 	{ <sym> }
				token group:sym<rising-pitch> 	{ <sym> }
				token group:sym<falling-pitch> 	{ <sym> }
				token group:sym<concave-pitch> 	{ <sym> }
			token group:sym<length> 	{ <sym> }

	
	regex naked-class {
		$<class>=(<sign>?<group=naked-group><number>?<.ws>) [ <superscript>? <subscript> | <subscript> <superscript>? ]?
	}
	proto token naked-group {*}
		token naked-group:sym<X> 	{ 'X'|'*' } # stands for literally anything
		token naked-group:sym<consonants> 	{ 'C' }
		token naked-group:sym<vowels> 		{ 'V' }
		token naked-group:sym<syllable> 		{ 'S' }
		token naked-group:sym<fricative> 		{ 'F' }
		token naked-group:sym<affricate> 		{ "A" }
		token naked-group:sym<laryngeal> 		{ "L" }
		token naked-group:sym<semivowel> 		{ "W" }
		token naked-group:sym<plosive> 		{ "O" }
		token naked-group:sym<sonorant> 		{ 'R' }
		token naked-group:sym<liquid> 		{ 'L' }
		token naked-group:sym<back> 			{ 'B' }
		token naked-group:sym<front> 			{ "E" }
		token naked-group:sym<nasal> 			{ 'N' }
		token naked-group:sym<continuant> 	{ "Z" }

	token comment {
		'--' \V*
	}

	regex ws {
		<!ww> \h*
	}

	method error($msg) {
		my $parsed = self.target.substr(0, self.pos).trim-trailing;
		my $context = colored($parsed.substr($parsed.chars - 20 max 0), 'green') ~ colored('⏏', 'yellow') ~ colored(self.target.substr($parsed.chars, 20), 'red');
		my $line-no = $parsed.lines.elems;
		say "Cannot parse input: $msg\n"
			~ "at line $line-no, around \"" ~ $context ~ '"'
			~ "\n(error location indicated by ⏏)\n";
		#die if ;
		exit;
	}
}

#say Parser.parse('a → e / _ b');
#say Parser.parse('a → e / b _');
#say Parser.parse('a → e / # _');
#say Parser.parse('{b,m} → p');
#say Parser.parse('[aspirate] -> e');
#say Parser.parse('[aspirated] ');
#say Parser.parse('[aspirated] -> ');
#say Parser.parse('a -> e / #[sibliant]');
#say Parser.parse('a -> e /');
#say Parser.parse('CC → 2 1');
#say Parser.parse('[nasal] → z');
#say Parser.parse('nasal → z');
#say Parser.parse('N → z');
#say Parser.parse('/nasal/ → z');
#say Parser.parse('/nasal alveolar/ → z');
#say Parser.parse(' 
#-- top of file

#um → ũ / __#   --filiũ
#u → V		--filiu
#u → o / __#                   --filio
#li → lʲ / _ V	--filʲo
#');
#say Parser.parse('
#--- scf - sound changes file (format)

#^ G1 @0
#lʲ → j -- comment
#^G1.5 @500 ^G1
#ç → x
 #^ G 2 @1000 ^G1
#pt → p

#^G3 @ 2000  ^ G 2
#[stop] > [fricative]
#');
#say '############################';
