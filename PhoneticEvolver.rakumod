#use lib '.';
use Utils;
use Parser;
use Actions;
use NodesParser;
use NodesActions;
use RootWordListParser;
use RootWordListActions;
use Graph;
use Terminal::ANSIColor;

#unit sub MAIN (
	#:s(:$sound-changes) where *.IO.d, 	#= a folder containing all sound changes files
	#:w(:$word-list), 		#= the file with the list of words to change
	#Str :$change, 			#= a sound change list to test
	#Str :$word, 			#= a word list to test
#);

class PhoneticEvolver {
#	has $sound-changes where *.IO.d; 	#= a folder containing all sound changes files
#	has $word-list where *.IO.f; 		#= the file with the list of words to change
#	has Str $change=''; 			#= a sound change list to test
#	has Str $word=''; 			#= a word list to test

	has Graph $.graph = Graph.new;

	#if ($!change and $!word) {
		##$graph.cur-gen = $!graph.root;
		#my @result;
		#for $!word.words -> $w {
			#@result.append: Parser.parse($change, actions=>Actions.new(word=>$w, :$graph)).made;
		#}
		#say @result.join(' ');
	#}

	#if $!sound-changes and not $!word-list {
		#die 'if --sound-changes is specified, then --word-list must also be';
	#}
	#if $!word-list and not $!sound-changes {
		#die 'if --word-list is specified, then --sound-changes must also be';
	#}

	#method parse-generations(IO::Path :$sound-changes) {
		#my $snd-chngs = $sound-changes // $!sound-changes;
		#my $sc = $snd-chngs.IO.dir».slurp;
		## loop through all sound change files
		#for $sc {
			#NodesParser.parse($_, actions=>NodesActions.new(:$!graph));
		#}
		## quit program if theres an infinite loop in the nodes
		#my $cyclic = $!graph.is-cyclic;
		#error("Generation \"{$cyclic[0].get-name}\" creates an infinitely repeating loop with itself by having generation \"{$cyclic[1].get-name}\" as a parent.") if $cyclic;
	#}

	method parse-pef(Array:D $pef) {
		for @($pef) -> $generation {
			my Node $node = $!graph.get-node($generation<name>, $generation<year>, :create);
			$node.description = $generation<description>//'';

			my Node @parents;
			for @($generation<parents>) -> $/ {
				my Node $parent = $!graph.get-node($<name>, $<year>, :create);
				$parent.add-child: $node;
				@parents.append: $parent;
			}
			$node.add-parents(@parents);

			#save the coinages
			for @($generation<coinages>) -> $/ {
#				say 'processed: ', $<processed>;
				$node.coin-word($<word>, $<definition>, $<processed>//True);
			}

			#save the words
			for @($generation<glossary>) -> $/ {
#				say $<path>;
				$node.add-word($<word>, $<definition>, $<root-word>, $<root-definition>, @($<path>).map({$!graph.get-node(.<name>, .<year>, :create)}), $<processed>//True, $<will-root-word-be-changed>//False, $<will-root-definition-be-changed>//False);
			}

#			say $generation<sound-changes>;
			$node.sound-changes = $generation<sound-changes>;
		}
	}

	method parse-sound-changes(Word $word) {
		my Node @path;

		sub parse-sound-changes-recursive(Node $node, Word $word) {
			say ($word.word, $node.get-name, $node.sound-changes).join(' ');
			my Str $parsed-word = Parser.parse($node.sound-changes, actions=>Actions.new(word=>$word.word, :$!graph)).made;
			say 'parsed word: ', $parsed-word;
			my Word $new-word = $node.add-or-update-word($word.root-word, $word.root-definition, @path, $parsed-word, $word.definition, True);

			# update the glossary with the current word
			Utils.glossary.emit(%(:$node, old-word=>$word, :$new-word));

			# cascade the word through all child nodes
			for $node.children -> $child {
				@path.push: $child;
				parse-sound-changes-recursive($child, $new-word);
			}

#			$word.processed = True;
			@path.pop;
		}

		@path.push: $_ for $word.path[0..$word.starting-node];
		parse-sound-changes-recursive(@path[*-1], $word);
		say 'DONE';

		#$nodes = $!graph.nodes-parent;
		#$nodes();
		#my Str $result;
		#while $nodes() -> $_ {
			##say $_;
			#my ($parent, $node) = $_;
			#$result ~= "[{$parent.title} {'"'~$parent.description~'"' if $parent.description}\\n{$parent.words.map({.key, .value{'definition'}, .value{'path'}}).join('\n')}] -> [{$node.title} {'"'~$node.description~'"' if $node.description}\\n{$node.words.map({.key, .value{'definition'}, .value{'path'}}).join('\n')}]";
		#}
		#spurt "graph", $result;
	}

	#method parse-glossary(Hash $glossary) {
		#sub parse-generation(Hash $generation) {
			#my Node $node = $!graph.get-node($generation<generation-name>, $generation<generation-year>);

			##save the words
			#for @($generation<words>) -> $/ {
				#say $<path>;
				#$node.add-word($<word>, $<definition>, $<root-word>, @($<path>).map({$!graph.get-node(.[0], .[1])}), :dont-refresh);
			#}

			#for @($generation<children>) -> $child {
				#parse-generation($child) if $child;
			#}
		#}
		#parse-generation($glossary);
	#}
}


DOC CHECK {
	my @tests1 = [
		# alabama
		["a > e", "elebeme"],
		["a → e", "elebeme"],
		["a → zd", "zdlzdbzdmzd"],
		["a -> b; l -> m", "bmbbbmb"],
		["a > e / _ b", "alebama"],
		["a > e / b _", "alabema"],
		["a > e / a l _ b", "alebama"],
		["a > e / #_", "elabama"],
		["a > e / #_;l→m;", "emabama"],
		["l > e / #a _", "aeabama"],
		["m > e / _ a#", "alabaea"],
		["l > e / #_ a", "alabama"],
		["a > g / c _#", "alabama"],
		["a > g / _#", "alabamg"],
		["ba > ab ", "alaabma"],
		['{b,m} > p', "alapapa"],
		['a > e / _{b,m}', "alebema"],
		['V > e', "elebeme"],
		['C > z', "azazaza"],
		['V > e / C _', "alebeme"],
		['V > e / C _ m', "alabema"],
		["ba -> gz / la _", "alagzma"],
		["ba -> gz / #ala _", "alagzma"],
		["ba -> gz / #al _", "alabama"],
		["l…b → b…l ", "abalama"],
		["l…b → b…l / a ___ ", "abalama"],
		["l…b → z…q ", "azaqama"],
		["l…b → b...l / _a ", "abalama"],
		["l…b → b...l / _b ", "alabama"],
		["al…am → am…ab / #_", "amababa"],
		["l…b → b...l / _ma ", "alabama"],
		["l…a → z…q ", "azabqma"],
		["a…m → z…q", "alzbaqa"],
		["a…m → z…q / #_", "zlabaqa"],
		["a…b → z…q / #_ ", "zlaqama"],
		["a…b → z…q / #_ ; qa -> bz/_z", "zlaqama"],
		["a…b → z…q / #_ ; qa -> bz/_ma#", "zlabzma"],
		["a…m → z…q / #_ ; ba -> gz / #zla _", "zlagzqa"],
		['C > ∅', "aaaa"],
		['a→z/{l,m} _;C>∅/a _', "azbaz"],
	];

	my @tests2 = [
		#topgord
		['o > e / _ C₀', "tepgerd"],
		['o > e / _ [C₀]', "tepgerd"],
		['o > e / _ C₀#', "topgerd"],
		['o > e / _ C₁³', "tepgerd"],
		['o > e / _ C³₁', "tepgerd"],
		['o > e / _ C₂', "tepgerd"],
		['o > e / _ C₂#', "topgerd"],
		['o > e / #C _', "tepgord"],
		['o > e / _ C₃', "topgord"],
		['o > e / _ C₀g', "tepgord"],
		['o > e / _ C₀d', "topgerd"],
		['o > e // g', "topgerd"], # when neighboring 'g'
		['p>g;o > e // g', "teggerd"],
		['r > g // d #', "topgogd"],
		['r > g // z #', "topgord"],
		['o > e / X₂_', "topgerd"], # when preceded by 2 of anything
		['o > e / _ (p)', "tepgerd"],
		['o > e / _(C)', "tepgerd"],
		['o > e / _(C)#', "topgord"],
		['o > e / _ (C C)', "tepgerd"],
		['o > e / _(C)(C)#', "topgerd"],
		['∅ > at / #C _', "tatopgord"],
		['o => e / _ (C C)#', "topgerd"],
	];

	my @tests3 = [
		["N > z", "zztʰaz"],
		["[+nasal] > z", "zztʰaz"],
		["nasal alveolar > z", "nãtʰaŋ"],
		["/nasa/ > z", "nãtʰaŋ"],
		["/ã/ > /z̃/", "nz̃tʰaŋ"],
		["/nasal alveolar/ > z", "nãtʰaŋ"],
		["n > [+stop]", "d̃ãtʰaŋ"],
		["ŋ → g; n > [+stop]", "d̃ãtʰag"],
		["[+nasal] > [+stop]", "d̃ãtʰag̃"],
		["[+nasal] > [+stop] / _#", "nãtʰag̃"],
		["[+voiced] > z", "zztʰzz"],
		["[-voiced] > [velar]", "nãkʰaŋ"],
		["[voiced -vowel] > [voiceless]", "n̥ãtʰaŋ̥"],
		["[+nasal] > [+stop]; [voiced] > [voiceless]", "t̃ḁ̃tʰḁk̃"],
		["[voiced] > [voiceless]", "n̥ḁ̃tʰḁŋ̥"],
		["[+aspirated] > [voiced sibilant]", "nãzʰaŋ"],
		["[+aspirated] > [voiced sibilant fricative]", "nãzʰaŋ"],
		['{N, O} > z', 'zzzaz'],
		["tʰ → z", "nãzaŋ"],
		["[+stop] > z", "nãzaŋ"],
		["[+stop] > [voiced alveolar sibilant]", "nãzʰaŋ"],
		["[+nasal] > [voiced alveolar sibilant]", "z̃ãtʰaz̃"],
		["a -> e", "nãtʰeŋ"],
		["[+front] -> e", "netʰeŋ"],
		["[+front] -> [close-mid]", "nẽtʰeŋ"],
		["[-cont -nasal] > [+voice] / [vowel] _ [+vowel]", "nãdʰaŋ"],
		["[+aspirated] > [-aspirated]", "nãtaŋ"],
		["ŋ > [-nasal]", "nãtʰag"],
		["[+aspirated] > z", "nãzaŋ"],
		["t -> z", "nãzʰaŋ"],
		["[velar] -> [alveolar voiced sibilant]", "nãtʰaz̃"],
		["ŋ -> [alveolar voiced sibilant]", "nãtʰaz̃"],
		["[+aspirated] > [velar]", "nãkʰaŋ"],
		["[+stop] > [affricate]", "nãt͡sʰaŋ"],
		["[+stop] > [palatalized]", "nãtʰʲaŋ"],
		["[+V] > [close]", "nĩtʰiŋ"],
		["[+stop] > [palatalized]; V → [close]", "nĩtʰʲiŋ"],
		["O > [affricate]; [affricate] → [palatalized]", "nãt͡sʰʲaŋ"],
		["[alveolar nasal] > z", "zãtʰaŋ"],
		["[nasal -alveolar] > z", "nztʰaz"],
		["[-alveolar nasal] > z", "nztʰaz"],
	];

	my @tests4 = [
		['CC > 2 1', "togpodr"],
		['CC > @@', "topgpgordrd"],
		['X₁ > @@', "topgordtopgord"],
		['X > @@', "ttooppggoorrdd"],
		['CVC > @@', "toptopgorgord"],
	];

	my @tests5 = [
		# æftɚ
		['f * > 2:F', 'æθ̠tɚ'],
	];

	my @tests6 = [
		# mitigate
		['t > d / V1 _ V1', 'midigate'],
		['∅ > C1 V1 / #_ C1 V1', 'mimitigate'],
		['∅ > V1 C1 / #_ C1 V1', 'immitigate'],
		['∅ > C1 C2 / _ C1 V2 C2 V1', 'mtmitigtgate'],
		["[+stop] > [+nasal] / V1 _ V1", "dãtʰag"],
		["V -> e", "metegete"],
	];
	my @tests7 = [
		["[velar] -> [alveolar voiced sibilant]", "nãtʰaz̃"],
	];
	my @tests8 = [
		["[+alveolar +stop] → ɾ / [+vowel +stress] _ [vowel]", "bːɾəl"],
	];
	my @tests9 = [
		["[+affricate] → [+palatalized]", "nãt͡sʰʲaŋ"],
	];
	my @tests10 = [
		["[velar approximant] → z", "zazeziɫoz"],
		["[velar approximant] → [alveolar]", "ɹaɹeɹ̥iɫol"],
	];
	my @tests105 = [
		["[voiced fricative] → [stop]", "c"],
	];
	my @tests11 = [
		['V → ∅ / _#', 'nisti' ],
	];
	my @errors = [
		['C > [nasalized] / _ # ', 'topgord'],
		['C > 1 / _ _ # ', 'topgord'],
		['C > {b,m} / _ # ', 'topgord'],
	];

	#run-test("alabama", @tests1);
	#run-test("topgord", @tests2);
	#run-test("nãtʰaŋ", @tests3);
	#run-test("topgord", @tests4);
	#run-test("æftɚ", @tests5);
	#run-test("mitigate", @tests6);
	#run-test("nãtʰanˠ", @tests7);
	#run-test("biːtəl", @tests8);
	#run-test("nãt͡sʰaŋ", @tests9);
	#run-test("ɰaweʍiɫoʟ", @tests10);
	run-test("c", @tests105);
	#run-test("nɪt͡siː", @tests11);
	#run-test('topgord', @errors);

	sub run-test($word, @tests) {
		say "#####################################";
		say $word;
		for @tests -> $test {
			my $parse = Parser.parse($test[0], actions=>Actions.new(:$word, graph=>Graph.new));
			#my $eval = Evaluator.new(word=>$word);
			#$eval.eval($parse.made);
			#say $parse;
			if $parse.made eq $test[1] {
				say colored("OK","black on_green"), " $parse = $test[1]";
			} else {
				say colored("NOT OK","white on_red"), " $parse = {$parse.made}, ≠ $test[1]";
			}
		}
	}
}

