use Graph;
use Terminal::ANSIColor;

class RootWordListActions {
	has Graph $.graph is required;
	has Array[Pair] $.words;

	method TOP($/) {
		$/.make: $!words;
	}
	
	method statements($/) {
		#say $/;
		for $<word-definition> -> $/ {
			say $/;
			my $node = $!graph.get-node($<node><title>.made, +$<node><year>);
			$node.coin-word($<word>.made, $<meaning>.made);
			$!words.append: $node.cur-word;
			say $!words;
		}
	}
	method letters($/) { $/.make: $/.trim }

	method node($/) {
		my $node = $!graph.get-node($<title>.made, +$<year>);
		error($/, "No generation \"$<title> \@$<year>\"") if not $node;
	}
	method title($/) {
		$/.make: $/.trim;
	}

	sub error(Match $/, Str $msg) {
		my $parsed = $/.target.substr(0, $/.pos).trim-trailing;
		my $context = colored($parsed.substr($parsed.chars - 20 max 0), 'green') ~ colored('⏏', 'yellow') ~ colored($/.target.substr($parsed.chars, 20), 'red');
		my $line-no = $parsed.lines.elems;
		say "Cannot parse input: $msg\n"
			~ "at line $line-no, around \"" ~ $context ~ '"'
			~ "\n(error location indicated by ⏏)\n";
		#die if ;
		exit;
	}
}
