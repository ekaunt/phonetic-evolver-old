use Utils;
use Terminal::ANSIColor;

our %aspects is export =
  "rounded"      => <y ʏ ø œ ɶ ʉ ɵ ə ɞ ɐ ä̹ u ʊ o ɔ ɒ>,
  "unrounded"    => <i ɪ e ɛ æ a ɨ ɘ ə ɜ ɐ ä ʊ̜ ɯ ɤ ʌ ɑ>,
  "front"        => <i y i̞ y̞ e ø e̞ ø̞ ɛ œ æ æ̹ a ɶ>,
  "near-front"   => <ɪ ʏ>,
  "central"      => <ɨ ʉ ɨ̞ ʉ̞ ɘ ɵ ə ɜ ɞ ɐ ä ä̹>,
  "near-back"    => <ʊ̜ ʊ>,
  "back"         => <ɯ u ɯ̞ u̞ ɤ o ɤ̞ o̞ ʌ ɔ ɑ ɒ ɑ̝ ɒ̝>,
  "close"        => <i y i̙ y̙ ɨ ʉ ɯ̘ u̘ ɯ u>,
  "near-close"   => <i̞ y̞ ɪ ʏ ɨ̞ ʉ̞ ʊ̜ ʊ ɯ̞ u̞>,
  "close-mid"    => <e ø e̙ ø̙ ɘ ɵ ɤ̘ o̘ ɤ o>,
  "mid"          => <e̞ ø̞ ə ɤ̞ o̞>,
  "near-open"    => <æ æ̹ ɐ ɑ̝ ɒ̝>,
  "open-mid"     => <ɛ œ ɜ ɞ ʌ ɔ>,
  "open"         => <a ɶ ä ä̹ ɑ ɒ>,
  #"tense"        => <i u y o>,

  "voiced"       => <m ɱ n ɳ ɲ ŋ ɴ b d d ɟ g ɢ z ʒ ʐ ʑ β v ð ʝ ɣ ʁ ʕ ɦ ʋ ɹ ɻ j ɰ ⱱ ɾ ɽ ʙ r ʀ ʢ ɮ l ɫ ɭ ʎ ʟ ɺ ɥ w d̪z̪ dz dʒ d̠ʒ ɖʐ dʑ bβ b̪v d̪ð dɹ̝ d̠ɹ̠˔ ɟʝ gɣ ɢʁ ʡʢ dɮ ɟʎ̝ gʟ̝>.Array,
  "voiceless"    => <p t ʈ c k q ʡ ʔ s ʃ ʂ ɕ ɸ f θ ç x χ ħ h ʜ ɬ l̥ ʍ t̪s̪ ts tʃ t̠ʃ ʈʂ tɕ pɸ p̪f t̪θ tɹ̝̊ t̠ɹ̠̊˔ cç kx qχ ʔh tɬ ʈɭ̊˔ cʎ̝̊ kʟ̝̊>,
  "bilabial"     => <m̥ m p b ɸ β β̞ ⱱ̟ ʙ̥ ʙ pɸ bβ p̪f b̪v>,
  "labiodental"  => <ɱ p̪ b̪ ⱱ f v ʋ>,
  "linguolabial" => <n̼ t̼ d̼ θ̼ ð̼ ɾ̼>,
  "dental"       => <t̪ d̪ θ ð ð̞ l̪ t̪s̪ d̪z̪ t̪θ d̪ð>,
  "alveolar"     => <n̥ n t d s z θ̠ ð̠ ɹ ɾ̥ ɾ r̥ r ɬ l̥ ɮ l ɫ ɺ̥ ɺ ts dz tɹ̝̊ dɹ̝ tɬ dɮ>,
  "postalveolar" => <ʃ ʒ ɹ̠̊˔ ɹ̠˔ ɹ̠ r̠ l̠ tʃ t̠ʃ dʒ d̠ʒ t̠ɹ̠̊˔ d̠ɹ̠˔>,
  "retroflex"    => <ɳ̊ ɳ ʈ ɖ ʂ ʐ ɻ˔ ɻ ɽ̊ ɽ ɭ̊˔ ɭ˔ ɭ ɭ̥̆ ɭ̆ ʈʂ ɖʐ ɽ̊r̥ ɽr 	ʈɭ̊˔>,
  "palatal"      => <ɲ̊ ɲ ɲ̊ ɲ ɕ ʑ ç ʝ j ʎ̝̊ ʎ̝ ʎ ʎ̆ ɥ tɕ dʑ cç ɟʝ cʎ̝̊ ɟʎ̝>,
  "velar"        => <ŋ̊ ŋ k g x ɣ ɰ ʟ̝̊ ʟ̝ ʟ ʟ̆ ʍ w kx gɣ kʟ̝̊ gʟ̝>,
  "uvular"       => <ɴ q ɢ χ ʁ ʁ̞ ʀ̥ ʀ ɢ̆ ʟ̠ qχ ɢʁ>,
  "pharyngeal"   => <ʡ ħ ʕ ʡ̆ ʜ ʢ ʡʢ>,
  "glottal"      => <ʔ h ɦ ʔ̞ ʔh>,
  "plosive"      => <p b p̪ b̪ t̼ d̼ t̪ d̪ t d ʈ ɖ c ɟ k g q ɢ ʡ ʔ t̪s̪ d̪z̪ ts dz tʃ t̠ʃ dʒ d̠ʒ ʈʂ ɖʐ tɕ dʑ pɸ bβ p̪f b̪v t̪θ d̪ð tɹ̝̊ dɹ̝ t̠ɹ̠̊˔ d̠ɹ̠˔ cç ɟʝ kx gɣ qχ ɢʁ ʡʢ ʔh>,
  "nasal"        => <m̥ m ɱ n̼ n̥ n ɳ̊ ɳ ɲ̊ ɲ ŋ̊ ŋ ɴ>,
  "trill"        => <ʙ̥ ʙ r̥ r r̠ ʀ̥ ʀ ʜ ʢ>,
  "tap"          => <ⱱ̟ ⱱ ɾ̼ ɾ̥ ɾ ɽ̊ ɽ ɢ̆ ʡ̆ ɺ̥ ɺ ɭ̥̆ ɭ̆ ʎ̆ ʟ̆>,
  "fricative"    => <s z ʃ ʒ ʂ ʐ ɕ ʑ ɸ β f v θ̼ ð̼ θ ð θ̠ ð̠ ɹ̠̊˔ ɹ̠˔ ɻ˔ ç ʝ x ɣ χ ʁ ħ ʕ h ɦ ɬ l̥ ɮ ɭ̊˔ ɭ˔ ʎ̝̊ ʎ̝ ʟ̝̊ ʟ̝ t̪s̪ d̪z̪ ts dz tʃ t̠ʃ dʒ d̠ʒ ʈʂ ɖʐ tɕ dʑ pɸ bβ p̪f b̪v t̪θ d̪ð tɹ̝̊ dɹ̝ t̠ɹ̠̊˔ d̠ɹ̠˔ cç ɟʝ kx gɣ qχ ɢʁ ʡʢ ʔh>,
  "affricate"    => <t̪s̪ d̪z̪ ts dz tʃ t̠ʃ dʒ d̠ʒ ʈʂ ɖʐ tɕ dʑ pɸ bβ p̪f b̪v t̪θ d̪ð tɹ̝̊ dɹ̝ t̠ɹ̠̊˔ d̠ɹ̠˔ cç ɟʝ kx gɣ qχ ɢʁ ʡʢ ʔh tɬ dɮ ʈɭ̊˔ cʎ̝̊ ɟʎ̝ kʟ̝̊ gʟ̝>,
  "sibilant"     => <s z ʃ ʒ ʂ ʐ ɕ ʑ>, # t̪s̪ d̪z̪ ts dz tʃ t̠ʃ dʒ d̠ʒ ʈʂ ɖʐ tɕ dʑ>,
  "lateral"      => <ɬ l̥ ɮ ɭ̊˔ ɭ˔ ʎ̝̊ ʎ̝ ʟ̝̊ ʟ̝ l̪ l l̠ ɭ ʎ ʟ ʟ̠ ɺ̥ ɺ ɭ̥̆ ɭ̆ ʎ̆ ʟ̆ ɫ tɬ dɮ ʈɭ̊˔ cʎ̝̊ ɟʎ̝ kʟ̝̊ gʟ̝>,
  "approximant"  => <β̞ ʋ ð̞ ɹ ɹ̠ ɻ j ɰ ʁ̞ ʔ̞ l̪ l l̠ ɭ ʎ ʟ ʟ̠ ɥ ʍ w ɫ>,
  "click"        => <ʘ ǀ ǃ ǂ ǁ>,
  "implosive"    => <ɓ ɗ ʄ ɠ ʛ>,
  "labial"       => <ɥ w>,
	"nasalized" 			=> ["\c[COMBINING TILDE]", ],
	"labialized" 			=> ["\c[MODIFIER LETTER SMALL W]", ],
	"palatalized" 			=> ["\c[MODIFIER LETTER SMALL J]", ],
	"velarized" 			=> ["\c[MODIFIER LETTER SMALL GAMMA]",  'ɫ', ],
	"pharyngealized" 		=> ["\c[MODIFIER LETTER SMALL REVERSED GLOTTAL STOP]", ],
	"aspirated" 			=> ["\c[MODIFIER LETTER SMALL H]",]
  ;

my %vowel-diacritics = 
	"nasal" 			=> "\c[COMBINING TILDE]",
	"voiceless" 		=> "\c[COMBINING RING BELOW]",
	"breathy" 			=> "\c[COMBINING DIAERESIS BELOW]",
	"creaky" 			=> "\c[COMBINING TILDE BELOW]",
	"advanced" 			=> "\c[COMBINING PLUS SIGN BELOW]",
	"retracted" 		=> "\c[COMBINING MINUS SIGN BELOW]",
	"centralized" 		=> "\c[COMBINING DIAERESIS]",
	"mid-centralized"	=> "\c[COMBINING X ABOVE]",
	"raised" 			=> "\c[COMBINING UP TACK BELOW]",
	"lowered" 			=> "\c[COMBINING DOWN TACK BELOW]",
	"rounded" 			=> "\c[COMBINING RIGHT HALF RING BELOW]",
	"unrounded" 		=> "\c[COMBINING LEFT HALF RING BELOW]",
	"aspirated" 		=> "\c[MODIFIER LETTER SMALL H]",
	"front" 			=> "\c[COMBINING LEFT TACK BELOW]",
	"back" 				=> "\c[COMBINING RIGHT TACK BELOW]",
	"long" 				=> "\c[MODIFIER LETTER TRIANGULAR COLON]",
	"short" 			=> "\c[MODIFIER LETTER HALF TRIANGULAR COLON]",
	"extra-short" 		=> "\c[COMBINING BREVE]",
	;
my %consonant-diacritics = 
	"affricate" 		=> "\c[COMBINING DOUBLE INVERTED BREVE]",
	"nasal" 			=> "\c[COMBINING TILDE]",
	"voiceless" 		=> "\c[COMBINING RING BELOW]",
	"voiced" 			=> "\c[COMBINING CARON BELOW]",
	"breathy" 			=> "\c[COMBINING DIAERESIS BELOW]",
	"creaky" 			=> "\c[COMBINING TILDE BELOW]",
	"dental" 			=> "\c[COMBINING BRIDGE BELOW]",
	"apical" 			=> "\c[COMBINING INVERTED BRIDGE BELOW]",
	"linguolabial" 		=> "\c[COMBINING SEAGULL BELOW]",
	"laminal" 			=> "\c[COMBINING SQUARE BELOW]",
	"advanced" 			=> "\c[COMBINING PLUS SIGN BELOW]",
	"retracted" 		=> "\c[COMBINING MINUS SIGN BELOW]",
	"centralized" 		=> "\c[COMBINING DIAERESIS]",
	"mid-centralized"	=> "\c[COMBINING X ABOVE]",
	"raised" 			=> "\c[COMBINING UP TACK BELOW]",
	"lowered" 			=> "\c[COMBINING DOWN TACK BELOW]",
	"rounded" 			=> "\c[COMBINING RIGHT HALF RING BELOW]",
	"unrounded" 		=> "\c[COMBINING LEFT HALF RING BELOW]",
	"aspirated" 		=> "\c[MODIFIER LETTER SMALL H]",
	"labial" 			=> "\c[MODIFIER LETTER SMALL W]",
	"palatal" 			=> "\c[MODIFIER LETTER SMALL J]",
	"velar" 			=> "\c[MODIFIER LETTER SMALL GAMMA]",
	"pharyngeal" 		=> "\c[MODIFIER LETTER SMALL REVERSED GLOTTAL STOP]",
	"front" 			=> "\c[COMBINING LEFT TACK BELOW]",
	"back" 				=> "\c[COMBINING RIGHT TACK BELOW]",
	"rhotic" 			=> "\c[MODIFIER LETTER RHOTIC HOOK]",
	"long" 				=> "\c[MODIFIER LETTER TRIANGULAR COLON]",
	"short" 			=> "\c[MODIFIER LETTER HALF TRIANGULAR COLON]",
	"extra-short" 		=> "\c[COMBINING BREVE]",
	;

my %diacritics = 
	"\c[COMBINING TILDE]"                             =>  "nasal" 			,
	"\c[COMBINING RING BELOW]"                        =>  "voiceless" 		,
	"\c[COMBINING CARON BELOW]"                       =>  "voiced" 			,
	"\c[COMBINING DIAERESIS BELOW]"                   =>  "breathy" 			,
	"\c[COMBINING TILDE BELOW]"                       =>  "creaky" 			,
	"\c[COMBINING PLUS SIGN BELOW]"                   =>  "advanced" 			,
	"\c[COMBINING MINUS SIGN BELOW]"                  =>  "retracted" 		,
	"\c[COMBINING DIAERESIS]"                         =>  "centralized" 		,
	"\c[COMBINING X ABOVE]"                           =>  "mid-centralized"	,
	"\c[COMBINING UP TACK BELOW]"                     =>  "raised" 			,
	"\c[COMBINING DOWN TACK BELOW]"                   =>  "lowered" 			,
	"\c[COMBINING RIGHT HALF RING BELOW]"             =>  "rounded" 			,
	"\c[COMBINING LEFT HALF RING BELOW]"              =>  "unrounded" 		,
	"\c[MODIFIER LETTER SMALL H]"                     =>  "aspirated" 		,
	"\c[COMBINING LEFT TACK BELOW]"                   =>  "front" 			,
	"\c[COMBINING RIGHT TACK BELOW]"                  =>  "back" 				,
	"\c[MODIFIER LETTER TRIANGULAR COLON]"            =>  "long" 				,
	"\c[MODIFIER LETTER HALF TRIANGULAR COLON]"       =>  "short" 			,
	"\c[COMBINING BREVE]"                             =>  "extra-short" 		,
	"\c[COMBINING BRIDGE BELOW]"                      =>  "dental" 			,
	"\c[COMBINING INVERTED BRIDGE BELOW]"             =>  "apical" 			,
	"\c[COMBINING SEAGULL BELOW]"                     =>  "linguolabial" 		,
	"\c[COMBINING SQUARE BELOW]"                      =>  "laminal" 			,
	"\c[MODIFIER LETTER SMALL W]"                     =>  "labial" 			,
	"\c[MODIFIER LETTER SMALL J]"                     =>  "palatal" 			,
	"\c[MODIFIER LETTER SMALL GAMMA]"                 =>  "velar" 			,
	"\c[MODIFIER LETTER SMALL REVERSED GLOTTAL STOP]" =>  "pharyngeal" 		,
	"\c[MODIFIER LETTER RHOTIC HOOK]"                 =>  "rhotic" 			,
	"\c[COMBINING DOUBLE INVERTED BREVE]"             =>  "affricate",
	;

our %superscripts is export = 
	"rhoticized" 			=> "\c[MODIFIER LETTER RHOTIC HOOK]",
	"long" 				=> "\c[MODIFIER LETTER TRIANGULAR COLON]",
	"short" 			=> "\c[MODIFIER LETTER HALF TRIANGULAR COLON]",
	"aspirated" 		=> "\c[MODIFIER LETTER SMALL H]",
	"labialized" 			=> "\c[MODIFIER LETTER SMALL W]",
	"palatalized" 			=> "\c[MODIFIER LETTER SMALL J]",
	"velarized" 			=> "\c[MODIFIER LETTER SMALL GAMMA]",
	"pharyngealized" 		=> "\c[MODIFIER LETTER SMALL REVERSED GLOTTAL STOP]",
	;
our %modifiers is export = 
	"\c[MODIFIER LETTER RHOTIC HOOK]"                =>  "rhoticized" 			,
	"\c[MODIFIER LETTER TRIANGULAR COLON]"           =>  "long" 			,
	"\c[MODIFIER LETTER HALF TRIANGULAR COLON]"      =>  "short" 			,
	"\c[MODIFIER LETTER SMALL H]"                    =>  "aspirated" 		,
	"\c[MODIFIER LETTER SMALL W]"                    =>  "labialized" 			,
	"\c[MODIFIER LETTER SMALL J]"                    =>  "palatalized" 			,
	"\c[MODIFIER LETTER SMALL GAMMA]"                =>  "velarized" 			,
	"\c[MODIFIER LETTER SMALL REVERSED GLOTTAL STOP]"=>  "pharyngealized" 		,
	;

my %opposites =
	'voiced'           => 'voiceless',
	'voiceless'           => 'voiced',
	'rounded'          => 'unrounded',
	'unrounded'          => 'rounded',
	'front' 			=> 'back',
	'back' 			=> 'front',
	'open' 			=> 'close',
	'close' 			=> 'open',
	'open-mid' 			=> 'close-mid',
	'close-mid' 			=> 'open-mid',
	'open-full' 			=> 'close-full',
	'close-full' 			=> 'open-full',
	;


my @vowel-chart = [
	%aspects{"close"},
	%aspects{"near-close"},
	%aspects{"close-mid"},
	%aspects{"mid"},
	%aspects{"open-mid"},
	%aspects{"near-open"},
	%aspects{"open"}
];

# finish large groupings
%aspects{"close"}     = (%aspects{"close"} ∪ %aspects{"near-close"}).keys.cache;
%aspects{"mid"}       = (%aspects{"mid"}   ∪ %aspects{"close-mid"} ∪ %aspects{"open-mid"}).keys.cache;
%aspects{"open"}      = (%aspects{"open"}  ∪ %aspects{"near-open"}).keys.cache;
%aspects{"front"}     = (%aspects{"front"} ∪ %aspects{"near-front"}).keys.cache;
%aspects{"back"}      = (%aspects{"back"}  ∪ %aspects{"near-back"}).keys.cache;

%aspects{"labial"}    = (%aspects{"labial"} ∪ %aspects{"bilabial"} ∪ %aspects{"labiodental"} ∪ %aspects{"linguolabial"}).keys.cache;
%aspects{"coronal"}   = (%aspects{"linguolabial"} ∪ %aspects{"dental"} ∪ %aspects{"alveolar"} ∪ %aspects{"postalveolar"} ∪ %aspects{"retroflex"} ∪ %aspects{"palatal"}).keys.cache;
%aspects{"dorsal"}    = (%aspects{"palatal"} ∪ %aspects{"velar"} ∪ %aspects{"uvular"}).keys.cache;
%aspects{"laryngeal"} = (%aspects{"pharyngeal"} ∪ %aspects{"glottal"}).keys.cache;
%aspects{"obstruent"} = (%aspects{"plosive"} ∪ %aspects{"fricative"} ∪ %aspects{"affricate"}).keys.cache;
%aspects{"sonorant"} = (([∪] %aspects.values) (-) %aspects{"obstruent"}).keys.cache;
%aspects{"continuant"} = (%aspects{"fricative"} ∪ %aspects{"approximant"} ∪ (|$_ for @vowel-chart)).keys.cache;
#dds %aspects{'continuant'};

## add aliases
#%aspects{"high"}      := %aspects{"close"};
#%aspects{"low"}       := %aspects{"open"};
#%aspects{"high-mid"}  := %aspects{"close-mid"};
#%aspects{"low-mid"}   := %aspects{"open-mid"};
#%aspects{"near-high"} := %aspects{"near-close"};
#%aspects{"near-low"}  := %aspects{"near-open"};
#%aspects{"flap"}      := %aspects{"tap"};
#%aspects{"stop"}      := %aspects{"plosive"};
#%aspects{"occlusive"} := %aspects{"plosive"};
#%aspects{"epiglottal"}:= %aspects{"pharyngeal"};

my %exclusive = 
	#<rounded unrounded>,  # roundedness
	'backness' => <front central back near-front near-back>,  # backness
	'height' => <close close-full close-mid near-close mid open open-mid near-open open-full high low high-mid low-mid near-high near-low>, # height
	
	'voicedness' => <voiced voiceless rounded unrounded>, # voicedness
	'place' => <labial labialized coronal dorsal laryngeal bilabial linguolabial dental alveolar alveolarized postalveolar retroflex palatal palatalized velar velarized uvular pharyngeal pharyngealized glottal glottalized>, # place
	'manner' => <nasal plosive sibilant obstruent continuant sonorant coronal fricative approximant tap trill lateral>, # manner
	'other' => ['nasalized',],
	'affricate' => ["affricate", ], 
	'aspirated' => ['aspirated'], 
;


my (%letters, @letters);
@letters = ([∪] %aspects.values).keys;
my @vowels;
@vowels.push: |$_ for @vowel-chart;
my @consonants = (@letters (-) @vowels).keys;
%aspects{'voiced'}.push: |@vowels;
sub vowels is export  { @vowels };
sub consonants is export { @consonants };
#dds @letters;

for @letters -> $letter {
	%letters{$letter} = [];
	for %aspects.kv -> $k, $v {
		%letters{$letter}.push: $k if $v ∋ $letter;
	}
}
for %modifiers.kv -> $k,$v {
	%letters{$k} = $v;
}
#dds %letters{'dz'};

#cache aspects
my %get-aspects;
#|[ find all aspects that are a letter is part of ]
multi sub get-aspects($letter --> Array) is export {
	return %get-aspects{$letter} if %get-aspects{$letter}:exists;

	my @aspects = $letter.NFD».chr.map({ |(%letters{$_} || %diacritics{$_}) });
	# clear null values from @aspects
	@aspects = @aspects.grep: {$_ if not Any};

	# im an affricate if without diacritics im more that a char long
	@aspects.push: "affricate" if diacritics($letter, :negative) and diacritics($letter, :negative).chars > 1;
	# if one of the aspects is lets say 'nasalized', it should be counted as nasal too
	((@aspects.push($_.substr(0, *-4)) if $_.ends-with('ized')) for @aspects) if @vowels ∋ diacritics($letter,:negative);
	#dds @aspects;
	@aspects.push: |$letter.map({ |%letters{$_} });

	# diacritics should override all exclusive aspects that a letter has
	# ex: ḁ has a voiceless diacritic so that should delete the 'voiced' aspect of @aspects
	if diacritics($letter) {
		my $diacritic-aspects = diacritics($letter).NFD».chr.map({ %diacritics{$_} });
		#dds $diacritic-aspects;
		for %opposites.keys -> $exclusive {
			if $diacritic-aspects.cache ∋ $exclusive {
				#dds $exclusive;
				Utils.cut(@aspects, %opposites{$exclusive});
				#@aspects = ((@aspects (-) [$exclusive]) ∪ $diacritic-aspects).keys.Array;
			}
		}
	}

	# clear null values from @aspects
	@aspects = @aspects.grep: {$_ if not Any};
	%get-aspects{$letter} = @aspects.unique.Array;
	return @aspects.unique.Array;
}
multi sub get-aspects(@letter --> Array) is export {
	my @aspects = @letter.map({|%letters{$_} || |%diacritics{$_}}).unique;
	# remove unwanted '(Any)'s from the array
	@aspects = @aspects.grep: {$_ if not Any};
	return @aspects;
}

##|[ find all letters that fit a given aspect ]
#multi sub get-letters($aspect) {
	#return consonants if $aspect eq 'consonants';
	#return vowels if $aspect eq 'vowels';
	#my @letters;
	#for %aspects.kv -> $name,$ltrs {
		##says @ltrs;
		#@letters.push(|$ltrs) if $aspect eq $name;
	#}

	#for %vowel-diacritics.keys -> $diacritic {
		#@letters.push: %vowel-diacritics{$diacritic} if $aspect eq $diacritic;
	#}
	#for %consonant-diacritics.keys -> $diacritic {
		#@letters.push: %consonant-diacritics{$diacritic} if $aspect eq $diacritic;
	#}

	##%get-letters{$aspect} = $[|@letters.unique];
	#return @letters.unique;
#}
#says get-letters("nasal");

my %get-letters;
#|[ figure out which letters in a str includes a given aspect (including diacritics) ]
multi sub get-letters(Str $str, Str $aspect, :$negative?) is export {
	return %get-letters{$str}{$aspect}{'negative'} if %get-letters{$str}{$aspect}{'negative'}:exists;
	return %get-letters{$str}{$aspect} if %get-letters{$str}{$aspect}:exists;

	sub get-letters-in-str-by-aspect(Str $str, Str $aspect --> Seq) {
		my $split = split($str);
		#dds $split;
		my $letters = $split.map({[$_, get-aspects($_)]}).map({$_[0] if $_[1] ∋ $aspect});

		$letters = $split.map({$_ if diacritics($_, :negative) ∈ @vowels}) if $aspect eq 'vowels';
		$letters = $split.map({$_ if diacritics($_, :negative) ∈ @consonants}) if $aspect eq 'consonants';

		#dds $letters;
		return $letters;
	}

	my @ltrs;
	if $negative {
		@ltrs = |(split($str) (-) get-letters-in-str-by-aspect($str, $aspect)).keys;
	} else {
		#dds $aspect;
		@ltrs = |get-letters-in-str-by-aspect($str, $aspect);
	}
	#dds @ltrs;
	#my @matched = $str.split("",:skip-empty).map: {any(.NFD.map({.chr ∈ @ltrs})) if any(.NFD.map({%diacritics{.chr} ne $aspect}))};
	#my @returns;
	#my ($last-letter, $last-match) = ('','');
	#my @zip = $str.split("",:skip-empty);
	##dds @zip;
	#for @zip.kv -> $i, $ltr {
		#my $letter = %modifiers{$ltr} ?? $last-letter ~ $ltr !! $ltr;
		#my $match= any($ltr.NFD.map({%diacritics{.chr}||'' ne $aspect ?? .chr ∈ @ltrs!!False}));
		##$match = !$match if $negative;
		#my $return = $letter;
		#if $i < @zip.elems-1 and %modifiers{@zip[$i+1]}:exists {
			#$return ~= @zip[$i+1];
		#}
		##says ($letter, $match, $last-letter, $last-match);
		#@returns.push: $return if $match;#($return if $return.chars > 1 and $last-match);
		#($last-letter, $last-match) = ($letter, $match);
	#}
	#dds @returns;
	my @returns = @ltrs;
	$negative ?? (%get-letters{$str}{$aspect}{'negative'} = @returns) !! %get-letters{$str}{$aspect} = @returns;
	return %get-letters{$str}{$aspect}{'negative'} || %get-letters{$str}{$aspect};
}

#says get-letters('nãtʰaŋ', 'plosive');

#|[ detect whether 2 letters have the same aspects in common ]
sub overlaps(@aspects, Str $lhs, Str $rhs, --> Bool) is export {
#	dds @aspects, $lhs, $rhs;
#	says get-aspects($lhs);
#	says get-aspects($rhs);
	#dds @aspects.map({$_ ∈ get-aspects($lhs) and $_ ∈ get-aspects($rhs)}).any;
#	says @aspects.map({$_ ∈ get-aspects($rhs)}).all;
	#return @aspects.map({$_ ∈ get-aspects($lhs) and $_ ∈ get-aspects($rhs)}).any ?? True !! False;
}

my %get-diacritics-positive;
my %get-diacritics-negative;
#|[ return the diacritics of a letter.
    with :negative, it returns the letter without any of the diacritics attached ]
sub diacritics(Str $letter, :$negative --> Str) {
	if $negative {
		return %get-diacritics-negative{$letter} if %get-diacritics-negative{$letter}:exists;
	} else {
		return %get-diacritics-positive{$letter} if %get-diacritics-positive{$letter}:exists;
	}
	my @letters = $letter.NFD».chr;
	#dds @letters;
	my Str $result;
	for @letters {
		if $negative {
#			dds $_;
			$result ~= $_ if %diacritics{$_}:!exists;
		} else {
			$result ~= $_ if %diacritics{$_}:exists;
		}
	}
	#dds $result;
	if $negative {
		%get-diacritics-negative{$letter} = $result;
	} else {
		%get-diacritics-positive{$letter} = $result;
	}
	return $result;
}

# TODO: make t͡sʰ be considered as a single letter
#|[ split a word into its component letters (where modifiers (ʰ etc) count as part of the letter (as a diacritic)) ]
sub split(Str $str --> Array) is export {
	my @returns;
	my @zip = $str.comb;
	#dds @zip;
	while @zip {
		my $return = @zip.shift;

		# inner loop that looks ahead to see if following letters 
		# are part of the same letter (t͡sʰ should be one letter)
		loop (my $i = 0; $i < @zip.elems; ++$i) {
			my Str $next-letter = @zip[$i];
			if %modifiers{$next-letter}:exists or diacritics($return) ∋ %consonant-diacritics{'affricate'} {
				$return ~= @zip.shift;
			} else {
				last;
			}
		}
		@returns.push: $return;#($return if $return.chars > 1 and $last-match);
	}
	#dds @returns;
	return @returns.unique.Array;
}

#says switch-aspect("ɛ", "close"); # returns "i" cuz theyre both front unrounded
my %switch-aspect;
#|[ switch the aspect of a letter from one to another ]
multi sub switch-aspect($letter, $to-aspect) is export {
	my $ltr = $letter.Array; # .Array so that the underlying Seq doesnt get used up
	my @to-aspect = $to-aspect.Array;
	return %switch-aspect{$ltr.Str}{@to-aspect.Str} if %switch-aspect{$ltr.Str}{@to-aspect.Str}:exists;
	my $last-aspect = '';
	for @to-aspect -> $aspect {
		($last-aspect = '-'; next) if $aspect eq '-';
		dds $aspect;
		for $ltr {
#			dds $ltr;
			if $last-aspect eq '-' {
				says 'negative';
				$ltr = change-aspect($_, $aspect, :negative);
			} else {
				$ltr = change-aspect($_, $aspect);
			}
			dds $ltr;
			$ltr = $ltr[0] if $ltr ~~ Array;
#			dds $ltr;
		}
		# reset $last-aspect
		$last-aspect = '' if $aspect ne '-';
#		dds $ltr;
	}
	if $ltr {
		%switch-aspect{$ltr.Str}{@to-aspect.Str} = $ltr;
		#dds %switch-aspect;
		return %switch-aspect{$ltr.Str}{@to-aspect.Str};
	}
}

##|[ remove an aspect from a letter ]
#multi sub change-aspect($letter, $to, :$negative!) {
	#my $to-aspect = $to.Array;
	#if $to-aspect ∋ 'affricate' {
		##say Utils.cut($letter.NFD».chr.Array, %consonant-diacritics{'affricate'}).join;
		#return Utils.cut($letter.NFD».chr.Array, %consonant-diacritics{'affricate'}).join;
	#}
	#my $my-aspects = get-aspects($letter);
	#my $opposite = %opposites{$to} if %opposites{$to};
	#my $new-aspects = ($my-aspects (-) $to-aspect).keys.Array;
	#$new-aspects = ($new-aspects ∪ $opposite).keys.Array if $opposite;
	#my $new-letter = get-letter-by-aspects($new-aspects);
	#return $new-letter;
#}

#|[ change an aspect of a letter. conform forces a letter to keep 1(+) specific aspects ]
multi sub change-aspect($the-letter, $to, @conform=[], :$negative) {
	my $to-aspect = $to.Array;
	my $letter = $the-letter.Str;
	#my @to-aspect = $to-aspect.Array;
	#dds $to-aspect;
	#my $letter = switch-aspect($ltr, $to-aspect[0..*-2]) if $to-aspect.elems > 1;
	#dds $letter;
	# save the diacritics to rejoin later
	my $diacritics = diacritics($letter);
	my $no-diacritics = diacritics($letter, :negative);

	if $negative and $diacritics {
		# compare the diacritics to the $to-aspect and remove the diacritic if needed
		# (詰り, prioritize the changing of diacritics)
		my Str $diacs = $letter.Str.NFD».chr.map({
			says $_;
			my $diacritic = %diacritics{$_} || ' ';
			dds $diacritic;
			$_ if not $to-aspect.starts-with($diacritic);
		}).join||'';
		dds $diacs;
		dds ?$diacs;
		return $diacs;
	}

	if $negative and $to-aspect ∋ 'affricate' {
		#says Utils.cut($letter.NFD».chr.Array, %consonant-diacritics{'affricate'}).join;
		return Utils.cut($letter.NFD».chr.Array, %consonant-diacritics{'affricate'}).join;
	}
	#dds $letter.NFD[0].chr;
	my $my-aspects = get-aspects($letter); #?? %letters{$letter} !! %consonants{$letter};
	# if im an affricate, process each letter in the affricate separately

	my $split = split(Utils.cut($letter.NFD».chr.Array, %consonant-diacritics{'affricate'}).join);
	if $my-aspects ∋ 'affricate' or $split.elems > 1 {
		# save the diacritics of each letter in the affricate
		my $diacritics1 = diacritics($split[0]);
		my $diacritics2 = diacritics($split[1]);
		my Str $new-letters;
		if $negative {
			$new-letters ~= change-aspect($split[0], $to, ['plosive'], :negative);
			$new-letters ~= %consonant-diacritics{'affricate'};
			$new-letters ~= change-aspect($split[1], $to, ['fricative'], :negative);
		} else {
			#dds $split, $to;
			#dds $to-aspect, %superscripts.keys;
			# dont add modifiers to the first letter in the affricate (tʰ → [affricate] = t͡sʰ, not tʰ͡sʰ)
			my Str $l1;
			if $to-aspect ⊆ %superscripts.keys {
				$l1 = $split[0];
			} else {
				$l1 = change-aspect($split[0], $to, ['plosive']).Str;
			}
			$l1 ~= $diacritics1 if $diacritics1;
			$l1 ~= %consonant-diacritics{'affricate'};
			$l1 = $l1.NFD».chr.unique.join;
			my Str $l2 = change-aspect($split[1], $to, ['fricative']).Str;
			$l2 ~= $diacritics2 if $diacritics2;
			$l2 = $l2.NFD».chr.unique.join;
			#dds $diacritics;
			#$new-letters ~= Utils.cut($diacritics.NFD».chr.Array, %consonant-diacritics{'affricate'}).join;
			$new-letters = $l1 ~ $l2;
		}
#		says 'split: ',  $split;
		#dds $new-letters.NFD».chr;
		return $new-letters;
	}

	#says 'no split';
	#dds $letter;
	#dds $my-aspects;
	return $letter if $my-aspects ∋ $to-aspect;
	return Nil if !$my-aspects;
	for %exclusive.values -> @ex {
		says $my-aspects, @ex, " ", $to-aspect;
		# if $to-aspect is in @ex
		if @ex ∋ $to-aspect.any {
#		says 'in here';
			if $to-aspect ∋ "close" and $my-aspects ∋ "near-open" {
				Utils.cut($to-aspect, "close");
				$to-aspect.push: "near-close";
			} elsif $to-aspect ∋ "close" and $my-aspects ∋ "open-mid" {
				Utils.cut($to-aspect, "close");
				$to-aspect.push: "close-mid";
			} elsif $to-aspect ∋ "open" and $my-aspects ∋ "near-close" {
				Utils.cut($to-aspect, "open");
				$to-aspect.push: "near-open", "near";
			} elsif $to-aspect ∋ "open" and $my-aspects ∋ "close-mid" {
				Utils.cut($to-aspect, "open");
				$to-aspect.push("open-mid", "mid");
			} elsif $to-aspect ∋ "affricate" {
				#says 'affricate';
				#$to-aspect.push("fricative") if $my-aspects ∋ "plosive";
				#$to-aspect.push("plosive") if $my-aspects ∋ "fricative";
			#}
			#if $to-aspect ∋ 'affricate' {
				my $new-letter;
				if $my-aspects ∋ "plosive" {
					#says 'in here';
					#@new-letters = switch-aspect($letter, "fricative").map({
						#$letter ~ %consonant-diacritics{'affricate'} ~ $_ 
					#});
					
					#the plosive letter in affricates dont get modifiers
					$new-letter  = $letter.split('',:skip-empty).grep({$_ ∉ %modifiers.keys}).join;
					$new-letter ~= %consonant-diacritics{'affricate'};
					$new-letter ~= change-aspect($letter, 'fricative');
				} elsif $my-aspects ∋ "fricative" {
					$new-letter = switch-aspect($letter, "plosive").map({
						$_ ~ %consonant-diacritics{'affricate'} ~ $letter
					});
				#} else {
					#error("must be fricative or plosive to become an affricate")
				}
				return $new-letter;
			}
			# return the letter that addition of the set-intersection of the aspects is
			#says $my-aspects, @ex, $to-aspect;
			my $ex = ($my-aspects ∩ @ex).keys.grep({$_ ∈ %exclusive{'manner'}});
			# i need to calculate new-aspects more than once, so i put it in a function
			sub new-aspects($aspects) {
				my $new-aspects;
				if $negative {
#				says 'negative choice negative';
					my Str $opposite = %opposites{$to-aspect} || ''; #{
						#($to-aspect.ends-with('ized') || %diacritics.values.grep({False if .starts-with($to-aspect)})) ?? $to-aspect !! ''
					#);
					#}()[0];
#				dds $opposite;
					$new-aspects = (($aspects (-) @ex) ∪ $opposite).keys.Array;
#				dds $new-aspects;
				} else {
					$new-aspects = (($aspects (-) @ex) ∪ $to-aspect).keys.Array;
				}
				return $new-aspects;
			}
			my $new-aspects = new-aspects($my-aspects);
			# if i return a letter (and not an array of aspects) that means that 
			dds $my-aspects, $to-aspect, $ex, ($my-aspects (-) @ex).keys, $new-aspects;
			#dds (($my-aspects (-) @ex) ∪ $to-aspect).keys;
			#dds $new-aspects;

			my $new-letter = get-letter-by-aspects($new-aspects, @conform, original-letter=>$letter)[0] || $letter;
			dds $new-letter, @conform;

			# sibilant is a pain in the ass
			if $new-letter eq $letter and $new-aspects ∋ 'sibilant' and $no-diacritics ∉ @vowels {
				$new-letter = get-letter-by-aspects(($new-aspects (-) 'sibilant').keys.Array, @conform, original-letter=>$letter)[0] || $letter;
			}
			# if with diacritics the search for a letter was unsuccessful, 
			# then try searching without diacritics, and then add back on the diacritics
			if $new-letter eq $letter {
#				dds $no-diacritics;
				$new-letter = get-letter-by-aspects(((new-aspects(get-aspects($no-diacritics)) (-) @ex) ∪ $to-aspect).keys.Array, @conform, original-letter=>$no-diacritics)[0] || $no-diacritics;
#				dds $new-letter;
				$new-letter ~= $diacritics if $diacritics;
			}
			# sibilant is a pain in the ass
			if $new-letter eq $letter and $new-aspects ∋ 'sibilant' {
				$new-letter = get-letter-by-aspects(((new-aspects(get-aspects($no-diacritics)) (-) @ex (-) 'sibilant') ∪ $to-aspect).keys.Array, @conform, original-letter=>$no-diacritics)[0] || $no-diacritics;
#				dds $new-letter;
				$new-letter ~= $diacritics if $diacritics;
			}

			# keep modifier diacritics that dont go away manually
			my $modifier-diacritics = %superscripts.keys.grep({$_ ∈ $new-aspects});
			$new-aspects = (get-aspects($new-letter) ∪ $modifier-diacritics).keys.unique.Array;
			#dds $new-aspects;

#			says 'no diacritics: ',  $no-diacritics;
			if $no-diacritics ∈ @vowels {
				for %vowel-diacritics.kv -> $name, $diacritic {
					says $name;
					#$new-letter ~= $diacritic if ($to-aspect ∋ $name and $names??$names∌$diacritic!!True) or $ex.cache ∋ $name and ($negative?? ($name ∉ $to-aspect and $name~'ized' ∉ $to-aspect) !!True);
					$new-letter ~= $diacritic if (($to-aspect ∋ $name or $ex.cache ∋ $name) and $new-aspects ∌ $name) and ($negative?? ($name ∉ $to-aspect) !!True);
				}
			} elsif $no-diacritics ∈ @consonants {
#				says 'im a consonant';
				for %consonant-diacritics.kv -> $name, $diacritic {
					#dds $name, $name~'ized', $to-aspect if $name eq 'nasal', $ex, $new-letter, $diacritic;
					#dds ($name ∉ $to-aspect and $name~'ized' ∉ $to-aspect) if $name eq 'nasal';
					$new-letter ~= $diacritic if (($to-aspect ∋ $name or $ex.cache ∋ $name) and $new-aspects ∌ $name) and ($negative?? ($name ∉ $to-aspect and $name~'ized' ∉ $to-aspect) !!True);
				}
				# add modifiers that we missed
				for %modifiers.kv -> $diacritic, $name {
					$new-letter ~= $diacritic if ($new-aspects ∋ $name);
				}
			}
			# remove diacritics if $negative
			my $newer-letter = '';
			if $negative and $new-letter {
				#dds $new-letter, $new-letter.NFD».chr, $new-letter».NFD, $new-letter».NFD.chr, $new-letter.NFD».chr;
				for $new-letter.NFD».chr -> $ltr {
#					dds $ltr;
					#dds %diacritics{$ltr}, %diacritics{$ltr~'ized',(%diacritics{$ltr}~'ized' ∉ $to-aspect);
#					dds %diacritics{$ltr} ?? (%diacritics{$ltr} ∉ $to-aspect and %diacritics{$ltr}~'ized' ∉ $to-aspect) !! True;
					$newer-letter ~= $ltr if %diacritics{$ltr} ?? (%diacritics{$ltr} ∉ $to-aspect and %diacritics{$ltr}~'ized' ∉ $to-aspect) !! True;
				};
#				dds $newer-letter;
				$new-letter = $newer-letter;
			}
			# work with -ized (velarized, nasalized) features
			for $to-aspect {
				if .ends-with('ized') and !$negative {
					$new-letter ~= %aspects{$_}[0]
				}
			}
#			dds $diacritics, $new-letter;
			#$new-letter = |$new-letter.map({$_ ~ $diacritics.join}) if $diacritics;
			$new-letter = |$new-letter.map({.NFD».chr.unique.join});
#			dds $new-letter;

			return $new-letter;
		}
	}
}

multi sub switch-aspect($letter, $to-aspect, $dest) is export {
	#says 'other switch aspect!!!!!';
	my $ltr = $letter;
	#dds $to-aspect;
	#says $to-aspect;
	if $to-aspect {
		for $to-aspect -> $aspect {
			#says $aspect[0];
			($ltr ~= $_ if %modifiers{$_}) for $aspect[0].split('', :skip-empty);
		}
	# when u use a ∅ on the rhs it should return nothing
	} else {
		$ltr = '';
	}
	return $ltr;
}

#|[ get all superscript letters from a String $set ]
sub superscript(@aspects, $set) {
	my $return = $set;
	for @aspects -> $aspect {
		if %superscripts{$aspect}:exists and $aspect ∉ get-aspects($set) {
			$return ~= %superscripts{$aspect};
		}
	}
	#get-aspects($set~$return) ≡ @aspects;
	return $return
}

#|[ given a list of aspects, return a list of letters which fit the bill 
#| conform is to make sure that the letter has a specific aspect
#| original-letter is the letter from change-aspect that is getting changed
#|]
sub get-letter-by-aspects(@aspects where Array, @conform=[], :$original-letter='') is export {
	sub get-letters-for-aspects(@aspects) {
		my @sets;
		dds @aspects;
		for @aspects {
			my $aspect = %aspects{($_ ~~ Pair ?? $_.key !! $_)} if %modifiers{$_}:!exists;
			@sets.push: $aspect if $aspect;
		}
		#dds @sets;
		return @sets;
	}
	#@aspects = @aspects.grep: {$_ if not Any};
	my @sets = |([∩] get-letters-for-aspects(@aspects)).keys;
	dds @sets;
	if !@sets {
		for |%exclusive{'voicedness'} {
#			says $_;
			my @temp-aspects = (@aspects (-) %exclusive{'voicedness'}).keys;
#			says @temp-aspects, (get-letters-for-aspects(@temp-aspects) ∪ [%aspects{$_}]).keys;
			@sets = |([∩] (get-letters-for-aspects(@temp-aspects) ∪ [%aspects{$_}]).keys).keys.sort.Array;
			dds @sets;
			# add the diacritic we just 
			@sets = @sets.map(-> $set {$set~%consonant-diacritics{%opposites{$_}}});
			last if @sets.elems > 0;
		}
	}
	says "top", @aspects, @sets;

	# if the aspects given are an exact match to the aspects of a letter, 
	# then return the letter right away
	if @sets.elems > 1 {
		for @sets -> $set {
			#says $set;
			my $return =  superscript(@aspects, $set);
			return $return if get-aspects($return) ≡ @aspects;
			#if the last line fails Utils.cut this next line
			#return $_;
		}
	}
	if @sets ~~ Array {
		#dds @sets.elems;
#		dds @aspects;
		if @aspects ∌ "affricate" {
			my @temp;
			for @sets.kv -> $k, $_ {
#				says $k, "$_", get-aspects($_);
				#Utils.cut(@sets, $_) if get-aspects($_) ∋ "affricate";
				# ⊃ is the superset operator, and ⊂ is the subset operator
				my $push = @conform ?? get-aspects($_) ⊇ @conform !! True;
#				dds $push;
				# special rules for laterals
				my $lateral = @aspects ∋ 'lateral' ?? (get-aspects($original-letter) ∋ 'plosive' ?? get-aspects($_) ∋ 'approximant' !! True) !! True;
				@temp.push: $_ if get-aspects($_) ∌ "affricate" and $push and $lateral;
			}
			@sets = @temp;
			dds @sets;
		}

		# smart match remaining choices to figure out what the real match is
		# the letter in @sets with the shortest amount of aspects wins
		if @sets.elems > 1 and diacritics(@sets[0], :negative) ∈ @consonants {
			my @shortest-sets;
			my $shortest = Inf;
			dds @sets.sort.sort({get-aspects($_).elems});
			for @sets.sort.sort({get-aspects($_).elems}) -> $set {
				my $aspects = get-aspects($set).elems;
				if $aspects <= $shortest {
					@shortest-sets.push: $set;
					$shortest = $aspects;
				}
				says $aspects;
			}
			@sets = @shortest-sets;
			dds @sets;
		}

		# prioritize sibilants cuz mendy said so (bad idea)
		for @sets -> $set {
			return $set if get-aspects($set) ∋ 'sibilant';
		}
		# sort is for tests, so that i can see if its indeed the same list of letters
		return @sets.sort({.NFD[0].chr});
	} else {
		return @sets[0];
	}
	die "{@aspects} is not a real letter";
}

sub move($letter, $dir where "close" | "open" | "front" | "back") is export {
	##return "∅" if $dir ne "up" | "down" | "right" | "left";

	my ($x, $y, $max-x, $max-y, $v-elems);
	$max-y = @vowel-chart.elems-1;
	for @vowel-chart.kv -> $k,$v {
		if $v ∋ $letter {
			$y = $k;
			$x = $v.first($letter, :k);
		}
	}

	$y += 1 if $dir eq "open";
	$y -= 1 if $dir eq "close";
	$y = Utils.clamp($y, 0, $max-y);
	$max-x = @vowel-chart[$y].elems-1;
	$x += 2 if $dir eq "back";
	$x -= 2 if $dir eq "front";
	$x = Utils.clamp($x, 0, $max-x);

	#dds @vowel-chart, $x, $y, $dir;

	return @vowel-chart[$y][$x];
}

#say "###########################";
#my @tests4 = [%letters{"ɬ"}.Array, <voiced velar plosive>.Array, <velar plosive>.Array, <labial velar approximant>.Array, <velar approximant>.Array, <retroflex nasal>.Array];
#my @results4 = ["ɬ", "g", <k g>, "w", <ɰ w ʍ ʟ>, <ɳ ɳ̊>];
#for @tests4.kv -> $i,$v {
	##say "";
	#my $value = get-letter-by-aspects($v);
	#say ($value eq @results4[$i].sort) ?? colored("OK","black on_green")~" $v → $value" !! colored("NOT OK","white on_red")~" $v ≠ $value = {@results4[$i]}";
#}
#say "###########################";

#my @tests3 = [<y close>, <o open>, <ɜ back>, <a back>, <ʊ front>, <a front>, <ɘ close>, <ɐ back>, <ə open>];
#my @results3 = <y o̞ ʌ ä ʉ̞ a ɨ̞ ɒ̝ ɜ>;
#for @tests3.kv -> $i,$v {
	##say "";
	#my $value = move($v[0], $v[1]);
	#say ($value eq @results3[$i]) ?? colored("OK","black on_green")~" {$v[0]} → {$v[1]} = $value" !! colored("NOT OK","white on_red")~" {$v[0]} → {$v[1]} ≠ $value";
#}
#say "###########################";

#my @tests2 = [<y near-back>, <u front>, <o open>, <a back>, <ɛ central>, <a near-close>, <u unrounded>, <u close-mid>, <ʊ unrounded>, <ä rounded>, <ɐ rounded>];
#my @results2 = <ʊ y ɔ ɑ ɜ ɪ ɯ o ʊ̜ ɶ̈ ɐ>;
#for @tests2.kv -> $i,$v {
	##say "";
	#my $value = switch-aspect($v[0], $v[1]);
	#say ($value eq @results2[$i]) ?? colored("OK","black on_green")~" {$v[0]} → {$v[1]} = $value" !! colored("NOT OK","white on_red")~" {$v[0]} → {$v[1]} ≠ $value = @results2[$i]";
#}
#say "###########################";

#my @tests1 = [%letters{"o"}.Array, <rounded front close>.Array, <unrounded central near-open open>.Array, <unrounded front close-mid>.Array];
#my @results1 = ["o", ["y", "ʏ"], "ɐ", "e"];
#for @tests1.kv -> $i,$v {
	##say "";
	#my $value = get-letter-by-aspects($v);
	#say ($value eq @results1[$i]) ?? colored("OK","black on_green")~" $v → $value" !! colored("NOT OK","white on_red")~" $v ≠ $value = {@results1[$i]}";
#}

#say "###########################";
#my @tests5 = [<k glottal>, <p voiced>, <b fricative>, <k fricative>, <ʃ voiced>, <t affricate>, ['t', ['affricate', 'lateral']], ['t', ['affricate', 'velar']], <g affricate>, <n plosive>, <ŋ plosive>, <d voiceless>, <g voiceless>, <k voiced>, <n voiceless>, <a velar>, ['d', ['-', 'voiced']], ['ŋ', ['alveolar', 'voiced', 'sibilant']], <n sibilant>, <s plosive>, <t͡s velar>, <t͡ʃ voiced>, ['t͡s', ['-', 'affricate']], <s affricate>, <t nasalized>, <t nasal>, ['t̃͡s̃', ['-', 'nasalized']], <t̃͡s̃ velar>, <t̃ velar>, <s̃ velar>, <t lateral>, <s lateral>, ['tʰ', <voiced sibilant fricative>], <tʰ velar>, <kʰ voiced>, <tʰ affricate>, <t͡s aspirated>, ['tʰ', ['-', 'aspirated']], ['t͡sʰ', ['-', 'aspirated']], ['t͡sʰ', ['-', 'affricate']], <tˠ affricate>, <t͡s velarized>, ['tˠ', ['-', 'velarized']], ['t͡sˠ', ['-', 'velarized']], ['t͡sˠ', ['-', 'affricate']], <t͡s affricate>, <t͡sʰ affricate>, ['ã', <voiced alveolar sibilant>], <t͡sʰ palatalized>, ['sˠ', ['-', 'velar']], ['ʃˠ', ['-', 'velar']], ['ʃˠ', ['-', 'velarized']], ['tʰ', ["-", "aspirated"]], ['ŋ', ['-', 'nasal']], ['ŋ', ['-', 'nasalized']], ['ɫ', ['-', 'velar']], ['ɫ', ['-', 'velarized']], <l velar>, ['ã', ['-', 'nasal']], ['ã', ['-', 'nasalized']], <a nasal>, <a nasalized>,<ɰ alveolar>,<ʟ alveolar>, <ʍ alveolar>, <w̥ alveolar>, <ã voiceless>, <a voiceless>, <æ voiceless>, <œ voiceless>, ];
#my @results5 = ['ʔ', 'b', 'β', 'x', 'ʒ', 't͡s', 't͡ɬ', 'k͡x', 'g͡ɣ', 'd̃', 'g̃', 't', 'k', 'g', 'n̥', 'a', 't', 'z̃', 'z̃', 't', 'k͡x', 'd͡ʒ', 'ts', 't͡s',  't̃', 'n̥', 't͡s', 'k̃͡x̃', 'k̃', 'x̃', 'l̥', 'ɬ', 'zʰ', 'kʰ', 'gʰ', 't͡sʰ', 't͡sʰ', 't', 't͡s', 'tsʰ', 't͡sˠ', 't͡sˠ', 't', 't͡s', 'tsˠ', 't͡s', 't͡sʰ', 'ã', 't͡sʰʲ',  's', 'ʃ', 'ʃ', 't', 'g', 'ŋ', 'l', 'l', 'ʟ', 'a', 'a', 'ã', 'ã', 'ɹ', 'l', 'ɹ̥', 'ɹ̥', 'ḁ̃', 'ḁ', 'æ̥', 'œ̥' ];
#for @tests5.kv -> $i,$v {
	##says "";
##	says @tests5[$i];
	#my $value = switch-aspect($v[0], $v[1]);
	#say ($value eq @results5[$i]) ?? colored("OK","black on_green")~" {$v[0]} → {$v[1]} = $value" !! colored("NOT OK","white on_red")~" {$v[0]} → {$v[1]} ≠ $value = {@results5[$i]}";
#}

#says switch-aspect('ã', 'voiceless');
#says switch-aspect("tʰ","voiced");
#says switch-aspect("tʰ", "velar");
#my $a = switch-aspect("ã", "voiceless");
#says $a, ' ', $a.NFD.elems;
#says get-letters('nãtʰaŋ', 'aspirated');
#says get-letters('nãtʰaŋ', 'plosive');
#says get-letters('nãtʰaŋ', 'nasal', :negative);
#says get-letters('nãtʰaŋ', 'alveolar', :negative);
# nãaŋt͡s → - affricate =
#says get-letters('nãtʰaŋ', 'continuant');
#says get-letters('nãtʰaŋ', 'continuant', :negative);
#says switch-aspect('tʰ', <voiced sibilant fricative>);
#says switch-aspect('tʰ', <affricate>);
#says 't͡s → [nasalized] = ', switch-aspect('t͡s', 'nasalized');
#says 't̃͡s̃ → [-nasalized] = ', switch-aspect('t̃͡s̃', ['-', 'nasalized'])
#says 't̃͡s̃ → [-alveolar] = ', switch-aspect('t̃͡s̃', ['-', 'alveolar']);
#says split('nãtʰaŋ');
#says overlaps(['plosive'],  'tʰ', 'z');
#says overlaps(['aspirated'], 'tʰ', 'z');
#says switch-aspect('ŋ', <alveolar voiced sibilant>); //
#says get-aspects('t');
#says get-aspects('n');
#says change-aspect('s', 'aspirated', ['fricative']);
#says split('nãt͡sʰaŋ');
#says get-letters('nãt͡sʰaŋ', 'affricate');
#says get-letters('nãt͡sʰaŋ', 'nasal');
#says get-letters('nãt͡sʰaŋ', 'open');
#says get-letters('nãt͡sʰaŋ', 'vowels');
#says get-letters('nãt͡sʰaŋ', 'consonants');
#says get-aspects('ḁ̃');
#says get-aspects('ŋ̥');
#says get-aspects('a');
#says get-letters('nãt͡sʰḁŋ', 'voiced');
#says get-letters('nḁ̃t͡sʰaŋ', 'voiceless');
sub dds(|c) {
	#say callframe(1).line;
	#dd |c;
}
sub says(**@c) {
	#say |@c;
}

