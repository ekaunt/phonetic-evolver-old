#use Grammar::Tracer;
use Terminal::ANSIColor;

grammar NodesParser {
	regex TOP {
		[ \h* [<new-node>||<conversions>]* <comment>? ]+ %% [ \v | ';' ]
	}

	rule conversions {
		<conversion>+ %% [ \v | ';' ]
	}

	token conversion {
		<-[^\v-]> <-[\v]>*
	}

	token comment {
		'--' \V*
	}

	token number { \d+ }

	rule new-node {
		'^' <new-gen=node> ['"' ~ '"' $<description>=<-[\v"]>*?]? [ '^' <ref-gen=node>]*
	}
	rule node {
		[<title>||<error("Missing generation's title/name.")>]
		['@'||<error("Year must be specified with an @ sign (before the year number)")>]
		[<year=number>||<error('The year MUST be specified')>]
	}
	token title {
		<-[^@\v]>+
	}

	method error($msg) {
		my $parsed = self.target.substr(0, self.pos).trim-trailing;
		my $context = colored($parsed.substr($parsed.chars - 10 max 0), 'red') ~ colored('⏏', 'yellow') ~ self.target.substr($parsed.chars, 10);
		say "Cannot understand input: $msg\n"
			~ "at around \"" ~ $context ~ '"'
			~ "\n(error location indicated by ⏏)\n";
		#die if ;
		exit;
	}
}

#say NodesParser.parse('
#--- scf - sound changes file (format)

#^ G1 @0 "desciption is here"
#lʲ → j -- comment
#^G1.5 @500 ^G1@0
#ç → x
#p → v -- test

 #^ G 2 @1000 ^G1@1
#pt → p

#^G3 @ 2000  ^ G 2 @ 1000
#[stop] > [fricative]
#');
#say '############################';
