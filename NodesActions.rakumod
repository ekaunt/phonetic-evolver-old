use Graph;
use Terminal::ANSIColor;

class NodesActions {
	has Graph $.graph is required;
	
	method conversions($/) { 
		$!graph.cur-gen.sound-changes = $<conversion>».made.join("\n");
	}
	method conversion($/) { $/.make: ~$/ }

	method new-node($/) {
		#$!graph.cur-gen.words.append: $!word;
		#say $<ref-gen>;
		my @parents = $<ref-gen>.map: {$!graph.get-node($_<title>.made, +$_<year>, :create)};
		say @parents;
		@parents = $!graph.cur-gen if not @parents;
		#$!word = @parents[0].words[*-1];
		my $child = $!graph.add-node(Node.new(title=>$<new-gen><title>.made, year=>+$<new-gen><year>));
		error($/, "Cannot make node a child of itself") if $child ∈ @parents;
		@parents».add-child: $child;
		$child.add-parents: @parents;
		$child.description = ~$<description> if $<description>;
		say 'added child: ', $child;
	}
	method title($/) {
		$/.make: $/.trim;
	}

	sub error(Match $/, Str $msg) {
		my $parsed = $/.target.substr(0, $/.pos).trim-trailing;
		my $context = colored($parsed.substr($parsed.chars - 20 max 0), 'green') ~ colored('⏏', 'yellow') ~ colored($/.target.substr($parsed.chars, 20), 'red');
		my $line-no = $parsed.lines.elems;
		say "Cannot parse input: $msg\n"
			~ "at line $line-no, around \"" ~ $context ~ '"'
			~ "\n(error location indicated by ⏏)\n";
		#exit;
	}
}
