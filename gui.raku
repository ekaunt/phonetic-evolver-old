use lib ".";
use Utils;
use PhoneticEvolver;
use Graph;
use GUI::Tree;
#use GUI::Lexicon;
use GUI::Glossary;
use GUI::SoundChanges;
use GUI::CoinageList;

use JSON::Fast;

use Gnome::Gtk3::CellRendererText;
use Gnome::Gtk3::TreeStore;
use Gnome::Gtk3::TreeView;
use Gnome::Gtk3::Notebook;
use Gnome::Gtk3::Main;
use Gnome::Gtk3::Builder;
use Gnome::Gtk3::Window;
use Gnome::Gtk3::Dialog;
use Gnome::Gtk3::MessageDialog;
use Gnome::Gtk3::FileChooser;
use Gnome::Gtk3::FileChooserDialog;
use Gnome::Gtk3::FileFilter;
use Gnome::Gtk3::Label;
use Gnome::Gtk3::TreeIter;
use Gnome::Gtk3::TreeViewColumn;
use Gnome::Gtk3::ListStore;
use Gnome::Gtk3::SearchEntry;
use Gnome::Gtk3::Grid;
use Gnome::Gtk3::Entry;
use Gnome::Gtk3::Box;
use Gnome::Gtk3::Button;
use Gnome::Gtk3::ProgressBar;
use Gnome::Gtk3::StyleContext;
use Gnome::Gtk3::StyleProvider;
use Gnome::Gtk3::CssProvider;
use Gnome::Gdk3::Screen;
use Gnome::Gdk3::Events;
use Gnome::Glib::Error;
use Gnome::N::GlibToRakuTypes;
use Gnome::N::NativeLib;

use NativeCall;

#use Gnome::N::X;
#Gnome::N::debug(:on);

class GUIMain {

my PhoneticEvolver $pe .= new();

my Gnome::Gtk3::Builder $builder .= new;
my Gnome::Glib::Error $e = $builder.add-from-file('gui.glade');
die $e.message if $e.is-valid;
my Gnome::Gtk3::Window $w .= new(:build-id<window>);
has IO::Path $!file;
my IO::Path $tempdir;
#my IO::Path $lexicon-dir;
my $glossary;
my %sc;
my %open-sc-tabs;


# set an outline around changed words
my Gnome::Gtk3::CssProvider $style .= new;
my $context = Gnome::Gtk3::StyleContext.new(:native-object($w.get-style-context()));
$style.load-from-data('
#changed {
	border: 1px solid;
}
');
my Gnome::Gdk3::Screen $screen .= new;
$context.add-provider-for-screen($screen, $style, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);


sub add-drill-nodes() {
	my Gnome::Gtk3::TreeView $drill 					.= new(:build-id<drill>);
	my Gnome::Gtk3::TreeStore $drill-store 				.= new(:build-id<drill-store>);
	my Gnome::Gtk3::TreeViewColumn $drill-generation 	.= new(:build-id<drill-generation>);
	my Gnome::Gtk3::CellRendererText $cr-generation  	.= new(:build-id<cr-generation>);

	$drill-generation.add-attribute($cr-generation, 'text', 0);

	my Gnome::Gtk3::TreeIter $iter;
	# keep track of parents so that the nodes will go attach to the correct parent
	my %iters = %(:root($iter));
	my $nodes = $pe.graph.nodes-parent();
	# skip the root node
	$nodes();
	while $nodes() -> $_ {
		my ($parent, $node) = $_;
		my $name = $node.get-name~(' - '~$node.description if $node.description);
		#say $name;
		my $iter;
		# start a new root node if im a root node
		if $parent.title eq 'root' {
			$iter = $drill-store.gtk-tree-store-append(Nil);
		} else {
			$iter = $drill-store.gtk-tree-store-append(%iters{$parent.title~$parent.year});
		}
		$drill-store.set($iter, 0, $name);
		%iters{$node.title~$node.year} = $iter;
	}
	$drill.show-all;
}

# add the list of nodes to the dropdown menu in the lexicon Gen Appears column.
#sub add-lexicon-generation-names() {
	#my Gnome::Gtk3::ListStore $list-store .= new(:build-id<gen-list>);
	#my $nodes = $pe.graph.nodes;
	## dont get the root node in the list
	#$nodes();
	#while $nodes() -> $node {
		#my $iter = $list-store.gtk-list-store-append;
		#$list-store.gtk-list-store-set($iter, 0, $node.get-name);
	#}
#}

## add the list of glossaries to the dropdown menu in the coinages tab.
#sub update-lexicon-list {
	#my Gnome::Gtk3::ListStore $list-store .= new(:build-id<lexicon-list>);
	#$list-store.list-store-clear;
	#for Utils.lexicons.keys {
		#my $iter = $list-store.gtk-list-store-append;
		#$list-store.gtk-list-store-set($iter, 0, $_);
	#}
#}




method on-window-close(N-GdkEvent $event --> gboolean) {
	#say 'here';
	#$w.hide-on-delete;
	#say 'hidden';
	if not self.save {
		return True;
	} else {
		$w.destroy;
		Gnome::Gtk3::Main.new.quit;
		return False;
	}
}

# root word list
#class LexiconTab {
	#my Gnome::Gtk3::Notebook $nb .= new(:build-id<lexicon>);
	#my Supply $coinages-supply = Utils.coinages-changes.Supply;

	#method add-lexicon-tab(IO::Path :$file) {
		#my GUILexicon $lexicon .= new(:$pe, :main(self));

		#my Str $tab-title = $file ?? $file.basename !! 'Lexicon '~$nb.get-n-pages;

		## insert a page at the second to last position (before the add tab tab)
		#$nb.insert-page($lexicon, (my $name = Gnome::Gtk3::Label.new(:text($tab-title))), $nb.get-n-pages-1);
		#$lexicon.tab-name = $name;
		#$lexicon.load-file($file) if $file;
		#$lexicon.show-all;
		#$nb.set-current-page($nb.get-n-pages-2);
		#Utils.lexicons{$lexicon.tab-name.get-text} = $lexicon;
		#update-lexicon-list;

		#$coinages-supply.tap(-> $/ {
			#$lexicon.update-lexicon($<old-word>//Word, $<new-word>//Word);
		#});
	#}

	#method rename-tab(Str $old-name, Str $new-name, GUILexicon $lexicon) {
		#if Utils.lexicons{$new-name}:exists or $new-name eq '' {
			#$lexicon.show-dialog("The root word list name \"{$new-name}\" is already taken");
			#$lexicon.on-rename-clicked;
			#return False;
		#}
		#my GUILexicon $tab = Utils.lexicons{$old-name};
		#Utils.lexicons{$old-name}:delete;
		#Utils.lexicons{$new-name} = $lexicon;
		#update-lexicon-list;
		#return True;
	#}

	#method remove-tab(Str $name) {
		#Utils.lexicons{$name}:delete;
		#say Utils.lexicons;
		#update-lexicon-list;
	#}


	#method on-lexicon-new-tab-clicked() {
		## 1 = left mouse, 2 = middle, 3 = left
		##if $button.type == GDK_BUTTON_RELEASE and $button.button == 1 {
			#self.add-lexicon-tab;
		##}
	#}
#}

# sound changes
class SC {
	my Gnome::Gtk3::Notebook $nb .= new(:build-id<tree>);
	my Supply $coinages-supply = Utils.coinages-changes.Supply;
	my Supply $glossary-supply = Utils.glossary.Supply;

	method add-tab(Node $node) {
		if %open-sc-tabs{$node.get-name}:exists {
			$nb.set-current-page(%open-sc-tabs{$node.get-name});
			return;
		}
		say 'creating sound changes tab';
		#dd $node.sound-changes;
		my SoundChanges $sc .= new(:graph($pe.graph), :$node, :main(self));
		say 'created';

		$nb.append-page($sc, Gnome::Gtk3::Label.new(:text($node.get-name)));
		#say GUISoundChanges.new(:native-object($nb.get-nth-page($nb.get-n-pages-1))).node;

		$coinages-supply.tap(-> $/ {
			if $<node>.get-name eq $node.get-name {
				say 'updating node ', $<node>;
				$sc.update-coinage($<old-word>//Word, $<new-word>//Word, $<row-num>);
			}
		});
		$glossary-supply.tap(-> $/ {
			if $<node>.get-name eq $node.get-name {
				say 'updating glossary ', $<node>.get-name;
				$sc.update-glossary($<old-word>//Word, $<new-word>//Word);
			}
		});

		$sc.show-all;
		$nb.set-tab-reorderable($sc, 1);
		say 'attached';
		$nb.set-current-page(-1);
		%open-sc-tabs{$node.get-name} = $nb.get-current-page;
		%sc{$node.get-name} = $sc;
	}

	method remove-tab(Str $name) {
		%open-sc-tabs{$name}:delete;
		%sc{$name}:delete;
	}
}

#| set the size of the tree's drawing area to the size of the window when the window changes size.
#method on-configure-event(N-GdkEventConfigure $event, :$tree) {
#	$tree.draw.set-size-request($w.get-allocated-width, $w.get-allocated-height);
#}

#| draw the tree once the window has already appeared on screen
method on-window-spawn(N-GdkEvent $event) {
	my SC $sc .= new;
	my Tree $tree .= new(:graph($pe.graph), :$sc);
#	$w.register-signal(self, 'on-configure-event', 'configure-event', :$tree);
}

#| open up and load a .pe file into the program
method open-pe-file {
	#my Gnome::Gtk3::FileChooserDialog $dialog .= new(
	  #:title("Open File"), :parent($w),
	  #:action(GTK_FILE_CHOOSER_ACTION_OPEN),
	  #:button-spec( [
		  #"_Cancel", GTK_RESPONSE_CANCEL,
		  #"_Open", GTK_RESPONSE_ACCEPT,
		#]
	  #)
	#);
	#my Gnome::Gtk3::FileFilter $file-filter .= new;
	#$file-filter.add-pattern('*.pef');
	#$dialog.set-filter($file-filter);

	#my $response = $dialog.gtk-dialog-run;
	#$dialog.gtk-widget-hide;
	#if ($response ~~ GTK_RESPONSE_ACCEPT) {
		#$!file = $dialog.get-filename.IO;
		#say $!file;
		##$tempdir = tempdir.IO;
		##say $tempdir;
		##.extract for archive-read($!file.IO, destpath=>$tempdir.Str);
		##$lexicon-dir = $tempdir.add('/lexicons');
		##$glossary = from-json $tempdir.add('/glossary.gls').slurp;

		## without this the program will quit (on purpose (first line in method start()))
		#return True;
	#}

	$!file = 'test/language.pef'.IO;
	##my File::Zip $file .= new($!file);
	#$tempdir = tempdir.IO;
	#say $tempdir;
	##$file.extract($tempdir);
	#.extract for archive-read($!file.IO, destpath=>$tempdir.Str);
	#$lexicon-dir = $tempdir.add('/lexicons');
	return True;
}



# this method gets called when the 'Run!' button is pressed (when you want to process all words through all generations)
method run {
	self.save;

	my Gnome::Gtk3::Dialog $dialog .= new(:build-id<run-confirm>);
	$dialog.show-all;
	my $response = $dialog.gtk-dialog-run;
	$dialog.gtk-widget-hide;
	if $response ~~ GTK_RESPONSE_ACCEPT {
		my Gnome::Gtk3::Window $loading-screen .= new(:build-id<loading-screen>);
		my Gnome::Gtk3::ProgressBar $percent-bar .= new(:build-id<percent>);
		say Utils.unprocessed.elems;
		my Rat $percentages-to-move-by = 1.00 / Utils.unprocessed.elems;
		my Rat $percent-upto = 0.00;

		# we need to put the loading screen in another thread so that it shows up on screen
#		start {
			$loading-screen.show;

			#my $nodes = $pe.graph.nodes;
			#while $nodes() -> $node {
				#$pe.parse-sound-changes($node.coinages, :$percent-todo, :$percent-upto);
				#$percent-upto += $percent-todo;
			#}
			while Utils.unprocessed.pop -> $word {
				say 'utils.unprocessed: ', Utils.unprocessed».word, ': ', $word.word;
				$pe.parse-sound-changes($word);
				$word.processed = True;
				$percent-bar.set-fraction($percent-upto += $percentages-to-move-by);
			}

			#update the word bank entries with the new data
			for %sc.values -> $sc {
				$sc.update-glossary;
			}

			say 'hiding';
			$loading-screen.hide;
#		}
	}
}


#| [ save the entire project to a .pef file ]
method save() {
	#my IO::Path $temp = tempdir.IO;
	#say $temp;

	#mkdir($temp);
	#my $cwd = $*CWD;
	#chdir $temp;

	## add all the sound changes
	#my IO::Path $sound-changes = "sound-changes/".IO;
	#mkdir($sound-changes);

	#first save all sound-changes changes into memory
	for %sc.values {
		.save-sound-changes;
	}

	## then commit it to storage
	#my $nodes = $pe.graph.nodes-unique;
	## skip the root node
	#$nodes();
	#while $nodes() -> $node {
		## skip if this node already exists
		##next if $sound-changes.add($node.title~$node.year).f;
		#say $node;
		#my Str $contents = '^'~$node.get-name;
		#$contents ~= ' "'~$node.description~'"' if $node.description;
		#for $node.parents {
			#$contents ~= '  ^'~.get-name;
		#}
		## prevent the file from getting filled with semicolons and extra whitespace
		#$contents ~= ";\n" ~ ($node.sound-changes.starts-with(';') ?? $node.sound-changes.split(';')[1..*-1].join.trim !! $node.sound-changes) ~ "\n";
		##say $node.sound-changes;
		##say $contents;
		#$sound-changes.add($node.title.trim~$node.year).spurt: $contents;
	#}

	my $nodes = $pe.graph.nodes-unique;
	my @save-data;
	while $nodes() -> $node {
		my $glossary = $node.words.map({
			%(
				word => .word,
				definition => .definition,
				root-word => .root-word,
				root-definition => .root-definition,
				path => (%(name => .title, year => .year) for .path),
				#(lexicon => .lexicon.tab-name.get-text if .lexicon)
				(processed => .processed if .processed eqv False),
				(will-root-word-be-changed => .will-root-word-be-changed if .will-root-word-be-changed),
				(will-root-definition-be-changed => .will-root-definition-be-changed if .will-root-definition-be-changed),
			)
		});

		my $coinages = $node.coinages.map({
			%(
				word => .word,
				definition => .definition,
				(processed => .processed if .processed eqv False),
			)
		});

		my $parents = $node.parents.map({
			%(
				name => .title,
				year => .year,
			)
		});

		my $save-data = %(
			name => $node.title,
			year => $node.year,
			description => $node.description,
			:$parents,
			:$glossary,
			:$coinages,
			sound-changes => $node.sound-changes,
		);
		@save-data.append: $save-data;
	}
	'test/test-lang.pef'.IO.spurt: to-json @save-data, :pretty, :sorted-keys;
	say 'saved to test-lang.pef';


	# add all the root word lists
	#my IO::Path $lexicons = "lexicons/".IO;
	#mkdir($lexicons);


	##my Gnome::Gtk3::Notebook $nb .= new(:build-id<lexicon>);
	#for Utils.lexicons.values -> $lexicon {
	##for ^$nb.get-n-pages -> $i {
		##my GUIRootWordList $lexicon .= new(:graph($pe.graph), :main($!lexicon), :native-object($nb.get-nth-page($i)));
		#say $lexicon.tab-name.get-text;
		#my Str $file-contents = $lexicon.get-lexicon-rows;
		##say $file-contents;
		#return False if not $file-contents;
		#$lexicons.add($lexicon.tab-name.get-text).spurt: $file-contents;
	#}

	## now compress the files
	#with archive-write($*HOME~'/dev/raku/phonetic-evolver/test/test-lang.pef', format=>'zip') {
		#.add: dir($temp);
		#.add: dir($temp.add($sound-changes));
		##.add: dir($temp.add($lexicons));
		#.close;
	#}
	#chdir $cwd;

	return True;
}


#has LexiconTab $!lexicon .= new;
method start {
	#make sure that the program is able to be threaded
#	gdk_threads_init();
#	gdk_threads_enter();

	# select a file and quit if none selected
	exit if not self.open-pe-file;

	# add the generations and sound changes to the program after processing it in PhoneticEvolver
	#Promise.start({
		#$pe.parse-generations(:sound-changes($tempdir.add('/sound-changes')));
	#}).then({
		#add-lexicon-generation-names;

		# open the glossary and add all entries to ram
		#$pe.parse-glossary($glossary);

		$pe.parse-pef(from-json $!file.slurp);


		# set the size of the tree's drawing area to the size of the scroll window container.
		# the delay is cuz the program doesnt know about the size till it loads
#		start {
#			sleep 1.0;
#			my Gnome::Gtk3::ScrolledWindow $scroll .= new(:build-id<tree-scroll>);
#			$tree.draw.set-size-request($tree.get-allocated-width, Gnome::Gtk3::Adjustment.new(:native-object($scroll.get-vadjustment)).get-upper.Int);
#		}

		# add all wordbank tab into their tabs
		#for $lexicon-dir.dir {
			#$!lexicon.add-lexicon-tab(:file($_));
		#}
		my CoinageList $coinage-list .= new(:graph($pe.graph), :build-id<coinage-list>);
		my Glossary $glossary .= new(:graph($pe.graph), :build-id<glossary>);

		$w.register-signal(self, 'on-window-spawn', 'map-event');
		$w.show-all;
	#});


	my Hash $handlers = %(
		:run(self),
		:save(self),
		:on-window-close(self),
		#:on-lexicon-new-tab-clicked($!lexicon),
	);
	$builder.connect-signals-full($handlers);

	# open the main window
	Gnome::Gtk3::Main.new.main;
#	gdk_threads_leave();
}
sub gdk_threads_init() is native(&gdk-lib) { * };
sub gdk_threads_enter() is native(&gdk-lib) { * };
sub gdk_threads_leave() is native(&gdk-lib) { * };
}

my GUIMain $main .= new;
$main.start;
