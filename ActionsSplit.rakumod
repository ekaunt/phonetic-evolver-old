#| [ split each line of the sound changes file into an Array[List] structured as [LHS, RHS, WHS] ]
class Split {
	has Array[List] $!result;

	method TOP($/) {
		$/.make: $!result;
	}

	multi method conversion($/ where $<surround>) {
		my Str $when;
		$when ~= '#' if $<word-beg>;
		#say 'sides: ', $<sides>.made if $<sides>;
		$when ~= $<sides>.made if $<sides>;
		$when ~= '#' if $<word-end>;
		$!result.push: [$<lhs>.made, $<rhs>.made, ~$<surround>, $when];
	}
	multi method conversion($/ where !$<surround>) {
		my Str $when;
		$when ~= '#' if $<word-beg>;
		#say 'when: ', $<when>.made if $<when>;
		$when ~= ~$<when>.made if $<when>;
		$when ~= '#' if $<word-end>;
		$!result.push: [$<lhs>.made, $<rhs>.made, False, $when];
	}

	method side($/) {
		$/.make: $/.Str.trim;
	}
}
